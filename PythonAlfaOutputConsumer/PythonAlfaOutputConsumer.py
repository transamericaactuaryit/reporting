import os
import io
import sys
import http
import pprint
import time
import pyodbc
import textwrap
import traceback
import zipfile as zip
import cProfile as cp
import numpy as np
import pandas as pd
import openpyxl as pxl
import HadoopUtilities as hu
import TapUtilities as tap
import AlfaUtilities as au
import Constants as const
from datetime import (timedelta, datetime)
from pathlib import Path
from AlfaFileSet import AlfaFileSet
from PandasUtilities import PandasUtilities

class PythonAlfaOutputConsumer:

    def __init__(self):
        self.params = {}
        self.tap = tap.TapUtilities()
        self.am1_utils = au.AlfaUtilities()
        pass

    def set_config_param(self, key, value):
        self.params[key] = value

    def get_config_param(self, key) -> object:
        return self.params[key]


    def run_arx2_code(self):
        start_time = time.monotonic()

        #arx2_file_list = [const.ARX2_FILE_PATH + 'Base.Run.002.Arx2',
        #                  const.ARX2_FILE_PATH + 'Fpq118_Base_Det.Run.002.Arx2',
        #                  const.ARX2_FILE_PATH + 'Bopparallel.Run.003.Arx2']

        #arx2_file_list = [const.ARX2_FILE_PATH + 'Fpq118_Base_Det.Run.002.Arx2']

        #arx2_file_list = [const.ARX2_FILE_PATH + 'Bopparallel.Run.003.Arx2']

        arx2_file_list = ['./Bopparallel.Run.003.Arx2']

        #arx2_file_list = [const.ARX2_FILE_PATH + 'Vaq118_Base_105.Run.094.Arx2',
        #                                         'Vaq118_Base_105.Run.088.Arx2',
        #                                         'Vaq118_Base_105.Run.040.Arx2']

        #arx2_file_list = [const.ARX2_FILE_PATH + const.ArxPaths.VOBA.value + 'Vaq118_Base_105.Run.088.edited.Arx2']

        #arx2_file_list = [const.ARX2_FILE_PATH + const.ArxPaths.BOND_ATTRIBUTION.value + 'Q317_Ifrs_Bondmm_Attrib.Run.002.Arx2',
        #                  const.ARX2_FILE_PATH + const.ArxPaths.CROSS_ATTRIBUTION.value + 'Q317_Ifrs_Cross_Attrib.Run.002.Arx2',
        #                  const.ARX2_FILE_PATH + const.ArxPaths.EQUITY_ATTRIBUTION.value + 'Q317_Ifrs_Equity_Attrib.Run.002.Arx2',
        #                  const.ARX2_FILE_PATH + const.ArxPaths.RATE_ATTRIBUTION.value + 'Q317_Ifrs_Nrov.Run.002.Arx2']

        #arx2_file_list = [const.ARX2_FILE_PATH + const.ArxPaths.RANDOM_SAMPLES.value + '2017q3attribution_Interest_C3p2.Run.001.Arx2',
        #                  const.ARX2_FILE_PATH + const.ArxPaths.RANDOM_SAMPLES.value + '2017q3attribution_Hedge_Stoch.Run.001.Arx2',
        #                  const.ARX2_FILE_PATH + const.ArxPaths.RANDOM_SAMPLES.value + '2017q3attribution_Rebase_Det.Run.002.Arx2',
        #                  const.ARX2_FILE_PATH + const.ArxPaths.RANDOM_SAMPLES.value + 'Ag43c3p2stoch2q17_Interest_Stoch.Run.001.Arx2',
        #                  const.ARX2_FILE_PATH + const.ArxPaths.GMB_SINGLE_SCENARIO.value + 'Gmbq317_Singlescen.Run.001.Arx2']

        arx2_util = au.AlfaUtilities()

        arx2_util.extract_arx2_header_parameters(arx2_file_list[0])

        for af in arx2_file_list:
            arx2_util.process_arx_file(datetime.now(), af, const.BLACK_LIST_OF_ROWS, const.SECTION_TITLES)

        end_time = time.monotonic()
        print(timedelta(seconds=end_time - start_time))
        pass

    #region odbc_insert_attempt
    def odbc_insert_example(self, row):
        try:
            test_sql = """insert into table am1_cohort values (
                          {}, {}, {}, {}, {}, {}, {}, {},
                          {}, {}, {}, {}, {}, {}, {}, {},
                          {}, {}, {}, {}, {}, {}, {}, {},
                          {}, {}, {}, {}, {}, {}, {}, {},
                          {}, {}, {}, {}, {}, {}, {}, {},
                          {}, {}, {}, {}, {}, {}, {}, {},
                          {}, {}, {}, {}, {}, {}, {}, {},
                          {}, {}, {}, {}, {}, {}, {}, {})""".format(row.row_id, row.processed_ts, row.model_name, row.file_run_no, row.alfa_run_id, row.segment_number, row.segment_record, row.alfa_plan_code,
                                                                    row.issue_age, row.gender, row.tax_rider, row.biz_unit, row.legal_entity_stat_comp, row.is_qualified, row.gmdb_type, row.gmib_type,
                                                                    row.reinsurance_type, row.cohort_group, row.glwb_type, row.issue_year, row.issue_month, row.is_new_business, row.policy_number, row.dac_group,
                                                                    row.cohort_name, row.category, row.valuation_date, row.as_of_date, row.is_current_qtr, row.run_type_1, row.run_type_2, row.account_value,
                                                                    row.withdrawal_benefit_amt, row.accum_benefit_amt, row.policy_count, row.separate_accnt_value, row.cqocs_margin_amt, row.cqocs_wb_benefit_amt,
                                                                    row.cqocs_ab_benefit_amt, row.cqocs_ab_fee_amt, row.cqocs_wbdb_fee_amt, row.cqocs_wb_fee_amt, row.cqocs_wb_reserve_amt, row.cqocs_ab_reserve_amt,
                                                                    row.pqocs_wb_benefit_amt, row.pqocs_ab_benefit_amt, row.pqocs_ab_fee_amt, row.pqocs_wbdb_fee_amt,
                                                                    row.pqocs_wb_fee_amt, row.pqocs_wb_reserve_amt, row.pqocs_ab_reserve_amt, row.cqocs_up20bps_ab_reserve_amt,
                                                                    row.cqocs_dn20bps_wb_reserve_amt, row.cqocs_dn20bps_ab_reserve_amt, row.cqocs_up20bps_wb_reserve_amt,
                                                                    row.cqocs_nomrgn_ab_fee_amt, row.cqocs_nomrgn_wbdb_fee_amt, row.cqocs_nomrgn_wb_fee_amt, row.cqocs_nomrgn_wb_reserve_amt,
                                                                    row.cqocs_nomrgn_ab_reserve_amt, row.cqocs_nomrgn_up20bps_wb_reserve_amt,
                                                                    row.cqocs_nomrgn_up20bps_ab_reserve_amt, row.cqocs_nomrgn_dn20bps_wb_reserve_amt, row.cqocs_nomrgn_dn20bps_ab_reserve_amt)

            #print(test_sql)

            #conn = pyodbc.connect(const.IMPALA_DSN, autocommit = True)
            #cursor = conn.cursor()
            #cursor.execute("show databases;")
            #cursor.execute("select * from actuarialresultsdev.am1_cohort limit 5;")
            #cursor.execute("select * from actuarialresultsdev.am1_nb6pt_average limit 3;")
            #cursor.execute(test_sql)
            #rows = cursor.fetchall()

            #for r in rows:
            #    print(r)

            #cursor.close()
            #conn.close()
        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback)
        finally:
            try:
                if cursor:
                    cursor.close()
                if conn:
                    conn.close()
            except:
                pass
        pass
    #endregion

    def process_am1_files(self):

        afs_list = []

        file_list = self.tap.get_list_of_files_by_type(const.ALFA_INPUT_PATH, const.AM1_FILE_EXT, False)

        file_dict = self.tap.build_dict_of_file_paths(const.ALFA_INPUT_PATH)
        pprint.pprint(file_dict)

        for file in file_list:
            self.am1_utils.process_file(self.get_config_param(const.PROCESSED_TS), file.path)

        for k, v in file_dict.items():
            tmp_afs = AlfaFileSet(user_name='fpolicastro', model_name=k.replace(const.AM1_FILE_EXT, ''))
            tmp_afs = self.tap.summarize_file_meta_data_list(file_list=file_dict[k], has_header=True, alfa_file_set=tmp_afs)
            afs_list.append(tmp_afs)
            pass

        #for t in afs_list:
        #    print(str(t))

        # todo: need to curr_qtr and fp_quarter logic

        # TODO: Need logic to handle empty am1 files that contain only the header and no data
        # Partially done...?

        txt_file_list = self.tap.get_list_of_files_by_type(const.ALFA_INPUT_PATH, const.TXT_FILE_EXT, False)

        pdu = PandasUtilities()
        inforce_cohorts_df = pdu.convert_file_to_data_frame(const.INFORCE_COHORTS_PATH, const.ColumnNameSet.COHORT)
        nb6_cohorts_df = pdu.convert_file_to_data_frame(const.NB6_COHORTS_PATH, const.ColumnNameSet.COHORT)

        for file in txt_file_list:
            df = pdu.convert_file_to_data_frame(file.path, const.ColumnNameSet.AM1)

            if str(df.model_name[0]).lower() == const.NB6_MODEL_NAME.lower():
                tmp = pd.merge(df, nb6_cohorts_df, how='left', on=[const.COL_DAC_GROUP])
            else:
                tmp = pd.merge(df, inforce_cohorts_df, how='left', on=[const.COL_FILE_RUN_NO, const.COL_DAC_GROUP])

            col_list = tmp.columns.tolist()
            cg_idx = col_list.index('category')
            col_list.insert(cg_idx, col_list.pop())
            tmp = tmp[col_list]

            #pprint.pprint(pdu.sql_select(tmp))

            #paoc.odbc_insert_example(tmp.iloc[[0]])

            with io.open(file.path.lower().replace(const.TXT_FILE_EXT, const.CSV_FILE_EXT), "w", const.FILE_BUFFER_SIZE) as output:
                output.write(tmp.to_csv(sep=const.TAB_CHAR, header=False, index=False))

        self.tap.delete_files_by_extension(const.ALFA_INPUT_PATH, const.TXT_FILE_EXT)
    pass

