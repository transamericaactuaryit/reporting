import Constants as const
import pprint
from AlfaFileSet import AlfaFileSet
import path
import os
from pathlib import Path
from itertools import (takewhile, repeat)

class TapUtilities:

    def __init__(self):
        pass


    def count_lines_in_file(self, file_name: str, has_header_row: bool):
        with open(file_name, 'rb', const.FILE_BUFFER_SIZE) as fp:
            buffer = takewhile(lambda x: x, (fp.raw.read(const.FILE_BUFFER_SIZE) for tmp in repeat(None)))
            line_count = sum(buf.count(b'\n') for buf in buffer)
            if has_header_row:
                line_count -= 1
        return line_count


    def delete_files_by_extension(self, path, file_ext):
        for root, dirs, files in os.walk(path):
            for current_file in files:
                if current_file.lower().endswith(file_ext):
                    os.remove(os.path.join(root, current_file))


    def get_list_of_files_by_type(self, path: str, file_ext: str, include_empties: bool):
        for entry in os.scandir(path):
            if entry.is_dir(follow_symlinks=False):
                yield from self.get_list_of_files_by_type(entry.path, file_ext, include_empties)
            else:
                if entry.name.lower().endswith(file_ext) and entry.is_file() and os.path.getsize(entry) > 0:
                    yield entry


    def build_dict_of_file_paths(self, root_dir: str) -> dict:
        file_dict = {}
        for dir_name, sub_dir_list, file_list in os.walk(root_dir, topdown=True):
            for file in file_list:
                if file.lower().endswith(const.AM1_FILE_EXT):
                    full_file_path = dir_name + os.altsep + file
                    if not file_dict.get(dir_name):
                        file_dict[dir_name] = [full_file_path]
                    else:
                        file_dict[dir_name].append(full_file_path)
        return file_dict


    def summarize_file_meta_data_dict(self, file_dict: dict, has_header: bool, alfa_file_set: AlfaFileSet) -> AlfaFileSet:
        for k in file_dict.keys():
            alfa_file_set.file_name_map[k] = []
            for v in file_dict[k]:
                p = Path(v)
                line_count = self.count_lines_in_file(p, has_header_row=has_header)
                alfa_file_set.file_name_map[k].append(p)
                alfa_file_set.file_count += 1
                alfa_file_set.row_count_by_file[p.name] = line_count
                alfa_file_set.row_count += line_count
                alfa_file_set.model_name = (v.split('.')[1]).split('/')[3]
        return alfa_file_set


    def summarize_file_meta_data_list(self, file_list: list, has_header: bool, alfa_file_set: AlfaFileSet) -> AlfaFileSet:
        for f in file_list:
            p = Path(f)
            line_count = self.count_lines_in_file(p, has_header_row=has_header)
            #alfa_file_set.file_name_map[k].append(p)
            alfa_file_set.file_count += 1
            #alfa_file_set.row_count_by_file[p.name] = line_count
            alfa_file_set.row_count += line_count
            #print((f.split('.')[1]).split('/')[3])
            alfa_file_set.model_name = (f.split('.')[1]).split('/')[3]
        return alfa_file_set

