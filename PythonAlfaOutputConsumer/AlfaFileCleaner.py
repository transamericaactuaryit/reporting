import io
import os
import csv
import sys
import shutil
import pandas as pd
import numpy as np
import openpyxl as pxl
import uuid
import zlib
import TapUtilities as tap
import Constants as const
from zipfile import ZipFile
from pathlib import Path
from itertools import (takewhile, repeat)
from datetime import (timedelta, datetime)
from AlfaFileSet import AlfaFileSet


class AlfaFileCleaner:

    def __init__(self):
        self.tap = tap.Tap_Utilities()
        self.processed_ts = datetime.now()
        pass

    #def __init__(self, afs:AlfaFileSet):
    #    print('in alternate constructor')
    #    self.tap = tap.TapUtilities()
    #    self.processed_ts = datetime.now()
    #    self.afs = afs


    def get_afs_data():
        return self.afs

    def GetListOfAm1Files(self, path, afs):
        for entry in os.scandir(path):
            p = Path(entry.path)
            if entry.is_dir(follow_symlinks=False):
                afs.file_name_map[p.parts[1]] = []
                yield from self.GetListOfAm1Files(entry.path, afs)
            else:
                if entry.name.lower().endswith(const.AM1_FILE_EXT) and entry.is_file():
                    lineCount = self.tap.count_lines_in_file(entry.path, True)
                    if not afs.file_name_map:
                        afs.file_name_map[p.parts[1]] = [{'output' : p, 'lineCount' : lineCount}]
                    else:
                        afs.file_name_map[p.parts[1]].append({'output' : p, 'lineCount' : lineCount})

                    afs.row_count_by_file[entry.name] = lineCount
                    afs.row_count += lineCount
                    afs.file_count += 1
                    yield entry

    #need to also join to cohort name in sql statement
    #if model name contains nb6, use nb6 cohorts table, otherwise use inforce table


    def FixAm1File(self, fullFilePath):
        file_name = Path(fullFilePath).name
        file_name_pieces = file_name.split(const.PERIOD)
        with io.open(fullFilePath, "r", const.FILE_BUFFER_SIZE) as input:
            headerLine = input.readline()
            with io.open(fullFilePath.lower().replace(const.AM1_FILE_EXT, const.TXT_FILE_EXT), "w", const.FILE_BUFFER_SIZE) as output:
                for line in input:
                    new_line = str(uuid.uuid4()) + const.TAB_CHAR
                    new_line += str(self.processed_ts) + const.TAB_CHAR
                    new_line += file_name_pieces[0] + const.TAB_CHAR
                    new_line += file_name_pieces[2] + const.TAB_CHAR
                    new_line += '2' + const.TAB_CHAR
                    new_line += line
                    output.write(new_line)


    def ProcessLine(self, line):
        columns = line.split(const.TAB_CHAR)
        line = line.replace('segmentnumber', 'SegmentNumber')
        line = line.replace('segmentrecord', 'SegmentRecord')
        line = line.replace('ck.Plan', 'AlfaPlanCode')
        line = line.replace('ck.IssAge', 'IssueAge')
        line = line.replace('ck.Gender', 'Gender')
        line = line.replace('ck.Class', 'TaxRider')
        line = line.replace('ck.Char1', 'BizUnit')
        line = line.replace('ck.Char2', 'LegalEntityStatComp')
        line = line.replace('ck.Char3', 'IsQualified')
        line = line.replace('ck.Char4', 'GmdbType')
        line = line.replace('ck.Char5', 'GmibType')
        line = line.replace('ck.Char6', 'ReinsuranceType')
        line = line.replace('ck.Char7', 'CohortGroup')
        line = line.replace('ck.Char8', 'GlwbType')
        line = line.replace('ck.IssYear', 'IssueYear')
        line = line.replace('ck.IssMon', 'IssueMonth')
        line = line.replace('ck.NewBus', 'IsNewBusiness')
        line = line.replace('DACGroup', 'DacGroup')
        line = line.replace('sys.category', 'Category')
        line = line.replace('ValDate', 'ValuationDate')
        line = line.replace('CurrQtr', 'IsCurrentQtr')
        line = line.replace('RunTypeII', 'RunType2')
        line = line.replace('RunTypeI', 'RunType1')
        line = line.replace('AVIF_ts_E0', 'AccountValue')
        line = line.replace('F133PVRptInitWBFace', 'WithdrawalBenefitAmt')
        line = line.replace('F133PVRptInitABFace', 'AccumBenefitAmt')
        line = line.replace('F133PVRptInitLives', 'PolicyCount')
        line = line.replace('F133PVRptInitSAV', 'SeparateAccntValue')
        line = line.replace('F133PVMarginOCS', 'CqcsMarginAmt')
        line = line.replace('F133PVGMWBBenOCS', 'CqcsWbBenefitAmt')
        line = line.replace('F133PVGMABBenOCS', 'CqcsAbBenefitAmt')
        line = line.replace('F133PVGMABFeeOCS', 'CqcsAbFeeAmt')
        line = line.replace('F133PVGMWBDBFeeOCS', 'CqcsWbDbFeeAmt')
        line = line.replace('F133PVGMWBFeeOCS', 'CqcsWbFeeAmt')
        line = line.replace('F133PVGMWBResOCS', 'CqcsWbReserveAmt')
        line = line.replace('F133PVGMABResOCS', 'CqcsAbReserveAmt')
        line = line.replace('F133PVGMWBBenOCSPQ', 'PqcsWbBenefitAmt')
        line = line.replace('F133PVGMABBenOCSPQ', 'PqcsAbBenefitAmt')
        line = line.replace('F133PVGMABFeeOCSPQ', 'PqcsAbFeeAmt')
        line = line.replace('F133PVGMWBDBFeeOCSPQ', 'PqcsWbDbFeeAmt')
        line = line.replace('F133PVGMWBFeeOCSPQ', 'PqcsWbFeeAmt')
        line = line.replace('F133PVGMWBResOCSPQ', 'PqcsWbReserveAmt')
        line = line.replace('F133PVGMABResOCSPQ', 'PqcsAbReserveAmt')
        line = line.replace('F133PVGMWBResOCSUp', 'CqcsUp20BpsWbReserveAmt')
        line = line.replace('F133PVGMABResOCSUp', 'CqcsUp20BpsAbReserveAmt')
        line = line.replace('F133PVGMWBResOCSDn', 'CqcsDn20BpsWbReserveAmt')
        line = line.replace('F133PVGMABResOCSDn', 'CqcsDn20BpsAbReserveAmt')
        line = line.replace('F133PVNMGMABFeeOCS', 'CqcsNoMrgnAbFeeAmt')
        line = line.replace('F133PVNMGMWBDBFeeOCS', 'CqcsNoMrgnWbDbFeeAmt')
        line = line.replace('F133PVNMGMWBFeeOCS', 'CqcsNoMrgnWbFeeAmt')
        line = line.replace('F133PVNMGMWBResNoOCS', 'CqcsNoMrgnWbReserveAmt')
        line = line.replace('F133PVNMGMABResNoOCS', 'CqcsNoMrgnAbReserveAmt')
        line = line.replace('F133PVNMGMABResOCSDn', 'CqcsNoMrgnUp20BpsWbReserveAmt')
        line = line.replace('F133PVNMGMABResOCSUp', 'CqcsNoMrgnUp20BpsAbReserveAmt')
        line = line.replace('F133PVNMGMWBResOCSDn', 'CqcsNoMrgnDn20BpsWbReserveAmt')
        line = line.replace('F133PVNMGMWBResOCSUp', 'CqcsNoMrgnDn20BpsAbReserveAmt')
        line = line.replace('/', '')
        line = line.replace('_th_ELastValue', '')
        return line
