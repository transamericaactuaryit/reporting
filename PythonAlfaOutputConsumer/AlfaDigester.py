import sys
import Constants as const
from TapUtilities import Tap_Utilities
import pyodbc
import traceback
import zipfile as zip
import os
import http
import pprint
import time
import cProfile as cp
import numpy as np
import pandas as pd
import openpyxl as pxl
import decimal as d
from pathlib import Path
from datetime import timedelta
from AlfaFileSet import AlfaFileSet
from AlfaFileCleaner import AlfaFileCleaner

class AlfaDigester:

    def __init__(self):
        self.tap = Tap_Utilities()
        pass


    def Execute2(self):
        pass


    def Execute(self, afs):

        afc = AlfaFileCleaner()

        account_value_total = 0
        policy_count_total = 0

        am1FileList = afc.GetListOfAm1Files(sys.argv[1] if len(sys.argv) > 1 else afs.am1_file_path, afs)
        #am1FileList = afc.GetListOfAm1Files(afs)

        for entry in am1FileList:
            afc.FixAm1File(entry.path)
            p = Path(entry.path)

            df = pd.read_table(entry.path.lower().replace(const.AM1_FILE_EXT, const.TXT_FILE_EXT),
                               header=None,
                               sep=const.TAB_CHAR,
                               names=const.AM1_COLUMN_NAMES)

            account_value_total += df['account_value'].sum()
            policy_count_total += df['policy_count'].sum()

        for entry in afs.file_name_map.keys():
            try:
                current_path = afs.am1_file_path + '/' + entry + '/' + entry + const.ZIP_FILE_EXT

                try:
                    os.remove(current_path)
                except Exception as ex:
                    pass

                with zip.ZipFile(current_path, mode='a', compression = zip.ZIP_DEFLATED, allowZip64 = True) as zipped:
                    for entry2 in afs.file_name_map[entry]:
                        tmpFile = str(entry2['output']).lower().replace(const.AM1_FILE_EXT, const.TXT_FILE_EXT)
                        zipped.write( './' + tmpFile, './' + tmpFile)
            except Exception as ex:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_exception(exc_type, exc_value, exc_traceback)
                pass

        print('account_value_total = ' + str(account_value_total))
        print('policy_count_total = ' + str(policy_count_total))
        print('user = ', afs.user_name)
        print('timestamp = ', afs.time_stamp)
        print('model = ', afs.model_name)
        print('file count = ', afs.file_count)
        print('row count = ', afs.row_count)
        print('row count by file:')
        pprint.pprint(afs.row_count_by_file)
        print('file name map:')
        pprint.pprint(afs.file_name_map)


def main():
    try:
        input("Press any key to continue ...")
        start_time = time.monotonic()
        print('Starting cleaning process...')

        #am1_file_path = './am1_files/cohort_base/'
        am1_file_path = './am1_files/'
        arx2_file_path = './arx2_files/'

        afs = AlfaFileSet('fpolicastro', 'Base', am1_file_path, arx2_file_path)
        afc = AlfaFileCleaner()

        pam1f = AlfaDigester()
        pam1f.Execute(afs)

        afs2 = AlfaFileSet('fpolicastro', 'Arx2', am1_file_path, arx2_file_path)
        parx2f = arx2.Arx2Digester(afc)
        arx2_file_list = parx2f.GetListOfArx2Files(sys.argv[1] if len(sys.argv) > 1 else arx2_file_path, afs2)
        print(list(arx2_file_list))

        for entry in list(arx2_file_list):
            parx2f.ParseArx2File(entry.path)

        end_time = time.monotonic()
        print(timedelta(seconds=end_time - start_time))

    except Exception as ex:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback)


if __name__ == '__main__':
    main()
   #cp.run('main()')

