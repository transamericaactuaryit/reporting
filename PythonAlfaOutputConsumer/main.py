import os
import HdfsUtilities as hdfsu
import io
import sys
import http
import pprint
import pyodbc
import py4j
import textwrap
import traceback
import zipfile as zip
import cProfile as cp
import numpy as np
import pandas as pd
import openpyxl as pxl
import HadoopUtilities as hu
import TapUtilities as tap
import AlfaUtilities as au
import Constants as const
from datetime import (timedelta, datetime)
from pathlib import Path
from AlfaFileSet import AlfaFileSet
from PandasUtilities import PandasUtilities
from PythonAlfaOutputConsumer import PythonAlfaOutputConsumer


def main():
    try:
        input("Press any key to continue ...")

        paoc = PythonAlfaOutputConsumer()
        paoc.set_config_param(const.ALFA_INPUT_PATH, const.ALFA_INPUT_PATH)
        paoc.set_config_param(const.PROCESSED_TS, datetime.utcnow())
        paoc.process_am1_files()
        paoc.run_arx2_code()
        #paoc.odbc_insert_example()

        #hdu = hdfsu.HdfsUtilities()
        #print(hdu.run_cmd(['hdfs', 'dfs', '-ls', '/projects/actuarialresultsdev/db']))

        #hdp = hu.HadoopUtilities()
        #hdp.drop_table(const.TableNames.SUM_BY_COHORT.value)
        #hdp.create_table(const.TableNames.SUM_BY_COHORT.value, const.TableNames.BOP_PARALLEL.value)
        #hdp.append_table(const.TableNames.SUM_BY_COHORT.value, const.TableNames.BASE.value)
        #hdp.append_table(const.TableNames.SUM_BY_COHORT.value, const.TableNames.CROSS_ATTRIBUTION.value)
        #hdp.append_table(const.TableNames.SUM_BY_COHORT.value, const.TableNames.EQUITY_ATTRIBUTION.value)
        #hdp.append_table(const.TableNames.SUM_BY_COHORT.value, const.TableNames.NB_6PT.value)
        #hdp.append_table(const.TableNames.SUM_BY_COHORT.value, const.TableNames.RATE_ATTRIBUTION.value)

        #hdp = hu.HadoopUtilities()
        #rows = hdp.select_table(const.TableNames.SUM_BY_COHORT.value, True)
        #for row in rows:
        #    print(row)
        #    for r in row:
        #        print(r)

        #hdp = hu.HadoopUtilities()
        #sql = hdp.get_summary_sql(const.TableNames.BASE.value)
        #rows = hdp.select_stmt(sql)
        #for row in rows:
        #    print(row)
        #    for r in row:
        #        print(r)

        #hdp = hu.HadoopUtilities()
        #rows = hdp.select_table(const.TableNames.NB_6PT.value, True)
        #for row in rows:
        #    print(row)
        #    for r in row:
        #        print(r)


    except Exception as ex:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback)


if __name__ == '__main__':
    main()
    #cp.run('main()')