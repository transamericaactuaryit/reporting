import io
import sys
import traceback
from PandasUtilities import PandasUtilities
import os
import re
import string
import datetime
import uuid
import Constants as const
from pathlib import Path
import pandas as pd
from collections import OrderedDict

class AlfaUtilities:

    def __init__(self):
        pass

    def process_file(self, processed_ts, full_file_path):
        file_path_obj = Path(full_file_path)
        file_name = file_path_obj.name
        file_name_pieces = file_name.split(const.PERIOD)
        with io.open(full_file_path, "r", const.FILE_BUFFER_SIZE) as input:
            headerLine = input.readline()
            with io.open(full_file_path.lower().replace(const.AM1_FILE_EXT, const.TXT_FILE_EXT), "w", const.FILE_BUFFER_SIZE) as output:
                for line in input:
                    new_line = str(uuid.uuid4()) + const.TAB_CHAR
                    new_line += str(processed_ts) + const.TAB_CHAR
                    new_line += file_name_pieces[0] + const.TAB_CHAR
                    new_line += file_name_pieces[2] + const.TAB_CHAR
                    new_line += '2' + const.TAB_CHAR    # placeholder for alfa_run_id
                    new_line += line
                    output.write(new_line)

    ####################################################################################################
    # ARX2 Section
    ####################################################################################################

    def get_type(self, var):
        try:
            if int(var) == float(var):
                return const.DataTypes.INT.value
        except:
            try:
                float(var)
                return const.DataTypes.FLOAT.value
            except:
                return const.DataTypes.STRING.value


    def process_arx_file(self, processed_ts: datetime, full_file_path: str, black_list: list, section_titles: list):

        arx_file = Path(full_file_path)

        file_name = arx_file.name

        # "********************" means number is too big to be displayed which indicates
        # something is possibly wrong or a scenario is so extreme

        col_list = []
        rows_list = []
        new_list = []
        tmp_list = []
        title = ''
        section = ''

        p = re.compile(r'(?P<Month>(\d{1,2}))/(?P<Year>(?:\d{4}))')
        q = re.compile(r'"[^\s{1}\d{1,4}/\d{1,4}]\w+[\s*|&|(|)]*.*')


        with io.open(arx_file, "r", const.FILE_BUFFER_SIZE) as input:
            all_lines = input.readlines()

        for line in all_lines:
            if not line.startswith(tuple(black_list)):
                line = re.sub(r'""', '', line, count=0)
                line = re.sub(r'"""[-]+"', '', line, count=0)

                i = p.finditer(line)
                for m in i:
                    col_list.append(m.group())

                tmp_list = []

                if line.startswith(tuple(section_titles)):
                    section = line.replace('"', '')
                    section = section.replace('\n', '')
                    continue

                # match lines that ends with a lot of numbers
                matched_lines = re.findall(q, line)

                bq_counter = 0
                for ml in matched_lines:
                    title = ''
                    line_pieces = ml.split()

                    for item in line_pieces:
                        if self.get_type(item) == const.DataTypes.STRING.value:
                            if str(item) == '"':
                                bq_counter += 1
                                if bq_counter == 2:
                                    tmp_list.insert(0, const.NA)   # change this to be N/A instead of 0
                                    bq_counter = 0
                            elif str(item).startswith(const.ARX_BAD_VALUE):
                                tmp_list.append(const.NA)
                            elif len(title) == 0:
                                title += item.strip('"')
                            else:
                                title += (' ' + item.strip('"'))
                            pass
                        elif self.get_type(item) == const.DataTypes.FLOAT.value:
                            tmp_list.append(float(item))
                        elif self.get_type(item) == const.DataTypes.INT.value:
                            tmp_list.append(int(item))


                    tmp_list.insert(0, title)
                    tmp_list.insert(0, section)
                    rows_list.append(tmp_list)
        pass

        if len(col_list) > 0:
            arx2_col_list = list(OrderedDict((x, True) for x in col_list).keys())
            new_list = list(OrderedDict((x, True) for x in col_list).keys())
            new_list.insert(0, const.M_TITLE)
            new_list.insert(0, const.M_SECTION)


        pu = PandasUtilities()
        df = pu.create_data_frame(new_list, rows_list)


        df_dup = df[df.duplicated(keep='first')]
        df_dup2 = df.loc[df.duplicated(keep=False), :]
        #print(df_dup)
        #print(df_dup2)


        print('arx2 before = ' + str(df.shape))
        df.drop_duplicates(inplace=True)
        print('arx2 after = ' + str(df.shape))
        dft = df.transpose()


        output_file = const.ARX2_FILE_PATH + arx_file.stem + const.EXCEL_FILE_EXT
        writer = pd.ExcelWriter(output_file)
        df.to_excel(writer, index=False, sheet_name='ARX2')
        dft.to_excel(writer, header=False, sheet_name='ARX2 Transposed')
        writer.save()


        arx2_rows_list = []


        arx_output_file = const.ARX2_FILE_PATH + arx_file.stem + const.TXT_FILE_EXT
        print('Working on ' + arx_output_file)


        for row in rows_list:
            for idx, item in enumerate(arx2_col_list, start=2):
                tmp2_list = []
                tmp2_list.append(str(row[0]))
                tmp2_list.append(str(row[1]))
                tmp2_list.append(str(item))
                if str(row[idx]) != const.NA:
                    tmp2_list.append(str(row[idx]))
                else:
                    tmp2_list.append(str(0))

                arx2_rows_list.append(tmp2_list)

        df2 = pu.create_data_frame(const.ColumnNameSet.ARX2.value, arx2_rows_list)
        print('arx2 flattened before = ' + str(df2.shape))
        df2.drop_duplicates(inplace=True)
        print('arx2 flattened after = ' + str(df2.shape))
        df2.to_csv(arx_output_file, sep='\t', header=False, index=False)



    def extract_arx2_header_parameters(self, full_file_path: str):
        try:
            rawLines = []
            with open(full_file_path) as f:
                for lines in f:
                    rawLines.append(lines)

            x = rawLines

            variableList = []

            for index, i in enumerate(x):
                if re.search(r'Page: (\d+)', i):
                    a = re.search(r'(\d+)', i[::]).span()
                    variableList.append(({'Index':index}, {'Page':i[a[0]:a[1]]}))
                if re.search(r'Run Date: (\d+-\d+-\d+)', i):
                    b = re.search(r'(\d+-\d+-\d+)', i[::]).span()
                    variableList.append((index, i[b[0]:b[1]]))
                if re.search(r'Run Time: (\d+:\d+:\d+)', i):
                    c = re.search(r'(\d+:\d+:\d+)', i[::]).span()
                    variableList.append((index, i[c[0]:c[1]]))
                if re.search(r'Version: (\d+.\d+.\d+)', i):
                    d = re.search(r'(\d+.\d+.\d+)', i[::]).span()
                    variableList.append((index, i[d[0]:d[1]]))
                if re.search(r'Val Date: (\d+/\d+)', i):
                    e = re.search(r'(\d+/\d+)', i[::]).span()
                    variableList.append((index, i[e[0]:e[1]]))
                if re.search(r'Run Mode: (Monthly|Quarterly)', i):
                    f = re.search(r'(Monthly|Quarterly)', i[::]).span()
                    variableList.append((index, i[f[0]:f[1]]))
                if re.search(r'Cycles: (\d+)', i):
                    g = re.search(r'(\d+)', i[::]).span()
                    variableList.append((index, i[g[0]:g[1]]))

            print(variableList)

            pass
        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback)
