import os
import io
import sys
import http
import pprint
import time
import pyodbc
import textwrap
import Constants as const
from Constants import TableNames
from datetime import (timedelta, datetime)
from pathlib import Path


class HadoopUtilities():

    def __init__(self):
        pass


    def drop_table(self, table_name: str):
        try:
            sql = textwrap.dedent("""drop table if exists {table_name};""".format(table_name = table_name))

            conn = pyodbc.connect(const.IMPALA_DSN, autocommit = True)
            cursor = conn.cursor()
            cursor.execute(sql)
            cursor.close()
            conn.close()
        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback)
        finally:
            try:
                if cursor:
                    cursor.close()
                if conn:
                    conn.close()
            except:
                pass
        pass


    def create_table(self, dest_table: str, source_table: str):
        try:
            sql = textwrap.dedent("""create table {dest_table} as
                                     select model_name, file_run_no,
                                            as_of_date, valuation_date,
                                            dac_group, cohort_name, is_current_qtr,
                                            sum(account_value) as account_value,
                                            sum(policy_count) as policy_count,
                                            sum(cqocs_ab_benefit_amt) as cqocs_ab_benefit_amt,
                                            sum(pqocs_ab_benefit_amt) as pqocs_ab_benefit_amt,
                                            sum(cqocs_ab_fee_amt) as cqocs_ab_fee_amt,
                                            sum(pqocs_ab_fee_amt) as pqocs_ab_fee_amt,
                                            sum(cqocs_ab_reserve_amt) as cqocs_ab_reserve_amt,
                                            sum(cqocs_dn20bps_ab_reserve_amt) as cqocs_dn20bps_ab_reserve_amt,
                                            sum(pqocs_ab_reserve_amt) as pqocs_ab_reserve_amt,
                                            sum(cqocs_up20bps_ab_reserve_amt) as cqocs_up20bps_ab_reserve_amt,
                                            sum(cqocs_wb_benefit_amt) as cqocs_wb_benefit_amt,
                                            sum(pqocs_wb_benefit_amt) as pqocs_wb_benefit_amt,
                                            sum(cqocs_wb_fee_amt) as cqocs_wb_fee_amt,
                                            sum(pqocs_wb_fee_amt) as pqocs_wb_fee_amt,
                                            sum(cqocs_wb_reserve_amt) as cqocs_wb_reserve_amt,
                                            sum(cqocs_dn20bps_wb_reserve_amt) as cqocs_dn20bps_wb_reserve_amt,
                                            sum(pqocs_wb_reserve_amt) as pqocs_wb_reserve_amt,
                                            sum(cqocs_up20bps_wb_reserve_amt) as cqocs_up20bps_wb_reserve_amt,
                                            sum(cqocs_nomrgn_wb_fee_amt) as cqocs_nomrgn_wb_fee_amt,
                                            sum(cqocs_nomrgn_wb_reserve_amt) as cqocs_nomrgn_wb_reserve_amt,
                                            sum(cqocs_nomrgn_dn20bps_wb_reserve_amt) as cqocs_nomrgn_dn20bps_wb_reserve_amt,
                                            sum(cqocs_nomrgn_up20bps_wb_reserve_amt) as cqocs_nomrgn_up20bps_wb_reserve_amt,
                                            sum(cqocs_wbdb_fee_amt) as cqocs_wbdb_fee_amt,
                                            sum(cqocs_nomrgn_wbdb_fee_amt) as cqocs_nomrgn_wbdb_fee_amt
                                     from   {source_table}
                                     group  by model_name, file_run_no, as_of_date, valuation_date, dac_group, cohort_name, is_current_qtr
                                     order  by model_name, file_run_no, as_of_date, valuation_date, dac_group, cohort_name, is_current_qtr;"""
                                     .format(dest_table = dest_table, source_table = source_table))

            conn = pyodbc.connect(const.IMPALA_DSN, autocommit = True)
            cursor = conn.cursor()
            cursor.execute(sql)
            cursor.close()
            conn.close()
        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback)
        finally:
            try:
                if cursor:
                    cursor.close()
                if conn:
                    conn.close()
            except:
                pass


    def overwrite_table(self, dest_table: str, source_table: str):
        try:
            sql = textwrap.dedent("""insert overwrite table {dest_table}
                                     select model_name, file_run_no,
                                            as_of_date, valuation_date,
                                            dac_group, cohort_name, is_current_qtr,
                                            sum(account_value) as account_value,
                                            sum(policy_count) as policy_count,
                                            sum(cqocs_ab_benefit_amt) as cqocs_ab_benefit_amt,
                                            sum(pqocs_ab_benefit_amt) as pqocs_ab_benefit_amt,
                                            sum(cqocs_ab_fee_amt) as cqocs_ab_fee_amt,
                                            sum(pqocs_ab_fee_amt) as pqocs_ab_fee_amt,
                                            sum(cqocs_ab_reserve_amt) as cqocs_ab_reserve_amt,
                                            sum(cqocs_dn20bps_ab_reserve_amt) as cqocs_dn20bps_ab_reserve_amt,
                                            sum(pqocs_ab_reserve_amt) as pqocs_ab_reserve_amt,
                                            sum(cqocs_up20bps_ab_reserve_amt) as cqocs_up20bps_ab_reserve_amt,
                                            sum(cqocs_wb_benefit_amt) as cqocs_wb_benefit_amt,
                                            sum(pqocs_wb_benefit_amt) as pqocs_wb_benefit_amt,
                                            sum(cqocs_wb_fee_amt) as cqocs_wb_fee_amt,
                                            sum(pqocs_wb_fee_amt) as pqocs_wb_fee_amt,
                                            sum(cqocs_wb_reserve_amt) as cqocs_wb_reserve_amt,
                                            sum(cqocs_dn20bps_wb_reserve_amt) as cqocs_dn20bps_wb_reserve_amt,
                                            sum(pqocs_wb_reserve_amt) as pqocs_wb_reserve_amt,
                                            sum(cqocs_up20bps_wb_reserve_amt) as cqocs_up20bps_wb_reserve_amt,
                                            sum(cqocs_nomrgn_wb_fee_amt) as cqocs_nomrgn_wb_fee_amt,
                                            sum(cqocs_nomrgn_wb_reserve_amt) as cqocs_nomrgn_wb_reserve_amt,
                                            sum(cqocs_nomrgn_dn20bps_wb_reserve_amt) as cqocs_nomrgn_dn20bps_wb_reserve_amt,
                                            sum(cqocs_nomrgn_up20bps_wb_reserve_amt) as cqocs_nomrgn_up20bps_wb_reserve_amt,
                                            sum(cqocs_wbdb_fee_amt) as cqocs_wbdb_fee_amt,
                                            sum(cqocs_nomrgn_wbdb_fee_amt) as cqocs_nomrgn_wbdb_fee_amt
                                     from {source_table}
                                     group by model_name, file_run_no, as_of_date, valuation_date, dac_group, cohort_name, is_current_qtr
                                     order by model_name, file_run_no, as_of_date, valuation_date, dac_group, cohort_name, is_current_qtr;"""
                                     .format(dest_table = dest_table, source_table = source_table))

            conn = pyodbc.connect(const.IMPALA_DSN, autocommit = True)
            cursor = conn.cursor()
            cursor.execute(sql)
            cursor.close()
            conn.close()
        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback)
        finally:
            try:
                if cursor:
                    cursor.close()
                if conn:
                    conn.close()
            except:
                pass


    def append_table(self, dest_table: str, source_table: str):
        try:
            sql = textwrap.dedent("""insert into table {dest_table}
                                     select model_name, file_run_no,
                                            as_of_date, valuation_date,
                                            dac_group, cohort_name, is_current_qtr,
                                            sum(account_value) as account_value,
                                            sum(policy_count) as policy_count,
                                            sum(cqocs_ab_benefit_amt) as cqocs_ab_benefit_amt,
                                            sum(pqocs_ab_benefit_amt) as pqocs_ab_benefit_amt,
                                            sum(cqocs_ab_fee_amt) as cqocs_ab_fee_amt,
                                            sum(pqocs_ab_fee_amt) as pqocs_ab_fee_amt,
                                            sum(cqocs_ab_reserve_amt) as cqocs_ab_reserve_amt,
                                            sum(cqocs_dn20bps_ab_reserve_amt) as cqocs_dn20bps_ab_reserve_amt,
                                            sum(pqocs_ab_reserve_amt) as pqocs_ab_reserve_amt,
                                            sum(cqocs_up20bps_ab_reserve_amt) as cqocs_up20bps_ab_reserve_amt,
                                            sum(cqocs_wb_benefit_amt) as cqocs_wb_benefit_amt,
                                            sum(pqocs_wb_benefit_amt) as pqocs_wb_benefit_amt,
                                            sum(cqocs_wb_fee_amt) as cqocs_wb_fee_amt,
                                            sum(pqocs_wb_fee_amt) as pqocs_wb_fee_amt,
                                            sum(cqocs_wb_reserve_amt) as cqocs_wb_reserve_amt,
                                            sum(cqocs_dn20bps_wb_reserve_amt) as cqocs_dn20bps_wb_reserve_amt,
                                            sum(pqocs_wb_reserve_amt) as pqocs_wb_reserve_amt,
                                            sum(cqocs_up20bps_wb_reserve_amt) as cqocs_up20bps_wb_reserve_amt,
                                            sum(cqocs_nomrgn_wb_fee_amt) as cqocs_nomrgn_wb_fee_amt,
                                            sum(cqocs_nomrgn_wb_reserve_amt) as cqocs_nomrgn_wb_reserve_amt,
                                            sum(cqocs_nomrgn_dn20bps_wb_reserve_amt) as cqocs_nomrgn_dn20bps_wb_reserve_amt,
                                            sum(cqocs_nomrgn_up20bps_wb_reserve_amt) as cqocs_nomrgn_up20bps_wb_reserve_amt,
                                            sum(cqocs_wbdb_fee_amt) as cqocs_wbdb_fee_amt,
                                            sum(cqocs_nomrgn_wbdb_fee_amt) as cqocs_nomrgn_wbdb_fee_amt
                                     from {source_table}
                                     group by model_name, file_run_no, as_of_date, valuation_date, dac_group, cohort_name, is_current_qtr
                                     order by model_name, file_run_no, as_of_date, valuation_date, dac_group, cohort_name, is_current_qtr;"""
                                     .format(dest_table = dest_table, source_table = source_table))

            conn = pyodbc.connect(const.IMPALA_DSN, autocommit = True)
            cursor = conn.cursor()
            cursor.execute(sql)
            cursor.close()
            conn.close()
        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback)
        finally:
            try:
                if cursor:
                    cursor.close()
                if conn:
                    conn.close()
            except:
                pass


    def select_table(self, table_name: str, add_order_by: bool):
        try:
            tmp = 'select * from {table_name}'.format(table_name = table_name)

            if add_order_by:
                sql = textwrap.dedent(tmp + ' order by model_name, file_run_no')
            else:
                sql = textwrap.dedent(tmp)

            conn = pyodbc.connect(const.IMPALA_DSN, autocommit = True)
            cursor = conn.cursor()
            return cursor.execute(sql).fetchall()
        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback)
        finally:
            try:
                if cursor:
                    cursor.close()
                if conn:
                    conn.close()
            except:
                pass


    def get_summary_sql(self, table_name: str):
        return textwrap.dedent("""select    model_name, file_run_no,
                                            as_of_date, valuation_date,
                                            dac_group, cohort_name, is_current_qtr,
                                            sum(account_value) as account_value,
                                            sum(policy_count) as policy_count,
                                            sum(cqocs_ab_benefit_amt) as cqocs_ab_benefit_amt,
                                            sum(pqocs_ab_benefit_amt) as pqocs_ab_benefit_amt,
                                            sum(cqocs_ab_fee_amt) as cqocs_ab_fee_amt,
                                            sum(pqocs_ab_fee_amt) as pqocs_ab_fee_amt,
                                            sum(cqocs_ab_reserve_amt) as cqocs_ab_reserve_amt,
                                            sum(cqocs_dn20bps_ab_reserve_amt) as cqocs_dn20bps_ab_reserve_amt,
                                            sum(pqocs_ab_reserve_amt) as pqocs_ab_reserve_amt,
                                            sum(cqocs_up20bps_ab_reserve_amt) as cqocs_up20bps_ab_reserve_amt,
                                            sum(cqocs_wb_benefit_amt) as cqocs_wb_benefit_amt,
                                            sum(pqocs_wb_benefit_amt) as pqocs_wb_benefit_amt,
                                            sum(cqocs_wb_fee_amt) as cqocs_wb_fee_amt,
                                            sum(pqocs_wb_fee_amt) as pqocs_wb_fee_amt,
                                            sum(cqocs_wb_reserve_amt) as cqocs_wb_reserve_amt,
                                            sum(cqocs_dn20bps_wb_reserve_amt) as cqocs_dn20bps_wb_reserve_amt,
                                            sum(pqocs_wb_reserve_amt) as pqocs_wb_reserve_amt,
                                            sum(cqocs_up20bps_wb_reserve_amt) as cqocs_up20bps_wb_reserve_amt,
                                            sum(cqocs_nomrgn_wb_fee_amt) as cqocs_nomrgn_wb_fee_amt,
                                            sum(cqocs_nomrgn_wb_reserve_amt) as cqocs_nomrgn_wb_reserve_amt,
                                            sum(cqocs_nomrgn_dn20bps_wb_reserve_amt) as cqocs_nomrgn_dn20bps_wb_reserve_amt,
                                            sum(cqocs_nomrgn_up20bps_wb_reserve_amt) as cqocs_nomrgn_up20bps_wb_reserve_amt,
                                            sum(cqocs_wbdb_fee_amt) as cqocs_wbdb_fee_amt,
                                            sum(cqocs_nomrgn_wbdb_fee_amt) as cqocs_nomrgn_wbdb_fee_amt
                                   from     {table_name}
                                   group by model_name, file_run_no, as_of_date, valuation_date, dac_group, cohort_name, is_current_qtr
                                   order by model_name, file_run_no, as_of_date, valuation_date, dac_group, cohort_name, is_current_qtr;"""
                                   .format(table_name = table_name))


    def select_stmt(self, select_sql: str):
        try:
            conn = pyodbc.connect(const.IMPALA_DSN, autocommit = True)
            cursor = conn.cursor()
            return cursor.execute(select_sql).fetchall()
        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback)
        finally:
            try:
                if cursor:
                    cursor.close()
                if conn:
                    conn.close()
            except:
                pass