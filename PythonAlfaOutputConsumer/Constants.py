#!/usr/bin/python
from enum import Enum

ARX2_SOURCE_PATH = r'\\TCMNASGRIDC1/valtest/Frank.Policastro/'

class ArxPaths(Enum):
    RATE_ATTRIBUTION = 'Q317_Rate_Attribution/'
    BOND_ATTRIBUTION = 'Q317_Bond_Attribution/'
    EQUITY_ATTRIBUTION = 'Q317_Equity_Attribution/'
    POT_ATTRIBUTION = 'Q317_Pot_Attribution/'
    CROSS_ATTRIBUTION = 'Q317_Cross_Attribution/'
    VOL_ATTRIBUTION = 'Q317_Vol_Attribution/'
    GMB_SINGLE_SCENARIO = 'gmbq317_single_senario/'
    RANDOM_SAMPLES = 'Q317_Random_Samples/'
    VOBA = 'VOBA_ARX2/'

FILE_BUFFER_SIZE = 1048576

IMPALA_DSN = r'DSN=Impala DSN;'

NB6_COHORTS = 'nb6'
INFORCE_COHORTS = 'inforce'

NB6_MODEL_NAME = 'Newbusiness6pt'

PROCESSED_TS = 'processed_ts'

COL_FILE_RUN_NO = 'file_run_no'
COL_DAC_GROUP = 'dac_group'

NA = 'N/A'
ARX_BAD_VALUE = '"********************"'

NB6_COHORTS_PATH = './cohorts/cohort_names_nb6.txt'
INFORCE_COHORTS_PATH = './cohorts/cohort_names_inforce.txt'

ALFA_INPUT_PATH = './alfa_input/'
ARX2_FILE_PATH = './arx2_files/'

AM1_FILE_EXT = '._.am1'
ARX2_FILE_EXT = '.arx2'

TXT_FILE_EXT = '.txt'
ZIP_FILE_EXT = '.zip'
CSV_FILE_EXT = '.csv'
EXCEL_FILE_EXT = '.xlsx'

TAB_CHAR = '\t'
NEW_LINE = '\n'
PERIOD = '.'

M_TITLE = 'Title'
M_SECTION = 'Section'

COHORT_COLUMN_NAMES = ['file_run_no', 'dac_group', 'cohort_name']

ARX2_COLUMN_NAMES = ['section', 'title', 'time_period', 'field_value']

AM1_COLUMN_NAMES = ['row_id', 'processed_ts', 'model_name', 'file_run_no',
                    'alfa_run_id', 'segment_number', 'segment_record',
                    'alfa_plan_code', 'issue_age', 'gender', 'tax_rider',
                    'biz_unit', 'legal_entity_stat_comp', 'is_qualified',
                    'gmdb_type', 'gmib_type', 'reinsurance_type', 'cohort_group',
                    'glwb_type', 'issue_year', 'issue_month', 'is_new_business',
                    'policy_number', 'dac_group', 'category', 'valuation_date',
                    'as_of_date', 'is_current_qtr', 'run_type_1', 'run_type_2',
                    'account_value', 'withdrawal_benefit_amt',
                    'accum_benefit_amt', 'policy_count', 'separate_accnt_value',
                    'cqocs_margin_amt', 'cqocs_wb_benefit_amt',
                    'cqocs_ab_benefit_amt', 'cqocs_ab_fee_amt',
                    'cqocs_wbdb_fee_amt', 'cqocs_wb_fee_amt',
                    'cqocs_wb_reserve_amt', 'cqocs_ab_reserve_amt',
                    'pqocs_wb_benefit_amt', 'pqocs_ab_benefit_amt',
                    'pqocs_ab_fee_amt', 'pqocs_wbdb_fee_amt',
                    'pqocs_wb_fee_amt', 'pqocs_wb_reserve_amt',
                    'pqocs_ab_reserve_amt', 'cqocs_up20bps_ab_reserve_amt',
                    'cqocs_dn20bps_wb_reserve_amt', 'cqocs_dn20bps_ab_reserve_amt',
                    'cqocs_up20bps_wb_reserve_amt', 'cqocs_nomrgn_ab_fee_amt',
                    'cqocs_nomrgn_wbdb_fee_amt', 'cqocs_nomrgn_wb_fee_amt',
                    'cqocs_nomrgn_wb_reserve_amt', 'cqocs_nomrgn_ab_reserve_amt',
                    'cqocs_nomrgn_up20bps_wb_reserve_amt',
                    'cqocs_nomrgn_up20bps_ab_reserve_amt',
                    'cqocs_nomrgn_dn20bps_wb_reserve_amt',
                    'cqocs_nomrgn_dn20bps_ab_reserve_amt']

class ColumnNameSet(Enum):
    AM1 = AM1_COLUMN_NAMES
    COHORT = COHORT_COLUMN_NAMES
    ARX2 = ARX2_COLUMN_NAMES

class TableNames(Enum):
    BASE = 'am1_base_model'
    BOP_PARALLEL = 'am1_bop_parallel'
    CROSS_ATTRIBUTION = 'am1_cross_attribution'
    EQUITY_ATTRIBUTION = 'am1_equity_attribution'
    NB_6PT = 'am1_nb6pt_average'
    RATE_ATTRIBUTION = 'am1_rate_attribution'
    SUM_BY_COHORT = 'sum_by_cohort'

class DataTypes(Enum):
    STRING = 'str'
    FLOAT = 'float'
    INT = 'int'


BLACK_LIST_OF_ROWS = ['"" "--------------------"',
                        '"Scenario Summary for Module Projection',
                        '"Check1 = 0',
                        '"Check2 = 0',
                        '"Check3 = 0',
                        '"Check4 = 0',
                        '"Zero',
                        '"Check5 = 0',
                        '"Check6 = 0',
                        '"Check7 = 0',
                        '"Check8 = 0',
                        '"Check9 = 0',
                        '"TOTAL CHECK = 0"',
                        '"ULIC\'s REINSURANCE COST for 60% No LB Coin/ 100% LB Ceded to TLIC"',
                        '"Check', # might not work...
                        '"(Detail not shown in reports above so all profit rerpot are out of balance by cost amount)"',
                        ' "F133 PV GMWB Benefit with OCS (INCREASE)"',
                        ' "F133 PV GMAB Benefit with OCS (INCREASE)"',
                        ' "F133 PV GMWB Fee with OCS (INCREASE)"',
                        ' "F133 PV GMAB Fee with OCS (INCREASE)"',
                        ' "F133 PV GMWB/AB Fee with OCS (INCREASE)"',
                        ' "F133 PV GMWB ROP DB Fee with OCS (INCREASE)"',
                        ' "F133 PV GMWB Reserve with OCS (INCREASE)"',
                        ' "F133 PV GMAB Reserve with OCS (INCREASE)"',
                        '"                                                   Description:  U71 FAS 97 Report 5/13/2015 (Updated 7/14/2015) - TARe Only"',
                        '"                                                 Description:  U69 Living Benefit Streams (with Accr Fee 5/7/2015) - TARe Only"',
                        '"VIA GAIN"',
                        '"Living Benefit Streams"']

SECTION_TITLES = ['"ASSESSMENTS & CHARGES FOR FAS97 DAC UNLOCKING and MISCELLANEOUS INFO',
                  '"DIRECT POLICYHOLDER BENEFITS PAID',
                  '"SOP FOR FAS97 DAC UNLOCKING',
                  '"VARIOUS STATISTICS',
                  '"POLICY EXHIBIT LIVES',
                  '"RIDER INFORCE STATISTICS',
                  '"TOTAL ACCOUNT VALUE ROLLFORWARD',
                  '"EXPENSES"',
                  '"REINSURANCE CEDED REPORT',
                  '"FREE SURPLUS INFO for CAPITAL & RESERVES PROJECTIONS Only"',
                  '"REINSURANCE REPORT',
                  '"ML IB REINS AGG CAP"',
                  '"ML DB REINS AGG CAP"',
                  '"HEDGE COST RECOGNITION FOR DAC"',
                  '"Financial Plan Stat Income Statement"',
                  '"Financial Plan GA Rollforward & Stat Balance Sheet"',
                  '"Financial Plan SA Rollforward"',
                  '"Financial Plan Misc Output"',
                  '"CASH FLOW - STATUTORY STATEMENT"',
                  '"STATUTORY INCOME STATEMENT"',
                  '"GA STATUTORY ASSETS"',
                  '"GA STATUTORY LIABILITIES"',
                  '"GA STATUTORY SURPLUS"',
                  '"GA & SA RESERVE and IMR INFO"',
                  '"SEPARATE ACCOUNT VALUE ROLLFORWARD"',
                  '"GENERAL ACCOUNT VALUE ROLLFORWARD"',
                  '"C3P2 Standard Scenario"',
                  '"FAS97 2004"',
                  '"FAS97 2004 A"',
                  '"FAS97 2004 B"',
                  '"GMIB"',
                  '"GMWB"',
                  '"GMAB"',
                  '"TAXPAYER"',
                  '"INFO ONLY"']