import datetime

class AlfaFileSet:
    # description of class

    def __init__(self, user_name, model_name):
        print('Initializing AlfaFileSet object')
        self.time_stamp = datetime.datetime.utcnow()
        self.user_name = user_name
        self.model_name = model_name
        self.file_name_map = {}
        self.row_count_by_file = {}
        self.file_count = 0
        self.row_count = 0
        self.column_sums = {}
        self.original_header_row = ''
        self.valuation_date = ''
        self.as_of_date = ''
        self.alfa_run_id = 0
        self.arx2_file_names = {}
        self.arx2_file_count = 0

    def __str__(self):
        return ('user_name = {0}, \n'
                'model_name = {1}, \n'
                'time_stamp = {2}, \n'
                'file_name_map = {3}, \n'
                'row_count_by_file = {4}, \n'
                'file_count = {5}, \n'
                'row_count = {6}, \n'
                'column_sums = {7}, \n'
                'original_header_row = {8}, \n'
                'valuation_date = {9}, \n'
                'as_of_date = {10}, \n'
                'alfa_run_id = {11}, \n'
                'arx2_file_names = {12}, \n'
                'arx2_file_count = {13}, \n').format(self.user_name,
                                                     self.model_name,
                                                     self.time_stamp,
                                                     self.file_name_map,
                                                     self.row_count_by_file,
                                                     self.file_count,
                                                     self.row_count,
                                                     self.column_sums,
                                                     self.original_header_row,
                                                     self.valuation_date,
                                                     self.as_of_date,
                                                     self.alfa_run_id,
                                                     self.arx2_file_names,
                                                     self.arx2_file_count)

