import pandas as pd
from collections import OrderedDict as od
import pandasql as psql
import time
from datetime import datetime as dt


class Am1_Factory:

    def __init__(self, fileToProcess):
        self.fileToProcess = fileToProcess

    def clean_AM1_Header(self):
        f = open(self.fileToProcess)
        for index, i in enumerate(f.readlines()):
            if index == 0:
                noPeriod = i.replace(".", "")
                noPeriodNoForwardSlash = noPeriod.replace("/", "")
                headerList = noPeriodNoForwardSlash.strip().split("\t")
                return headerList
        f.close()

    def create_AM1_Dictionary(self):
        dic = od()
        header = self.clean_AM1_Header()
        for item in header:
            dic.update({item: []})
        f = open(self.fileToProcess)
        fr = f.readlines()
        for cIndex, col in enumerate(header):
            for rIndex, row in enumerate(fr):
                tempRowList = row.strip().split("\t")
                if rIndex > 0:
                    dic[col].append(tempRowList[cIndex])
        f.close()
        return dic

    def createDataFrame(self, dict):
        df = pd.DataFrame.from_dict(dict)
        return df

    # Decorator function
    def timeit(sqlMethod):
        def wrapper(*args, **kw):
            ts = time.time()
            result = sqlMethod(*args, **kw)
            te = time.time()
            tte =  ('%2.2f ms' % \
                  ((te - ts) * 1000))
            methodName = sqlMethod.__name__
            return {'method':result, 'timeToExecute':tte, 'methodName':methodName}
        return wrapper

    @timeit
    def sqlAgeFunction(self, dataFrame):
        query = """
            SELECT
                 df.ckIssAge AS Age
                ,COUNT(1) AS CountOccurance
            FROM dataFrame AS df
            GROUP BY ckIssAge
            ORDER BY COUNT(1) DESC;
        """
        return psql.sqldf(query, locals())

    @timeit
    def sqlSelectFunction(self, dataFrame):
        query = """
            SELECT
                 PolicyNumber, ckIssAge
            FROM dataFrame AS df
        """
        return psql.sqldf(query, locals())

    @timeit
    def sqlGenderFunction(self, dataFrame):
        query = """
            SELECT
                 ckGender, COUNT(1) AS UnitCount
            FROM dataFrame AS df
            GROUP BY ckGender
            ORDER BY COUNT(1) DESC
        """
        return psql.sqldf(query, locals())


def Main():
    root = 'C:\\Users\\jwilliamson\\Desktop\\AM1\\Base.Proj.006.Run.006.Rreq.032.Inv015.Scn.Mean._.Am1'
    a = Am1_Factory(root)
    d = a.create_AM1_Dictionary()
    df = a.createDataFrame(d)
    sdf = a.sqlAgeFunction(df)
    sdf2 = a.sqlSelectFunction(df)
    sdf3 = a.sqlGenderFunction(df)
    dateStamp = dt.now().strftime("%B_%d_%Y_%H_%M_%S")
    destinationFileRaw = 'C:\\Users\\jwilliamson\\Desktop\\Am1_Raw_' + dateStamp + '.csv'
    destinationFile = 'C:\\Users\\jwilliamson\\Desktop\\Am1_Age_Frequency_' + dateStamp + '.csv'
    destinationFile2 = 'C:\\Users\\jwilliamson\\Desktop\\Am1_Select_' + dateStamp + '.csv'
    destinationFile3 = 'C:\\Users\\jwilliamson\\Desktop\\Am1_Gender_' + dateStamp + '.csv'
    df.to_csv(destinationFileRaw, sep=',')
    sdf['method'].to_csv(destinationFile, sep=',')
    print(sdf['methodName']+':', sdf['timeToExecute'])
    sdf2['method'].to_csv(destinationFile2, sep=',')
    print(sdf2['methodName']+':', sdf2['timeToExecute'])
    sdf3['method'].to_csv(destinationFile3, sep=',')
    print(sdf3['methodName']+':', sdf3['timeToExecute'])


if __name__ == '__main__':
    Main()

