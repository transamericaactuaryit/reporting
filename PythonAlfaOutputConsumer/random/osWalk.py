import os
from collections import OrderedDict as od


class Am1_Factory:

    def __init__(self, fileToProcess):
        self.fileToProcess = fileToProcess

    def clean_AM1_Header(self):
        f = open(self.fileToProcess)
        for index, i in enumerate(f.readlines()):
            if index == 0:
                noPeriod = i.replace(".", "")
                noPeriodNoForwardSlash = noPeriod.replace("/", "")
                headerList = noPeriodNoForwardSlash.strip().split("\t")
                return headerList

    def create_AM1_Dictionary(self):
        dic = od()
        header = self.clean_AM1_Header()
        for item in header:
            dic.update({item: []})
        f = open(self.fileToProcess)
        fr = f.readlines()
        for cIndex, col in enumerate(header):
            for rIndex, row in enumerate(fr):
                tempRowList = row.strip().split("\t")
                if rIndex > 0:
                    dic[col].append(tempRowList[cIndex])
        return dic

    def create_AM1_Age_Statistic(self, am1Dict):
        try:
            statDict = {}
            statDict.update({'AverageAge': []})
            recordCountAge = 0
            sumRecordAge = 0
            for k, v in am1Dict.items():
                if k == 'ckIssAge':
                    for val in v:
                        recordCountAge += 1
                        sumRecordAge += int(val)
            statDict['AverageAge'].append(str(sumRecordAge / recordCountAge))
            return statDict
        except Exception as e:
            print(e)


def Main():
    rootDir = 'C:\\Users\\jwilliamson\\Desktop\AM1'
    for dirName, subdirList, fileList in os.walk(rootDir, topdown=True):
        for file in fileList:
            if file.endswith('.Am1'):
                cc = Am1_Factory(os.path.join(dirName, file))
                resultDict = cc.create_AM1_Age_Statistic(cc.create_AM1_Dictionary())
                print('\n')
                print(os.path.join(dirName, file))
                for k, v in resultDict.items():
                    if k == 'AverageAge':
                        print(k, v)
                if float(resultDict['AverageAge'][0]) > 61.9:
                    print('\n'+'***Passed (Average age > 61.9)***'+'\n')
                else:
                    print('\n'+'***Failed (Average age <= 61.9)***'+'\n')


if __name__ == '__main__':
    Main()