﻿SELECT *
FROM dbo.Measure_Name AS mn
INNER JOIN dbo.Measure_Value AS mv ON
  mn.Id = mv.MN_Id
WHERE Section = 'ASSESSMENTS & CHARGES FOR FAS97 DAC UNLOCKING and MISCELLANEOUS INFO'