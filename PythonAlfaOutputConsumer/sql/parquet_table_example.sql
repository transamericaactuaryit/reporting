DROP TABLE IF EXISTS arx2_version_3;
CREATE EXTERNAL TABLE IF NOT EXISTS arx2_version_3 (
		-- Placeholder for composite primary key columns
		row_id									string,
		file_proj_no                            string,
		file_run_no								string
	)	
PARTITIONED BY ( model_name STRING, section STRING, title STRING, processed_ts TIMESTAMP )	
ROW FORMAT SERDE 'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
WITH SERDEPROPERTIES ('field.delim'='\t','serialization.format'='\t')
STORED AS INPUTFORMAT 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION 'hdfs://nameservice1//projects/actuarialresultsdev/db/arx2_version_3'
TBLPROPERTIES ('parquet.compression'='SNAPPY');



DROP TABLE dcp75directsegmentgb_role_identification;
CREATE EXTERNAL TABLE dcp75directsegmentgb_role_identification
(
      uuid                                                        string,
      company_code                                                string,
      directory_id                                                string,
      segment_id                                                  string,
      segment_sequence_number                                     bigint,
      occurs_index                                                bigint,
      master_number                                               string,
      role_code                                                   string,
      time_stamp                                                  string
)
PARTITIONED BY ( load_date STRING, batchid STRING )
ROW FORMAT SERDE 'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
WITH SERDEPROPERTIES ('field.delim'='|','serialization.format'='|')
STORED AS INPUTFORMAT 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION '/projects/enterprisedatalakedev/db/enterprisedatacleansed/adminsystem/p75/directsegmentgb_role_identification'
TBLPROPERTIES ('parquet.compression'='SNAPPY');