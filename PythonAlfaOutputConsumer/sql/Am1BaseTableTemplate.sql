DROP TABLE IF EXISTS arx2_base_model;

CREATE EXTERNAL TABLE IF NOT EXISTS arx2_base_model (
	-- Placeholder for composite primary key columns
	row_id									INT,
	model_name								VARCHAR(50),
	file_run_no								VARCHAR(5),
	section      							VARCHAR(200),
	title    								VARCHAR(200),
	time_period								VARCHAR(20),
	field_value								VARCHAR(50)
)	
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
WITH SERDEPROPERTIES ('field.delim'='\t','serialization.format'='\t')
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION 'hdfs://nameservice1//projects/actuarialresultsdev/db/arx2_base_model';

--ROW FORMAT DELIMITED
--FIELDS TERMINATED BY '\t'
--STORED AS TEXTFILE;

--PARTITION (segment_number VARCHAR(10), segment_record VARCHAR(10), policy_number VARCHAR(50))
--ROW FORMAT DELIMITED 
--	FIELDS TERMINATED BY '\t'
--	--COLLECTION ITEMS TERMINATED BY '\t'
--	--MAP KEYS TERMINATED BY ':'
--	LINES TERMINATED BY '\n'
--STORED AS TEXTFILE;

--PARTITION (year INT, month TINYINT, day TINYINT)
--ROW FORMAT DELIMITED
--FIELDS TERMINATED BY '\t'
--STORED AS PARQUET;
--set COMPRESSION_CODEC=snappy;

--LOAD DATA LOCAL INPATH '/user/fpolicastro/' OVERWRITE INTO TABLE am1_base_table;

SELECT * FROM am1_base_model;

DROP TABLE IF EXISTS q218_rate_am1;

CREATE EXTERNAL TABLE IF NOT EXISTS q218_rate_am1 (
		-- Placeholder for composite primary key columns
		row_id									STRING,
		processed_ts							TIMESTAMP,
		model_name								STRING,
		file_proj_no                            STRING,
		file_run_no								STRING,
		alfa_run_id								INT,
		segment_number							STRING,
		segment_record							STRING,
		alfa_plan_code							STRING,
		issue_age								INT,
		gender									STRING,
		tax_rider								STRING,
		biz_unit								STRING,
		legal_entity_stat_comp					STRING,
		is_qualified							STRING,
		gmdb_type								STRING,
		gmib_type								STRING,
		reinsurance_type						STRING,
		cohort_group							STRING,           
		glwb_type								STRING,    
		issue_year								INT,
		issue_month								INT,
		is_new_business							STRING,
		policy_number							STRING,
		cohort_name                             STRING,
		dac_group								STRING,
		valuation_date							STRING,
		as_of_date								STRING,		
		is_current_qtr							STRING,
		account_value							DECIMAL(30, 8),
		withdrawal_benefit_amt					DECIMAL(30, 8),
		accum_benefit_amt						DECIMAL(30, 8),
		policy_count							DECIMAL(30, 8),
		separate_accnt_value					DECIMAL(30, 8),
		cqocs_margin_amt						DECIMAL(30, 8),
		cqocs_wb_benefit_amt					DECIMAL(30, 8),
		cqocs_ab_benefit_amt					DECIMAL(30, 8),
		cqocs_ab_fee_amt						DECIMAL(30, 8),
		cqocs_wbdb_fee_amt						DECIMAL(30, 8),
		cqocs_wb_fee_amt						DECIMAL(30, 8),
		cqocs_wb_reserve_amt					DECIMAL(30, 8),
		cqocs_ab_reserve_amt					DECIMAL(30, 8),
		pqocs_wb_benefit_amt					DECIMAL(30, 8),
		pqocs_ab_benefit_amt					DECIMAL(30, 8),
		pqocs_ab_fee_amt						DECIMAL(30, 8),
		pqocs_wbdb_fee_amt						DECIMAL(30, 8),
		pqocs_wb_fee_amt						DECIMAL(30, 8),
		pqocs_wb_reserve_amt					DECIMAL(30, 8),
		pqocs_ab_reserve_amt					DECIMAL(30, 8),
		cqocs_up20bps_ab_reserve_amt			DECIMAL(30, 8),
		cqocs_dn20bps_wb_reserve_amt			DECIMAL(30, 8),
		cqocs_dn20bps_ab_reserve_amt			DECIMAL(30, 8),
		cqocs_up20bps_wb_reserve_amt			DECIMAL(30, 8),
		cqocs_nomrgn_ab_fee_amt					DECIMAL(30, 8),
		cqocs_nomrgn_wbdb_fee_amt				DECIMAL(30, 8),
		cqocs_nomrgn_wb_fee_amt					DECIMAL(30, 8),
		cqocs_nomrgn_wb_reserve_amt				DECIMAL(30, 8),
		cqocs_nomrgn_ab_reserve_amt				DECIMAL(30, 8),
		cqocs_nomrgn_up20bps_wb_reserve_amt		DECIMAL(30, 8),
		cqocs_nomrgn_up20bps_ab_reserve_amt		DECIMAL(30, 8),
		cqocs_nomrgn_dn20bps_wb_reserve_amt		DECIMAL(30, 8),
		cqocs_nomrgn_dn20bps_ab_reserve_amt		DECIMAL(30, 8)
	)	
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
WITH SERDEPROPERTIES ('field.delim'='\t','serialization.format'='\t')
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION 'hdfs://nameservice1//projects/actuarialresultsdev/db/q218_rate_am1'
TBLPROPERTIES ('parquet.compression'='SNAPPY');


DROP TABLE IF EXISTS arx2_base_model;

CREATE EXTERNAL TABLE IF NOT EXISTS arx2_base_model (
	-- Placeholder for composite primary key columns
	section      							VARCHAR(300),
	title    								VARCHAR(300),
	t                                       INT,
	month_val                               INT,
	year_val                                INT,
	field_value								DECIMAL(30, 8)
)	
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
WITH SERDEPROPERTIES ('field.delim'='\t','serialization.format'='\t')
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION 'hdfs://nameservice1//projects/actuarialresultsdev/db/arx2_base_model';