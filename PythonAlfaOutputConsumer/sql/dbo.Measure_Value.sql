﻿CREATE TABLE [dbo].[Measure_Value]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [MN_Id] INT NOT NULL, 
    [Time_Period] VARCHAR(20) NOT NULL, 
    [Amount] VARCHAR(50) NOT NULL
)
