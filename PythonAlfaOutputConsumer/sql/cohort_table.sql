DROP TABLE IF EXISTS cohort_names_inforce;

CREATE EXTERNAL TABLE IF NOT EXISTS cohort_names_inforce (
		-- Placeholder for composite primary key columns
		run_no									VARCHAR(3),		
		dac_group								VARCHAR(10),
		cohort_name 							VARCHAR(100)
	)	
	ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
	WITH SERDEPROPERTIES ('field.delim'='\t','serialization.format'='\t')
	STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
	OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
	LOCATION 'hdfs://nameservice1//projects/actuarialresultsdev/db/cohort_names_inforce';


DROP TABLE IF EXISTS cohort_names_nb6;

CREATE EXTERNAL TABLE IF NOT EXISTS cohort_names_nb6 (
		-- Placeholder for composite primary key columns
		run_no									VARCHAR(3),		
		dac_group								VARCHAR(10),
		cohort_name 							VARCHAR(100)
	)	
	ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
	WITH SERDEPROPERTIES ('field.delim'='\t','serialization.format'='\t')
	STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
	OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
	LOCATION 'hdfs://nameservice1//projects/actuarialresultsdev/db/cohort_names_nb6';



DROP TABLE IF EXISTS cohort_names;

CREATE EXTERNAL TABLE IF NOT EXISTS cohort_names (
		-- Placeholder for composite primary key columns
		run_no									STRING,
		dac_group_code							STRING,
		cohort_model                            STRING,
		cohort_name 							STRING
	)	
	ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
	WITH SERDEPROPERTIES ('field.delim'='\t','serialization.format'='\t')
	STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
	OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
	LOCATION 'hdfs://nameservice1//projects/actuarialresultsdev/db/cohort_names';