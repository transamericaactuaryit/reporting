#!/opt/cloudera/parcels/Anaconda-4.0.0/bin/python
import io
import datetime
import uuid
import os
import sys
import time
import json
import traceback
import subprocess
import pandas as pd
import numpy as np
import Constants as const
from UnixCmdExecutor import *
from PandasUtilities import *
from pyspark import SparkContext, SparkConf, HiveContext
from pyspark.sql import SQLContext, SparkSession, Row
from pyspark.sql.functions import *
from pyspark.sql.types import *

class ysera_base(object):
    """description of class"""

    #region setup spark session
    spark = SparkSession.builder \
                        .enableHiveSupport() \
                        .config("spark.kryoserializer.buffer.max", "2047") \
                        .getOrCreate()

    sc    = spark.sparkContext

    spark.sql('set hive.exec.parallel=true')
    spark.sql('set hive.exec.dynamic.partition.mode=nonstrict')
    spark.sql('set hive.exec.dynamic.partition=true')
    #endregion setup spark session


    #region constructor
    def __init__(self):
        with open('config.json') as json_data_file:
            config_data = json.load(json_data_file)

        self.db_name = config_data['db_name']
        self.hdfs_path = config_data['hdfs_path']
        self.landing_path = config_data['landing_path']
        self.parquet_path = config_data['parquet_path']
        self.cohort_name_table = config_data['cohort_name_table']

        self.am1_table_name = config_data['am1_config']['am1_table_name']
        self.am1_file_ext = config_data['am1_config']['am1_file_ext']
        self.am1_source_path = config_data['am1_config']['am1_source_path']

        self.arx2_table_name = config_data['arx2_config']['arx2_table_name']
        self.arx2_file_ext = config_data['arx2_config']['arx2_file_ext']
        self.arx2_source_path = config_data['arx2_config']['arx2_source_path']

        self.pu = PandasUtilities()
        self.uce = UnixCmdExecutor()
        self.cohorts = ysera_base.spark.read.table(str(self.db_name + '.' + self.cohort_name_table))
        pass
    #endregion constructor


    #region get_uuid function
    def get_uuid(self):
        return str(uuid.uuid4())
    #endregion get_uuid function


