﻿SELECT   model_name, file_proj_no, file_run_no, 
         as_of_date, valuation_date,
	     cohort_name, is_current_qtr,
         
	     sum(policy_count) as policy_count,
	     sum(account_value) as account_value,       
         
         sum(cqocs_wb_reserve_amt) as cqocs_wb_reserve_amt,
	     sum(pqocs_wb_reserve_amt) as pqocs_wb_reserve_amt,
         sum(cqocs_nomrgn_wb_reserve_amt) as cqocs_nomrgn_wb_reserve_amt,
	     sum(cqocs_up20bps_wb_reserve_amt) as cqocs_up20bps_wb_reserve_amt,
         sum(cqocs_nomrgn_up20bps_wb_reserve_amt) as cqocs_nomrgn_up20bps_wb_reserve_amt,
	     sum(cqocs_dn20bps_wb_reserve_amt) as cqocs_dn20bps_wb_reserve_amt,
	     sum(cqocs_nomrgn_dn20bps_wb_reserve_amt) as cqocs_nomrgn_dn20bps_wb_reserve_amt,
	     
	     sum(cqocs_wb_benefit_amt) as cqocs_wb_benefit_amt,
         sum(pqocs_wb_benefit_amt) as pqocs_wb_benefit_amt,	
         
	     sum(cqocs_wb_fee_amt) as cqocs_wb_fee_amt,
         sum(pqocs_wb_fee_amt) as pqocs_wb_fee_amt,
         sum(cqocs_nomrgn_wb_fee_amt) as cqocs_nomrgn_wb_fee_amt,       	   
         
         sum(cqocs_wbdb_fee_amt) as cqocs_wbdb_fee_amt,     
         sum(cqocs_nomrgn_wbdb_fee_amt) as cqocs_nomrgn_wbdb_fee_amt,
	     
	     sum(cqocs_ab_reserve_amt) as cqocs_ab_reserve_amt,
	     sum(pqocs_ab_reserve_amt) as pqocs_ab_reserve_amt,
	     sum(cqocs_up20bps_ab_reserve_amt) as cqocs_up20bps_ab_reserve_amt,
         sum(cqocs_dn20bps_ab_reserve_amt) as cqocs_dn20bps_ab_reserve_amt,
	     
         sum(cqocs_ab_benefit_amt) as cqocs_ab_benefit_amt,
         sum(pqocs_ab_benefit_amt) as pqocs_ab_benefit_amt,
	     
	     sum(cqocs_ab_fee_amt) as cqocs_ab_fee_amt,
         sum(pqocs_ab_fee_amt) as pqocs_ab_fee_amt
         
FROM     actuarialresultsdev.pfv_am1
WHERE    model_name = 'Base'
GROUP BY model_name, file_proj_no, file_run_no, as_of_date, valuation_date, cohort_name, is_current_qtr
ORDER BY model_name, file_proj_no, file_run_no, as_of_date, valuation_date, cohort_name, is_current_qtr;