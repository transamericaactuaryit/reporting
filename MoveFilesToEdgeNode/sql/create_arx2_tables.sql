﻿DROP TABLE IF EXISTS actuarialresultsdev.lfv_arx2;
CREATE EXTERNAL TABLE IF NOT EXISTS actuarialresultsdev.lfv_arx2
(
	t                                          INT,
	month_val                                  INT,
	year_val                                   INT,
	discount_factor_before_tax                 DECIMAL(30, 8),
	discount_factor_with_OCS                   DECIMAL(30, 8),
	GMIB_reins_premium                         DECIMAL(30, 8),
	GMIB_reins_benefit_after_aggregate_limit   DECIMAL(30, 8),
	lives_inforce_GMIB_reins_only              DECIMAL(30, 8),
	fund_value_inforce_GMIB_reins_only         DECIMAL(30, 8),	
	row_id									   STRING
)	
PARTITIONED BY 
( 
	model_name                              STRING, 
	file_run_no                             STRING, 
	load_date                               STRING, 
	load_time                               STRING 
)
ROW FORMAT SERDE "org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe"
WITH SERDEPROPERTIES ("field.delim"="\t","serialization.format"="\t")
STORED AS INPUTFORMAT "org.apache.hadoop.mapred.TextInputFormat"
OUTPUTFORMAT "org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat"
LOCATION "/projects/actuarialresultsdev/db/landing/variable_annuities/fair_value/fv_arx2";



DROP TABLE IF EXISTS actuarialresultsdev.pfv_arx2;
CREATE EXTERNAL TABLE IF NOT EXISTS actuarialresultsdev.pfv_arx2
(	
	t                                          INT,
	month_val                                  INT,
	year_val                                   INT,
	discount_factor_before_tax                 DECIMAL(30, 8),
	discount_factor_with_OCS                   DECIMAL(30, 8),
	GMIB_reins_premium                         DECIMAL(30, 8),
	GMIB_reins_benefit_after_aggregate_limit   DECIMAL(30, 8),
	lives_inforce_GMIB_reins_only              DECIMAL(30, 8),
	fund_value_inforce_GMIB_reins_only         DECIMAL(30, 8),	
	row_id									   STRING
)	
PARTITIONED BY 
( 
	model_name                              STRING, 
	file_run_no                             STRING, 
	load_date                               STRING, 
	load_time                               STRING 
)
ROW FORMAT SERDE "org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe"
WITH SERDEPROPERTIES ("field.delim"="\t","serialization.format"="\t", "serialization.null.format"="''")
STORED AS INPUTFORMAT "org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat"
OUTPUTFORMAT "org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat"
LOCATION "/projects/actuarialresultsdev/db/enterpriseready/variable_annuities/fair_value/fv_arx2"
TBLPROPERTIES ("parquet.compression"="SNAPPY");