﻿SELECT   model_name, 
         file_run_no, 
         section, 
         title, 
         t, 
         month_val, 
         year_val, 
         field_val
FROM     pfv_arx2
WHERE    model_name = 'Rateattribution'
AND      file_run_no = '021'
AND      title in ('Lives Inforce (GMIB Reins only)', 
                   'Fund Value Inforce (GMIB Reins only)', 
                   'GMIB Reinsurance Premium', 
                   'GMIB Reins Benefit after Aggregate Limit')
ORDER BY model_name, file_run_no, section, title, t, month_val, year_val;