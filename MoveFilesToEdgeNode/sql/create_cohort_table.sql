﻿DROP TABLE IF EXISTS cohort_names;

CREATE EXTERNAL TABLE IF NOT EXISTS cohort_names 
(		
	proj_no									STRING,
	dac_group_code							STRING,
	cohort_model                            STRING,
	cohort_name 							STRING
)	
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
WITH SERDEPROPERTIES ('field.delim'='\t','serialization.format'='\t')
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION 'hdfs://nameservice1//projects/actuarialresultsdev/db/cohort_names';