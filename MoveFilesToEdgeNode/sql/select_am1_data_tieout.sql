﻿SELECT   model_name, file_run_no, as_of_date, valuation_date,
	     dac_group, cohort_name, is_current_qtr,
	     sum(policy_count) as policy_count,
	     sum(account_value) as account_value,
	     sum(cqocs_wb_reserve_amt) as wb_reserve,
	     sum(cqocs_nomrgn_wb_reserve_amt) as wb_economic_reserve,
	     sum(cqocs_ab_reserve_amt) as ab_reserve,
	     sum(cqocs_nomrgn_ab_reserve_amt) as ab_economic_reserve, 
	     sum(cqocs_wb_reserve_amt + cqocs_ab_reserve_amt) as total_reserves
FROM     pfv_am1
WHERE    model_name = 'Rateattribution'
GROUP BY model_name, file_run_no, as_of_date, valuation_date, dac_group, cohort_name, is_current_qtr
ORDER BY model_name, file_run_no, as_of_date, valuation_date, dac_group, cohort_name, is_current_qtr;