#!/opt/cloudera/parcels/Anaconda-4.0.0/bin/python
import sys
import time
import traceback
import pandas
from sparkling import Constants as const
from sparkling.alfa import am1_utility
from sparkling.alfa import am1_recon_report
from sparkling.alfa.fairvalue import fv_arx2_utility
from sparkling.alfa.fairvalue import fv_arx2_recon_report
from sparkling.flaskapi import flask_api


class MainMethod():


    def __init__(self):
        pass


    def run_am1_demo(self):
        load_date = time.strftime("%m-%d-%Y")
        load_time = time.strftime("%H.%M.%S")
        s_am1 = am1_utility.am1_utility(load_date=load_date, load_time=load_time)
        s_am1.clean_up_landing_area()
        s_am1.land_files(str(s_am1.am1_source_path + s_am1.am1_file_ext), str(s_am1.hdfs_path + s_am1.landing_path))
        rows = s_am1.parse_file_names()
        for r in rows:
            s_am1.process_file(r)
        s_am1.persist_data()

        #s_am1.am1_all = pandas.concat(s_am1.pd_list)
        print('s_am1 = ' + str(len(s_am1.am1_all)))
        print('s_am1[0:4] = ' + str(s_am1.am1_all.head(5)))

    def run_arx2_demo(self):
        load_date = time.strftime("%m-%d-%Y")
        load_time = time.strftime("%H.%M.%S")
        s_arx2 = fv_arx2_utility.fv_arx2_utility(load_date=load_date, load_time=load_time)
        s_arx2.clean_up_landing_area()
        s_arx2.land_files(str(s_arx2.arx2_source_path + s_arx2.arx2_file_ext), str(s_arx2.hdfs_path + s_arx2.landing_path))
        rows = s_arx2.parse_file_names()
        for r in rows:
            s_arx2.process_file(r)


    def run_report(self):
        rpt = am1_recon_report.am1_recon_report()
        #rpt.run_report()
        #rpt.run_report(model_name='Rateattribution')
        rpt.run_report(table_name='actuarialresultsdev.tp_tfv_am1', model_name='Base')


    def run_report_arx2(self):
        rpt = fv_arx2_recon_report.fv_arx2_recon_report()
        rpt.run_report(model_name='Base')


def main():
    try:
        #f = flask_api.main()
        mm = MainMethod()
        mm.run_am1_demo()
        #mm.run_arx2_demo()
        mm.run_report()
        #mm.run_report_arx2()
    except Exception as ex:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback)


if __name__ == '__main__':
    main()
