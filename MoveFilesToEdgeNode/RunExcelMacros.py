import win32com.client as w
from os import path as pt
from collections import OrderedDict as orderedDictionary

# This dictionary mimics the key value pairs of a config file
fileDictionary = orderedDictionary()
fileDictionary.update({"C:\\Users\\jwilliamson\\Desktop\\MacroTestEnvironment\\TestMacro2.xlsm": "TestMacro2"})
fileDictionary.update({"C:\\Users\\jwilliamson\\Desktop\\MacroTestEnvironment\\TestMacro3.xlsm": "TestMacro3"})
fileDictionary.update({"C:\\Users\\jwilliamson\\Desktop\\MacroTestEnvironment\\TestMacro4.xlsm": "TestMacro4"})

def runExcelMacro(filePath, macroName):
    try:
        xl = w.DispatchEx("Excel.Application")
        wb = xl.workbooks.open(filePath)
        xl.Visible = False
        xl.run(macroName)
        wb.Save()
        wb.Close()
        xl.Quit()
        # Clean up
        del xl
        # parse file name for log
        fileName = str(pt.split(filePath)[1])
        # Print success to console
        print("Macro run and " + fileName + " saved")
    except Exception as e:
        print(e)

def processMacroStack(configKeyValuePairs):
    try:
        for k, v in configKeyValuePairs.items():
            runExcelMacro(k, v)
    except Exception as e:
        print(e)

processMacroStack(fileDictionary)
