#!/opt/cloudera/parcels/Anaconda-4.0.0/bin/python
import numpy as np
import textwrap
import pandas as pd
import Constants as const
from enum import Enum
import time
#import pandasql as psql

class PandasUtilities():

    def __init__(self):
        pass

    def convert_file_to_data_frame(self, file_path, column_name_set, header_row_count):
        df = None

        if column_name_set == const.ColumnNameSet.AM1:
            df = pd.read_table(file_path, encoding="utf-8", dtype=str, header=header_row_count, sep=const.TAB_CHAR, names=const.ColumnNameSet.AM1.value)
        if column_name_set == const.ColumnNameSet.AM1_ORIG:
            df = pd.read_table(file_path, encoding="utf-8", dtype=str, header=header_row_count, sep=const.TAB_CHAR, names=const.ColumnNameSet.AM1_ORIG.value)
        elif column_name_set == const.ColumnNameSet.COHORT:
            df = pd.read_table(file_path, encoding="utf-8", dtype=str, header=header_row_count, sep=const.TAB_CHAR, names=const.ColumnNameSet.COHORT.value)
        elif column_name_set == const.ColumnNameSet.ARX2:
            df = pd.read_table(file_path, encoding="utf-8", dtype=str, header=header_row_count, sep=const.TAB_CHAR, names=const.ColumnNameSet.ARX2.value)

        return df


    def create_data_frame_from_dict(self, input):
        return pd.DataFrame.from_dict(input)

    def create_data_frame(self, columns, rows):
        return pd.DataFrame(columns=columns, data=rows)

    def get_as_json(self, data_frame):
        return data_frame.to_json(orient='index')

    # Decorator function
    #def time_it(sql_method):
    #    def wrapper(*args, **kw):
    #        ts = time.time()
    #        result = sql_method(*args, **kw)
    #        te = time.time()
    #        tte = ('%2.2f ms' % \
    #              ((te - ts) * 1000))
    #        method_name = sql_method.__name__
    #        return result, tte, method_name
    #    return wrapper


    #@time_it
    #def sql_select(self, data_frame):
    #    query = textwrap.dedent("""select cohort_name, SUM(account_value) as account_value
    #                               from   data_frame as df
    #                               group by cohort_name""");
    #    return psql.sqldf(query, locals())