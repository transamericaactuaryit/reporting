#!/opt/cloudera/parcels/Anaconda-4.0.0/bin/python
from flask import Flask
from sparkling import UnixCmdExecutor

#######################################################################
#   Author: Frank Policastro
#   Date:   9.6.2018
#   Notes:  The purpose of the flask_api class is as follows:
#           - Set up a service that listens for incoming REST requests
#             that will allow other software to notify us when it is
#             time to run
#
#   Change log:
#   9.6.2018        Initial Creation        Frank Policastro
#######################################################################

class flask_api():
    def __init__(self):
        pass

app = Flask(__name__)

@app.route('/')
def hello_world():
    uce = UnixCmdExecutor.UnixCmdExecutor()
    uce.execute('spark2-submit --num-executors 4 --executor-cores 4 --driver-memory 10G --executor-memory 10G --conf spark.port.maxRetries=50 --conf "spark.yarn.executor.memoryOverhead=8192" main.py > output.log')
    return 'Sparkling submitted!'

def main():
    app.debug = True
    app.run(host = '0.0.0.0', port=5005)

if __name__ == '__main__':
    main()