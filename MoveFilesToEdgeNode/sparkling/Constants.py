#!/opt/cloudera/parcels/Anaconda-4.0.0/bin/python
from enum import Enum
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DecimalType, DoubleType

am1_select_col_list = ('segment_number',
                       'segment_record',
                       'alfa_plan_code',
                       'issue_age',
                       'gender',
                       'tax_rider',
                       'biz_unit',
                       'legal_entity_stat_comp',
                       'is_qualified',
                       'gmdb_type',
                       'gmib_type',
                       'reinsurance_type',
                       'cohort_group',
                       'glwb_type',
                       'issue_year',
                       'issue_month',
                       'is_new_business',
                       'policy_number',
                       'dac_group',
                       'category',
                       'valuation_date',
                       'as_of_date',
                       'is_current_qtr',
                       'account_value',
                       'withdrawal_benefit_amt',
                       'accum_benefit_amt',
                       'policy_count',
                       'separate_accnt_value',
                       'cqocs_margin_amt',
                       'cqocs_wb_benefit_amt',
                       'cqocs_ab_benefit_amt',
                       'cqocs_ab_fee_amt',
                       'cqocs_wbdb_fee_amt',
                       'cqocs_wb_fee_amt',
                       'cqocs_wb_reserve_amt',
                       'cqocs_ab_reserve_amt',
                       'pqocs_wb_benefit_amt',
                       'pqocs_ab_benefit_amt',
                       'pqocs_ab_fee_amt',
                       'pqocs_wbdb_fee_amt',
                       'pqocs_wb_fee_amt',
                       'pqocs_wb_reserve_amt',
                       'pqocs_ab_reserve_amt',
                       'cqocs_up20bps_wb_reserve_amt',
                       'cqocs_up20bps_ab_reserve_amt',
                       'cqocs_dn20bps_wb_reserve_amt',
                       'cqocs_dn20bps_ab_reserve_amt',
                       'cqocs_nomrgn_ab_fee_amt',
                       'cqocs_nomrgn_wbdb_fee_amt',
                       'cqocs_nomrgn_wb_fee_amt',
                       'cqocs_nomrgn_wb_reserve_amt',
                       'cqocs_nomrgn_ab_reserve_amt',
                       'cqocs_nomrgn_dn20bps_ab_reserve_amt',
                       'cqocs_nomrgn_up20bps_ab_reserve_amt',
                       'cqocs_nomrgn_dn20bps_wb_reserve_amt',
                       'cqocs_nomrgn_up20bps_wb_reserve_amt',
                       'model_name',
                       'file_proj_no',
                       'file_run_no',
                       'load_date',
                       'load_time',
                       'cohort_name')

am1_pandas_schema = ('segment_number', 'segment_record', 'alfa_plan_code', 'issue_age',
                     'gender', 'tax_rider', 'biz_unit', 'legal_entity_stat_comp', 'is_qualified',
                     'gmdb_type', 'gmib_type', 'reinsurance_type', 'cohort_group', 'glwb_type',
                     'issue_year', 'issue_month', 'is_new_business', 'policy_number', 'dac_group',
                     'category', 'is_current_qtr', 'account_value', 'withdrawal_benefit_amt', 'accum_benefit_amt',
                     'policy_count', 'separate_accnt_value', 'cqocs_margin_amt', 'cqocs_wb_benefit_amt',
                     'cqocs_ab_benefit_amt', 'cqocs_ab_fee_amt', 'cqocs_wbdb_fee_amt', 'cqocs_wb_fee_amt',
                     'cqocs_wb_reserve_amt', 'cqocs_ab_reserve_amt', 'pqocs_wb_benefit_amt', 'pqocs_ab_benefit_amt',
                     'pqocs_ab_fee_amt', 'pqocs_wbdb_fee_amt', 'pqocs_wb_fee_amt', 'pqocs_wb_reserve_amt',
                     'pqocs_ab_reserve_amt', 'cqocs_up20bps_wb_reserve_amt', 'cqocs_up20bps_ab_reserve_amt',
                     'cqocs_dn20bps_wb_reserve_amt', 'cqocs_dn20bps_ab_reserve_amt', 'cqocs_nomrgn_ab_fee_amt',
                     'cqocs_nomrgn_wbdb_fee_amt', 'cqocs_nomrgn_wb_fee_amt', 'cqocs_nomrgn_wb_reserve_amt',
                     'cqocs_nomrgn_ab_reserve_amt', 'cqocs_nomrgn_dn20bps_ab_reserve_amt',
                     'cqocs_nomrgn_up20bps_ab_reserve_amt', 'cqocs_nomrgn_dn20bps_wb_reserve_amt',
                     'cqocs_nomrgn_up20bps_wb_reserve_amt', 'file_proj_no', 'file_run_no', 'cohort_name',
                     'row_id', 'model_name', 'valuation_date', 'as_of_date', 'load_date', 'load_time')


am1_renamed_schema  = StructType([
                      StructField("segment_number", StringType(), True),
                      StructField("segment_record", StringType(), True),
                      StructField("alfa_plan_code", StringType(), True),
                      StructField("issue_age", IntegerType(), True),
                      StructField("gender", StringType(), True),
                      StructField("tax_rider", StringType(), True),
                      StructField("biz_unit", StringType(), True),
                      StructField("legal_entity_stat_comp", StringType(), True),
                      StructField("is_qualified", StringType(), True),
                      StructField("gmdb_type", StringType(), True),
                      StructField("gmib_type", StringType(), True),
                      StructField("reinsurance_type", StringType(), True),
                      StructField("cohort_group", StringType(), True),
                      StructField("glwb_type", StringType(), True),
                      StructField("issue_year", IntegerType(), True),
                      StructField("issue_month", IntegerType(), True),
                      StructField("is_new_business", StringType(), True),
                      StructField("policy_number", StringType(), True),
                      StructField("dac_group", StringType(), True),
                      StructField("category", StringType(), True),
                      StructField("valuation_date", StringType(), True),
                      StructField("as_of_date", StringType(), True),
                      StructField("is_current_qtr", StringType(), True),
                      StructField("account_value", DecimalType(30, 8), True),
                      StructField("withdrawal_benefit_amt", DecimalType(30, 8), True),
                      StructField("accum_benefit_amt", DecimalType(30, 8), True),
                      StructField("policy_count", DecimalType(30, 8), True),
                      StructField("separate_accnt_value", DecimalType(30, 8), True),
                      StructField("cqocs_margin_amt", DecimalType(30, 8), True),
                      StructField("cqocs_wb_benefit_amt", DecimalType(30, 8), True),
                      StructField("cqocs_ab_benefit_amt", DecimalType(30, 8), True),
                      StructField("cqocs_ab_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_wbdb_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_wb_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("pqocs_wb_benefit_amt", DecimalType(30, 8), True),
                      StructField("pqocs_ab_benefit_amt", DecimalType(30, 8), True),
                      StructField("pqocs_ab_fee_amt", DecimalType(30, 8), True),
                      StructField("pqocs_wbdb_fee_amt", DecimalType(30, 8), True),
                      StructField("pqocs_wb_fee_amt", DecimalType(30, 8), True),
                      StructField("pqocs_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("pqocs_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_up20bps_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_up20bps_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_dn20bps_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_dn20bps_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_ab_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_wbdb_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_wb_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_dn20bps_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_up20bps_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_dn20bps_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_up20bps_wb_reserve_amt", DecimalType(30, 8), True)])

am1_final_schema    = StructType([
                      StructField("segment_number", StringType(), True),
                      StructField("segment_record", StringType(), True),
                      StructField("alfa_plan_code", StringType(), True),
                      StructField("issue_age", DoubleType(), True),
                      StructField("gender", StringType(), True),
                      StructField("tax_rider", StringType(), True),
                      StructField("biz_unit", StringType(), True),
                      StructField("legal_entity_stat_comp", StringType(), True),
                      StructField("is_qualified", StringType(), True),
                      StructField("gmdb_type", StringType(), True),
                      StructField("gmib_type", StringType(), True),
                      StructField("reinsurance_type", StringType(), True),
                      StructField("cohort_group", StringType(), True),
                      StructField("glwb_type", StringType(), True),
                      StructField("issue_year", DoubleType(), True),
                      StructField("issue_month", DoubleType(), True),
                      StructField("is_new_business", StringType(), True),
                      StructField("policy_number", StringType(), True),
                      StructField("dac_group", StringType(), True),
                      StructField("category", StringType(), True),
                      StructField("is_current_qtr", StringType(), True),
                      StructField("account_value", DecimalType(30, 8), True),
                      StructField("withdrawal_benefit_amt", DecimalType(30, 8), True),
                      StructField("accum_benefit_amt", DecimalType(30, 8), True),
                      StructField("policy_count", DecimalType(30, 8), True),
                      StructField("separate_accnt_value", DecimalType(30, 8), True),
                      StructField("cqocs_margin_amt", DecimalType(30, 8), True),
                      StructField("cqocs_wb_benefit_amt", DecimalType(30, 8), True),
                      StructField("cqocs_ab_benefit_amt", DecimalType(30, 8), True),
                      StructField("cqocs_ab_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_wbdb_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_wb_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("pqocs_wb_benefit_amt", DecimalType(30, 8), True),
                      StructField("pqocs_ab_benefit_amt", DecimalType(30, 8), True),
                      StructField("pqocs_ab_fee_amt", DecimalType(30, 8), True),
                      StructField("pqocs_wbdb_fee_amt", DecimalType(30, 8), True),
                      StructField("pqocs_wb_fee_amt", DecimalType(30, 8), True),
                      StructField("pqocs_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("pqocs_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_up20bps_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_up20bps_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_dn20bps_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_dn20bps_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_ab_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_wbdb_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_wb_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_dn20bps_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_up20bps_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_dn20bps_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_up20bps_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("file_proj_no", StringType(), True),
                      StructField("file_run_no", StringType(), True),
                      StructField("cohort_name", StringType(), True),
                      StructField("row_id", StringType(), True),
                      StructField("model_name", StringType(), True),
                      StructField("valuation_date", StringType(), True),
                      StructField("as_of_date", StringType(), True),
                      StructField("load_date", StringType(), True),
                      StructField("load_time", StringType(), True)])

########################################################################

class ConfigParameters(Enum):
    hdfs_path = "hdfs_path"
    db_name = "db_name"
    cohort_name_table = "cohort_name_table"

TEST_AREA = '/data1/mounts/ValTest/Frank.Policastro/'
LANDING_PATH = '/projects/actuarialresultsdev/landing/'
DB_HDFS_PATH = '/projects/actuarialresultsdev/db/'
VAL_TEST = '/data1/mounts/ValTest/'
NB6_COHORTS_PATH = './cohorts/cohort_names_nb6.txt'
INFORCE_COHORTS_PATH = './cohorts/cohort_names_inforce.txt'
COHORTS_PATH = './cohorts/cohort_names.txt'

FILE_BUFFER_SIZE = 1048576

AM1_FILE_EXT = '._.am1'
ARX2_FILE_EXT = '.arx2'

TXT_FILE_EXT = '.txt'
ZIP_FILE_EXT = '.zip'
CSV_FILE_EXT = '.csv'
EXCEL_FILE_EXT = '.xlsx'

TAB_CHAR = '\t'
NEW_LINE = '\n'
PERIOD = '.'
FWD_SLASH = '/'

NB6_COHORTS = 'nb6'
INFORCE_COHORTS = 'inforce'

NB6_MODEL_NAME = 'Newbusiness6pt'

COL_PROCESSED_TS = 'processed_ts'
COL_FILE_RUN_NO = 'file_run_no'
COL_DAC_GROUP = 'dac_group'
COL_DACGROUP = 'DACGroup'
COL_CATEGORY = 'category'

class HdfsCommands(Enum):
    List = 'hdfs dfs -ls'
    ListNameOnly = 'hdfs dfs -ls -C'
    Put = 'hdfs dfs -put'
    MkDir = 'hdfs dfs -mkdir'
    Delete = 'hdfs dfs -rm'

class DataTypes(Enum):
    STRING = 'str'
    FLOAT = 'float'
    INT = 'int'

class ProductLines(Enum):
    VARIABLE_ANNUITY = 'variable_annuity'
    FIXED_ANNUITY = 'fixed_annuity'
    RETIREMENT = 'retirement'
    LIFE = 'life'

class Models(Enum):
    FAIR_VALUE = 'fair_value'
    STAT = 'stat'
    MVN = 'mvn'
    DAC = 'dac'

NA = 'N/A'
ARX_BAD_VALUE = '"********************"'
M_MODEL_NAME = 'ModelName'
M_RUN_NO = 'RunNo'
M_TITLE = 'Title'
M_SECTION = 'Section'


COHORT_COLUMN_NAMES = ['proj_no', 'dac_group_code', 'cohort_model', 'cohort_name']

ARX2_COLUMN_NAMES = ['model_name', 'file_run_no', 'section', 'title', 't', 'month_val', 'year_val', 'field_val']

AM1_ORIGINAL_NAMES = ['//segmentnumber','segmentrecord','ck.Plan','ck.IssAge','ck.Gender','ck.Class','ck.Char1','ck.Char2',
                      'ck.Char3','ck.Char4','ck.Char5','ck.Char6','ck.Char7','ck.Char8','ck.IssYear','ck.IssMon','ck.NewBus',
                      'PolicyNumber','DACGroup','sys.category','ValDate','AsOfDate','CurrQtr', 'RunTypeI', 'RunTypeII', 'AVIF_ts_E0',
                      'F133PVRptInitWBFace_th_ELastValue','F133PVRptInitABFace_th_ELastValue','F133PVRptInitLives_th_ELastValue',
                      'F133PVRptInitSAV_th_ELastValue','F133PVMarginOCS_th_ELastValue','F133PVGMWBBenOCS_th_ELastValue',
                      'F133PVGMABBenOCS_th_ELastValue','F133PVGMABFeeOCS_th_ELastValue','F133PVGMWBDBFeeOCS_th_ELastValue',
                      'F133PVGMWBFeeOCS_th_ELastValue','F133PVGMWBResOCS_th_ELastValue','F133PVGMABResOCS_th_ELastValue',
                      'F133PVGMWBBenOCSPQ_th_ELastValue','F133PVGMABBenOCSPQ_th_ELastValue','F133PVGMABFeeOCSPQ_th_ELastValue',
                      'F133PVGMWBDBFeeOCSPQ_th_ELastValue','F133PVGMWBFeeOCSPQ_th_ELastValue','F133PVGMWBResOCSPQ_th_ELastValue',
                      'F133PVGMABResOCSPQ_th_ELastValue','F133PVGMWBResOCSUp_th_ELastValue','F133PVGMABResOCSUp_th_ELastValue',
                      'F133PVGMWBResOCSDn_th_ELastValue','F133PVGMABResOCSDn_th_ELastValue','F133PVNMGMABFeeOCS_th_ELastValue',
                      'F133PVNMGMWBDBFeeOCS_th_ELastValue','F133PVNMGMWBFeeOCS_th_ELastValue','F133PVNMGMWBResNoOCS_th_ELastValue',
                      'F133PVNMGMABResNoOCS_th_ELastValue','F133PVNMGMABResOCSDn_th_ELastValue','F133PVNMGMABResOCSUp_th_ELastValue',
                      'F133PVNMGMWBResOCSDn_th_ELastValue','F133PVNMGMWBResOCSUp_th_ELastValue']

AM1_ORIGINAL_NAMES_2 = ['//segmentnumber','segmentrecord','ck.Plan','ck.IssAge','ck.Gender','ck.Class','ck.Char1','ck.Char2',
                      'ck.Char3','ck.Char4','ck.Char5','ck.Char6','ck.Char7','ck.Char8','ck.IssYear','ck.IssMon','ck.NewBus',
                      'PolicyNumber','DACGroup','sys.category','ValDate','AsOfDate','CurrQtr', 'AVIF_ts_E0',
                      'F133PVRptInitWBFace_th_ELastValue','F133PVRptInitABFace_th_ELastValue','F133PVRptInitLives_th_ELastValue',
                      'F133PVRptInitSAV_th_ELastValue','F133PVMarginOCS_th_ELastValue','F133PVGMWBBenOCS_th_ELastValue',
                      'F133PVGMABBenOCS_th_ELastValue','F133PVGMABFeeOCS_th_ELastValue','F133PVGMWBDBFeeOCS_th_ELastValue',
                      'F133PVGMWBFeeOCS_th_ELastValue','F133PVGMWBResOCS_th_ELastValue','F133PVGMABResOCS_th_ELastValue',
                      'F133PVGMWBBenOCSPQ_th_ELastValue','F133PVGMABBenOCSPQ_th_ELastValue','F133PVGMABFeeOCSPQ_th_ELastValue',
                      'F133PVGMWBDBFeeOCSPQ_th_ELastValue','F133PVGMWBFeeOCSPQ_th_ELastValue','F133PVGMWBResOCSPQ_th_ELastValue',
                      'F133PVGMABResOCSPQ_th_ELastValue','F133PVGMWBResOCSUp_th_ELastValue','F133PVGMABResOCSUp_th_ELastValue',
                      'F133PVGMWBResOCSDn_th_ELastValue','F133PVGMABResOCSDn_th_ELastValue','F133PVNMGMABFeeOCS_th_ELastValue',
                      'F133PVNMGMWBDBFeeOCS_th_ELastValue','F133PVNMGMWBFeeOCS_th_ELastValue','F133PVNMGMWBResNoOCS_th_ELastValue',
                      'F133PVNMGMABResNoOCS_th_ELastValue','F133PVNMGMABResOCSDn_th_ELastValue','F133PVNMGMABResOCSUp_th_ELastValue',
                      'F133PVNMGMWBResOCSDn_th_ELastValue','F133PVNMGMWBResOCSUp_th_ELastValue']

AM1_COLUMN_NAMES_ORIG = ['segment_number', 'segment_record',
                    'alfa_plan_code', 'issue_age', 'gender', 'tax_rider',
                    'biz_unit', 'legal_entity_stat_comp', 'is_qualified',
                    'gmdb_type', 'gmib_type', 'reinsurance_type', 'cohort_group',
                    'glwb_type', 'issue_year', 'issue_month', 'is_new_business',
                    'policy_number', 'dac_group', 'category', 'valuation_date',
                    'as_of_date', 'is_current_qtr', 'account_value',
                    'withdrawal_benefit_amt',
                    'accum_benefit_amt', 'policy_count', 'separate_accnt_value',
                    'cqocs_margin_amt', 'cqocs_wb_benefit_amt',
                    'cqocs_ab_benefit_amt', 'cqocs_ab_fee_amt',
                    'cqocs_wbdb_fee_amt', 'cqocs_wb_fee_amt',
                    'cqocs_wb_reserve_amt', 'cqocs_ab_reserve_amt',
                    'pqocs_wb_benefit_amt', 'pqocs_ab_benefit_amt',
                    'pqocs_ab_fee_amt', 'pqocs_wbdb_fee_amt',
                    'pqocs_wb_fee_amt', 'pqocs_wb_reserve_amt',
                    'pqocs_ab_reserve_amt', 'cqocs_up20bps_ab_reserve_amt',
                    'cqocs_dn20bps_wb_reserve_amt', 'cqocs_dn20bps_ab_reserve_amt',
                    'cqocs_up20bps_wb_reserve_amt', 'cqocs_nomrgn_ab_fee_amt',
                    'cqocs_nomrgn_wbdb_fee_amt', 'cqocs_nomrgn_wb_fee_amt',
                    'cqocs_nomrgn_wb_reserve_amt', 'cqocs_nomrgn_ab_reserve_amt',
                    'cqocs_nomrgn_up20bps_wb_reserve_amt',
                    'cqocs_nomrgn_up20bps_ab_reserve_amt',
                    'cqocs_nomrgn_dn20bps_wb_reserve_amt',
                    'cqocs_nomrgn_dn20bps_ab_reserve_amt']

AM1_COLUMN_NAMES = ['row_id', 'processed_ts', 'model_name', 'file_run_no',
                    'alfa_run_id', 'segment_number', 'segment_record',
                    'alfa_plan_code', 'issue_age', 'gender', 'tax_rider',
                    'biz_unit', 'legal_entity_stat_comp', 'is_qualified',
                    'gmdb_type', 'gmib_type', 'reinsurance_type', 'cohort_group',
                    'glwb_type', 'issue_year', 'issue_month', 'is_new_business',
                    'policy_number', 'dac_group', 'category', 'valuation_date',
                    'as_of_date', 'is_current_qtr', 'account_value', 'withdrawal_benefit_amt',
                    'accum_benefit_amt', 'policy_count', 'separate_accnt_value',
                    'cqocs_margin_amt', 'cqocs_wb_benefit_amt',
                    'cqocs_ab_benefit_amt', 'cqocs_ab_fee_amt',
                    'cqocs_wbdb_fee_amt', 'cqocs_wb_fee_amt',
                    'cqocs_wb_reserve_amt', 'cqocs_ab_reserve_amt',
                    'pqocs_wb_benefit_amt', 'pqocs_ab_benefit_amt',
                    'pqocs_ab_fee_amt', 'pqocs_wbdb_fee_amt',
                    'pqocs_wb_fee_amt', 'pqocs_wb_reserve_amt',
                    'pqocs_ab_reserve_amt', 'cqocs_up20bps_ab_reserve_amt',
                    'cqocs_dn20bps_wb_reserve_amt', 'cqocs_dn20bps_ab_reserve_amt',
                    'cqocs_up20bps_wb_reserve_amt', 'cqocs_nomrgn_ab_fee_amt',
                    'cqocs_nomrgn_wbdb_fee_amt', 'cqocs_nomrgn_wb_fee_amt',
                    'cqocs_nomrgn_wb_reserve_amt', 'cqocs_nomrgn_ab_reserve_amt',
                    'cqocs_nomrgn_up20bps_wb_reserve_amt',
                    'cqocs_nomrgn_up20bps_ab_reserve_amt',
                    'cqocs_nomrgn_dn20bps_wb_reserve_amt',
                    'cqocs_nomrgn_dn20bps_ab_reserve_amt']

class ColumnNameSet(Enum):
    AM1 = AM1_COLUMN_NAMES
    AM1_ORIG = AM1_ORIGINAL_NAMES
    COHORT = COHORT_COLUMN_NAMES
    ARX2 = ARX2_COLUMN_NAMES


BLACK_LIST_OF_ROWS = ['"" "--------------------"',
                        '"Scenario Summary for Module Projection',
                        '"Check1 = 0',
                        '"Check2 = 0',
                        '"Check3 = 0',
                        '"Check4 = 0',
                        '"Zero',
                        '"Check5 = 0',
                        '"Check6 = 0',
                        '"Check7 = 0',
                        '"Check8 = 0',
                        '"Check9 = 0',
                        '"TOTAL CHECK = 0"',
                        '"ULIC\'s REINSURANCE COST for 60% No LB Coin/ 100% LB Ceded to TLIC"',
                        '"Check', # might not work...
                        '"(Detail not shown in reports above so all profit rerpot are out of balance by cost amount)"',
                        ' "F133 PV GMWB Benefit with OCS (INCREASE)"',
                        ' "F133 PV GMAB Benefit with OCS (INCREASE)"',
                        ' "F133 PV GMWB Fee with OCS (INCREASE)"',
                        ' "F133 PV GMAB Fee with OCS (INCREASE)"',
                        ' "F133 PV GMWB/AB Fee with OCS (INCREASE)"',
                        ' "F133 PV GMWB ROP DB Fee with OCS (INCREASE)"',
                        ' "F133 PV GMWB Reserve with OCS (INCREASE)"',
                        ' "F133 PV GMAB Reserve with OCS (INCREASE)"',
                        '"                                                   Description:  U71 FAS 97 Report 5/13/2015 (Updated 7/14/2015) - TARe Only"',
                        '"                                                 Description:  U69 Living Benefit Streams (with Accr Fee 5/7/2015) - TARe Only"',
                        '"VIA GAIN"',
                        '"Living Benefit Streams"']

SECTION_TITLES = ['"ASSESSMENTS & CHARGES FOR FAS97 DAC UNLOCKING and MISCELLANEOUS INFO',
                  '"DIRECT POLICYHOLDER BENEFITS PAID',
                  '"SOP FOR FAS97 DAC UNLOCKING',
                  '"VARIOUS STATISTICS',
                  '"POLICY EXHIBIT LIVES',
                  '"RIDER INFORCE STATISTICS',
                  '"TOTAL ACCOUNT VALUE ROLLFORWARD',
                  '"EXPENSES"',
                  '"REINSURANCE CEDED REPORT',
                  '"FREE SURPLUS INFO for CAPITAL & RESERVES PROJECTIONS Only"',
                  '"REINSURANCE REPORT',
                  '"ML IB REINS AGG CAP"',
                  '"ML DB REINS AGG CAP"',
                  '"HEDGE COST RECOGNITION FOR DAC"',
                  '"Financial Plan Stat Income Statement"',
                  '"Financial Plan GA Rollforward & Stat Balance Sheet"',
                  '"Financial Plan SA Rollforward"',
                  '"Financial Plan Misc Output"',
                  '"CASH FLOW - STATUTORY STATEMENT"',
                  '"STATUTORY INCOME STATEMENT"',
                  '"GA STATUTORY ASSETS"',
                  '"GA STATUTORY LIABILITIES"',
                  '"GA STATUTORY SURPLUS"',
                  '"GA & SA RESERVE and IMR INFO"',
                  '"SEPARATE ACCOUNT VALUE ROLLFORWARD"',
                  '"GENERAL ACCOUNT VALUE ROLLFORWARD"',
                  '"C3P2 Standard Scenario"',
                  '"FAS97 2004"',
                  '"FAS97 2004 A"',
                  '"FAS97 2004 B"',
                  '"GMIB"',
                  '"GMWB"',
                  '"GMAB"',
                  '"TAXPAYER"',
                  '"INFO ONLY"']

# Fair Value Constant strings
class FairValueSectionConstants(Enum):
    HedgeCostRecognitionForDac = 'HEDGE COST RECOGNITION FOR DAC'

class FairValueTitleConstants(Enum):
    LivesInforceGmibReinsOnly = 'Lives Inforce (GMIB Reins only)'
    FundValueInforceGmibReinsOnly = 'Fund Value Inforce (GMIB Reins Only)'
    GmibReinsBenefitAfterAggLimit = 'GMIB Reins Benefit after Aggregate Limit'
    GmibReinsPremium = 'GMIB Reins Premium'

FAIR_VALUE_COL_TITLES = ['Discount Factor (before Tax)',
                         'Discount Factor (with OCS)',
                         'GMIB Reins Premium',
                         'GMIB Reins Benefit after Aggregate Limit',
                         'Lives Inforce (GMIB Reins only)',
                         'Fund Value Inforce (GMIB Reins only)']
