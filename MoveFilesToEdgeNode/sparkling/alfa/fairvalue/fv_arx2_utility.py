#!/opt/cloudera/parcels/Anaconda-4.0.0/bin/python
import io
import datetime
import uuid
import os
import re
import sys
import time
import traceback
import subprocess
import pandas as pd
import numpy as np
from collections import OrderedDict
from decimal import Decimal
from pandas import pivot_table
from sparkling import Constants as const
from sparkling import ysera_base
from pyspark import SparkContext, SparkConf, HiveContext
from pyspark.sql import SQLContext, SparkSession, Row
from pyspark.sql.functions import *
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DecimalType

#######################################################################
#   Author: Frank Policastro
#   Date:   9.6.2018
#   Notes:  The purpose of the fv_arx2_utility class is as follows:
#           - Copy .ARX2 files from a network share into HDFS
#           - Perform some transformations on the ARX2 data in the
#             files like adding some additional columns
#           - Pivot the ARX2 data to prepare it for consumption
#           - Save the data to a landing area with the new columns
#           - Insert the landing area data into a compressed parquet
#             table
#
#   Change log:
#   9.6.2018        Initial Creation        Frank Policastro
#######################################################################

class fv_arx2_utility(ysera_base.ysera_base):

    arx2_select_col_list   = ('section',
                              'title',
                              't',
                              'month_val',
                              'year_val',
                              'field_val',
                              'model_name',
                              'file_run_no',
                              'load_date',
                              'load_time',
                              'row_id')
    arx2_schema       = StructType([
                        StructField("section", StringType(), True),
                        StructField("title", StringType(), True),
                        StructField("t", IntegerType(), True),
                        StructField("month_val", IntegerType(), True),
                        StructField("year_val", IntegerType(), True),
                        StructField("field_val", DecimalType(30, 8), True),
                        StructField("model_name", StringType(), True),
                        StructField("file_run_no", StringType(), True),
                        StructField("load_date", StringType(), True),
                        StructField("load_time", StringType(), True),
                        StructField("row_id", StringType(), True)])
    arx2_new_schema   = StructType([
                        StructField("t", IntegerType(), True),
                        StructField("month_val", IntegerType(), True),
                        StructField("year_val", IntegerType(), True),
                        StructField("discount_factor_before_tax", DecimalType(30, 8), True),
                        StructField("discount_factor_with_OCS", DecimalType(30, 8), True),
                        StructField("GMIB_reins_premium", DecimalType(30, 8), True),
                        StructField("GMIB_reins_benefit_after_aggregate_limit", DecimalType(30, 8), True),
                        StructField("lives_inforce_GMIB_reins_only", DecimalType(30, 8), True),
                        StructField("fund_value_inforce_GMIB_reins_only", DecimalType(30, 8), True),
                        StructField("row_id", StringType(), True),
                        StructField("model_name", StringType(), True),
                        StructField("file_run_no", StringType(), True),
                        StructField("load_date", StringType(), True),
                        StructField("load_time", StringType(), True)])


    #region default constructor
    def __init__(self, load_date="", load_time=""):
        ysera_base.ysera_base.__init__(self)
        self.load_date = load_date
        self.load_time = load_time
        pass
    #endregion default constructor


    #region get_type
    def get_type(self, var):
        try:
            if int(var) == float(var):
                return const.DataTypes.INT.value
        except:
            try:
                float(var)
                return const.DataTypes.FLOAT.value
            except:
                return const.DataTypes.STRING.value
        pass
    #endregion get_type


    #region extract_cycle
    def extract_cycles(self, text_lines):
        for index, i in enumerate(text_lines):
            if re.search(r'Cycles: (\d+)', i):
                g = re.search(r'(\d+)', i[::]).span()
                cycles = int(i[g[0]:g[1]])
                break
        return cycles
        pass
    #endregion extract_cycle


    #region land_file
    def land_files(self, src, dest):
        self.uce.execute("{0} {1} {2}".format(const.HdfsCommands.Put.value, src, dest))
        pass
    #endregion land_file


    #region parse_file_names
    def parse_file_names(self):
        results = []
        arx2_path = self.hdfs_path + self.landing_path + self.arx2_file_ext
        arx2_files = ysera_base.ysera_base.sc.wholeTextFiles(arx2_path).collect()
        for arx2_file in arx2_files:
            path_pieces = arx2_file[0].split("/")
            file_pieces = path_pieces[9].split(".")
            model_name = file_pieces[0]
            run_no = file_pieces[2]
            temp = model_name, run_no, arx2_file[0]
            results.append(temp)
        return results
    #endregion parse_file_names


    #region clean_up_landing_area
    def clean_up_landing_area(self):
        del_path = self.hdfs_path + self.landing_path + self.arx2_table_name + '/*'
        self.uce.execute("{0} {1}".format(const.HdfsCommands.Delete.value, del_path))
    #endregion clean_up_landing_area


    #region process_file
    def process_file(self, row_tuple):
        model_name = str(row_tuple[0])
        file_run_no = str(row_tuple[1])
        file_path = str(row_tuple[2])

        col_list = []
        rows_list = []
        new_list = []
        tmp_list = []
        title = ''
        section = ''

        file_name_pieces = file_path.split(const.PERIOD)

        month_year_regex = re.compile(r'(?P<Month>(\d{1,2}))/(?P<Year>(?:\d{4}))')
        row_data_regex = re.compile(r'"[^\s{1}\d{1,4}/\d{1,4}]\w+[\s*|&|(|)]*.*')

        all_lines = ysera_base.ysera_base.sc.textFile(file_path).collect()

        cycles = self.extract_cycles(all_lines)

        for line in all_lines:
            if not line.startswith(tuple(const.BLACK_LIST_OF_ROWS)):
                line = re.sub(r'""', '', line, count=0)
                line = re.sub(r'"""[-]+"', '', line, count=0)

                i = month_year_regex.finditer(line)
                for m in i:
                    col_list.append(m.group())

                tmp_list = []

                if line.startswith(tuple(const.SECTION_TITLES)):
                    section = line.replace('"', '')
                    section = section.replace('\n', '')
                    continue

                # match lines that ends with a lot of numbers
                matched_lines = re.findall(row_data_regex, line)

                bq_counter = 0
                for ml in matched_lines:
                    title = ''
                    line_pieces = ml.split()

                    for item in line_pieces:
                        if self.get_type(item) == const.DataTypes.STRING.value:
                            if str(item) == '"':
                                bq_counter += 1
                                if bq_counter == 2:
                                    tmp_list.insert(0, const.NA)   # change this to be N/A instead of 0
                                    bq_counter = 0
                            elif str(item).startswith(const.ARX_BAD_VALUE):
                                tmp_list.append(const.NA)
                            elif len(title) == 0:
                                title += item.strip('"')
                            else:
                                title += (' ' + item.strip('"'))
                            pass
                        elif self.get_type(item) == const.DataTypes.FLOAT.value:
                            tmp_list.append(float(item))
                        elif self.get_type(item) == const.DataTypes.INT.value:
                            tmp_list.append(int(item))


                    tmp_list.insert(0, title)
                    tmp_list.insert(0, section)
                    tmp_list.insert(0, file_name_pieces[2])
                    tmp_list.insert(0, file_name_pieces[0])
                    rows_list.append(tmp_list)

        if len(col_list) > 0:
            arx2_col_list = list(OrderedDict((x, True) for x in col_list).keys())
            new_list = list(OrderedDict((x, True) for x in col_list).keys())
            new_list.insert(0, const.M_TITLE)
            new_list.insert(0, const.M_SECTION)
            new_list.insert(0, const.M_RUN_NO)
            new_list.insert(0, const.M_MODEL_NAME)

        t = 0;
        arx2_rows_list = []

        for row in rows_list:
            for idx, item in enumerate(arx2_col_list, start=4):
                tmp2_list = []
                tmp2_list.append(model_name)
                tmp2_list.append(file_run_no)
                tmp2_list.append(str(row[2]))
                tmp2_list.append(str(row[3]))

                tmp2_list.append(t)

                date_pieces = item.split(const.FWD_SLASH)

                tmp2_list.append(int(date_pieces[0]))
                tmp2_list.append(int(date_pieces[1]))

                if str(row[idx]) != const.NA:
                    tmp2_list.append(Decimal(row[idx]))
                else:
                    tmp2_list.append(Decimal(0))

                arx2_rows_list.append(tmp2_list)

                if t < cycles:
                    t += 1
                else:
                    t = 0


        arx2_pandas = self.pu.create_data_frame(const.ColumnNameSet.ARX2.value, arx2_rows_list)
        arx2_pandas.drop_duplicates(inplace=True)


        df_tmp_filtered = arx2_pandas.loc[arx2_pandas['title'].isin(const.FAIR_VALUE_COL_TITLES)]

        df3 = pd.pivot_table(df_tmp_filtered,
                             index=['t', 'month_val', 'year_val', 'model_name', 'file_run_no'],
                             columns='title', values='field_val', aggfunc='first')

        df4 = df3.rename(columns={'Discount Factor (before Tax)': 'discount_factor_before_tax',
                                  'Discount Factor (with OCS)': 'discount_factor_with_OCS',
                                  'GMIB Reins Premium': 'GMIB_reins_premium',
                                  'GMIB Reins Benefit after Aggregate Limit': 'GMIB_reins_benefit_after_aggregate_limit',
                                  'Lives Inforce (GMIB Reins only)': 'lives_inforce_GMIB_reins_only',
                                  'Fund Value Inforce (GMIB Reins only)': 'fund_value_inforce_GMIB_reins_only'})

        column_order = ['discount_factor_before_tax',
                        'discount_factor_with_OCS',
                        'GMIB_reins_premium',
                        'GMIB_reins_benefit_after_aggregate_limit',
                        'lives_inforce_GMIB_reins_only',
                        'fund_value_inforce_GMIB_reins_only']

        df5 = df4.reindex_axis(column_order, axis = 1)

        df6 = pd.DataFrame(df5.to_records())

        df6['row_id'] = df6.apply(lambda row: self.get_uuid(), axis=1)
        df6['load_date'] = df6.apply(lambda row: self.load_date, axis=1)
        df6['load_time'] = df6.apply(lambda row: self.load_time, axis=1)

        df6 = df6[['t', 'month_val', 'year_val',
                   'discount_factor_before_tax', 'discount_factor_with_OCS',
                   'GMIB_reins_premium', 'GMIB_reins_benefit_after_aggregate_limit',
                   'lives_inforce_GMIB_reins_only', 'fund_value_inforce_GMIB_reins_only',
                   'row_id', 'model_name', 'file_run_no', 'load_date', 'load_time']]

        #arx2_data = ysera_base.spark.createDataFrame(arx2_pandas, self.arx2_schema)
        arx2_data = ysera_base.ysera_base.spark.createDataFrame(df6, self.arx2_new_schema)

        #arx2_data.repartition(50)

        ltable  = str(self.db_name) + ".tl_" + str(self.arx2_table_name)
        ptable  = str(self.db_name) + ".tp_" + str(self.arx2_table_name)

        arx2_data.coalesce(1).write.insertInto(ltable)
        #arx2_data.write.insertInto(ltable)

        arx2_data.createOrReplaceTempView("arx2_data")

        sql_stmt = """
                   INSERT INTO TABLE {0}
                   PARTITION (model_name, file_run_no, load_date, load_time)
                   SELECT arx2.t,
                          arx2.month_val,
                          arx2.year_val,
                          arx2.discount_factor_before_tax,
                          arx2.discount_factor_with_OCS,
                          arx2.GMIB_reins_premium,
                          arx2.GMIB_reins_benefit_after_aggregate_limit,
                          arx2.lives_inforce_GMIB_reins_only,
                          arx2.fund_value_inforce_GMIB_reins_only,
                          arx2.row_id,
                          arx2.model_name as model_name,
                          arx2.file_run_no as file_run_no,
                          arx2.load_date as load_date,
                          arx2.load_time as load_time
                   FROM   arx2_data arx2
                   """.format(ptable)

        ysera_base.ysera_base.spark.sql(sql_stmt)
        pass
    #endregion process_file


    #region get_period_list
    def get_period_list(self, start, count, freq):
        period_list = []

        tmp = pd.period_range(start, periods=count, freq=freq, name='Periods')

        for i, item in enumerate(tmp):
            period_list.append((i, item))

        return period_list
    #endregion get_period_list

