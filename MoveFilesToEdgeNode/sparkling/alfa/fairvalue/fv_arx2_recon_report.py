#!/opt/cloudera/parcels/Anaconda-4.0.0/bin/python
import io
import smtplib
import string
import datetime
import uuid
import os
import sys
import time
import traceback
import subprocess
import pandas as pd
import numpy as np
from sparkling import Constants as const
from sparkling import ysera_base
from pyspark import SparkContext, SparkConf, HiveContext
from pyspark.sql import SQLContext, SparkSession, Row
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DecimalType

from email import encoders
from email.message import Message
from email.mime.application import MIMEApplication
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
# For guessing MIME type based on file name extension
import mimetypes
from optparse import OptionParser


#######################################################################
#   Author: Frank Policastro
#   Date:   9.6.2018
#   Notes:  The purpose of the am1_recon_report class is as follows:
#           - Run a query to summarize the results of inserted set
#             of .ARX2 files
#
#   Change log:
#   9.11.2018        Initial Creation        Frank Policastro
#######################################################################

class fv_arx2_recon_report(ysera_base.ysera_base):


    def __init__(self):
        ysera_base.ysera_base.__init__(self)
        pass


    def run_report(self, table_name='', model_name='', proj_no='', run_no='', val_date='', as_of_date='', load_date='', load_time=''):
        has_where_clause = False

        if (table_name == ''):
            table_name = "actuarialresultsdev.pfv_arx2"

        ptable = table_name

        sql_stmt = """
                   select "trans step 1" as batch_desc,
                          model_name,
                          file_run_no,
                          "201806" as valuation_date,
                          "201806" as as_of_date,
                          load_date,
                          load_time,
                          sum(lives_inforce_gmib_reins_only) as lives_inforce_gmib_reins_only,
                          sum(fund_value_inforce_gmib_reins_only) as fund_value_inforce_gmib_reins_only,
                          sum(gmib_reins_premium) as gmib_reins_premium,
                          sum(gmib_reins_benefit_after_aggregate_limit) as gmib_reins_benefit_after_aggregate_limit
                   from   {0}
                   """.format(ptable)

        if (model_name != ''):
            if (not has_where_clause):
                has_where_clause = True
                sql_stmt += """
                            where model_name = '{0}'
                            """.format(model_name)
            else:
                sql_stmt += """
                            and model_name = '{0}'
                            """.format(model_name)

        if (proj_no != ''):
            if (not has_where_clause):
                has_where_clause = True
                sql_stmt += """
                            where file_proj_no = '{0}'
                            """.format(proj_no)
            else:
                sql_stmt += """
                            and file_proj_no = '{0}'
                            """.format(proj_no)

        if (run_no != ''):
            if (not has_where_clause):
                has_where_clause = True
                sql_stmt += """
                            where file_run_no = '{0}'
                            """.format(run_no)
            else:
                sql_stmt += """
                            and file_run_no = '{0}'
                            """.format(run_no)

        if (val_date != ''):
            if (not has_where_clause):
                has_where_clause = True
                sql_stmt += """
                            where valuation_date = '{0}'
                            """.format(val_date)
            else:
                sql_stmt += """
                            and valuation_date = '{0}'
                            """.format(val_date)

        if (as_of_date != ''):
            if (not has_where_clause):
                has_where_clause = True
                sql_stmt += """
                            where as_of_date = '{0}'
                            """.format(as_of_date)
            else:
                sql_stmt += """
                            and as_of_date = '{0}'
                            """.format(as_of_date)

        if (load_date != ''):
            if (not has_where_clause):
                has_where_clause = True
                sql_stmt += """
                            where load_date = '{0}'
                            """.format(load_date)
            else:
                sql_stmt += """
                            and load_date = '{0}'
                            """.format(load_date)

        if (load_time != ''):
            if (not has_where_clause):
                has_where_clause = True
                sql_stmt += """
                            where load_time = '{0}'
                            """.format(load_time)
            else:
                sql_stmt += """
                            and load_time = '{0}'
                            """.format(load_time)


        sql_stmt += """
                    group by model_name, file_run_no, load_date, load_time
                    order by load_date, load_time desc
                    """

        sql_df = ysera_base.ysera_base.spark.sql(sql_stmt)

        psdf = sql_df.toPandas()

        output_file = 'arx2_test_import_report' + const.EXCEL_FILE_EXT
        writer = pd.ExcelWriter(output_file)
        psdf.to_excel(writer, index=False, sheet_name='Import Details')
        writer.save()

        FROM = "frank.policastro@transamerica.com"
        SUBJECT = "Sample ARX2 report"
        text = "See attached. Sample report generated from actuarialresultsdev.pfv_arx2"

        TO = ['frank.policastro@transamerica.com',
              'jeff.williamson@transamerica.com',
              'matthew.hoffman@transamerica.com']

        #TO = ['frank.policastro@transamerica.com']

        msg = MIMEMultipart()
        msg['From'] = FROM
        msg['Subject'] = SUBJECT
        msg.attach(MIMEText(text))

        part = MIMEBase('application', "octet-stream")
        part.set_payload(open("/homedir/fpolicastro/arx2_test_import_report.xlsx", "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment', filename="arx2_test_import_report.xlsx")
        msg.attach(part)

        server = smtplib.SMTP('127.0.0.1')
        server.sendmail(FROM, TO, msg.as_string())
        server.quit()
        pass