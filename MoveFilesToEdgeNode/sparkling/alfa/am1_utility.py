#!/opt/cloudera/parcels/Anaconda-4.0.0/bin/python
import io
import datetime
import uuid
import os
import sys
import time
import traceback
import subprocess
import pandas as pd
import numpy as np
from sparkling import Constants as const
from sparkling import ysera_base
from pyspark import SparkContext, SparkConf, HiveContext
from pyspark.sql import SQLContext, SparkSession, Row
from pyspark.sql.functions import *
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DecimalType

#######################################################################
#   Author: Frank Policastro
#   Date:   9.6.2018
#   Notes:  The purpose of the am1_utility class is as follows:
#           - Copy .AM1 files from a network share into HDFS
#           - Perform some transformations on the AM1 data in the
#             files like adding some additional columns
#           - Save the data to a landing area with the new columns
#           - Insert the landing area data into a compressed parquet
#             table
#
#   Change log:
#   9.6.2018        Initial Creation        Frank Policastro
#######################################################################

class am1_utility(ysera_base.ysera_base):

    #region default constructor
    def __init__(self, load_date="", load_time=""):
        ysera_base.ysera_base.__init__(self)
        self.load_date = load_date
        self.load_time = load_time

        self.am1_all = pd.DataFrame(columns=const.am1_pandas_schema)
        self.pd_list = []
        pass
    #endregion default constructor


    #region clean_up_landing_area
    def clean_up_landing_area(self):
        del_path = self.hdfs_path + self.landing_path + self.am1_table_name + '/*'
        self.uce.execute("{0} {1}".format(const.HdfsCommands.Delete.value, del_path))
    #endregion clean_up_landing_area


    #region land_file
    def land_files(self, src, dest):
        self.uce.execute("{0} {1} {2}".format(const.HdfsCommands.Put.value, src, dest))
        pass
    #endregion land_file


    #region parse_file_names
    def parse_file_names(self):
        results = []
        am1_path = self.hdfs_path + self.landing_path + self.am1_file_ext
        am1_files = ysera_base.ysera_base.sc.wholeTextFiles(am1_path).collect()

        print('1. sc.applicationId = ' + self.spark_app_id)

        for am1_file in am1_files:
            path_pieces = am1_file[0].split("/")
            file_pieces = path_pieces[9].split(".")
            model_name = file_pieces[0]
            proj_no = file_pieces[2]
            run_no = file_pieces[4]

            am1_data = str(am1_file[1]).rstrip('\r\n')

            # split file by lines
            rows = am1_data.split(os.linesep)
            # lines in file minus header
            row_count = len(rows) - 1

            temp = model_name, proj_no, run_no, row_count, am1_file[0]
            results.append(temp)
        return results
    #endregion parse_file_names


    #region process_file
    def process_file(self, row_tuple):
        model_name = str(row_tuple[0])
        file_proj_no = str(row_tuple[1])
        file_run_no = str(row_tuple[2])
        row_count = str(row_tuple[3])
        file_path = str(row_tuple[4])

        #am1_data = ysera_base.ysera_base.spark.read.csv(path=file_path, schema=self.am1_renamed_schema, sep='\t', header=True)
        am1_data = ysera_base.ysera_base.spark.read.csv(path=file_path, schema=const.am1_renamed_schema, sep='\t', header=True)
        am1_data = am1_data.withColumn('model_name', lit(model_name))
        am1_data = am1_data.withColumn('file_proj_no', lit(file_proj_no))
        am1_data = am1_data.withColumn('file_run_no', lit(file_run_no))
        am1_data = am1_data.withColumn('load_date', lit(self.load_date))
        am1_data = am1_data.withColumn('load_time', lit(self.load_time))

        if model_name.lower() == const.NB6_MODEL_NAME.lower():
            cond = [am1_data.dac_group == self.cohorts.dac_group_code,
                    am1_data.file_proj_no == self.cohorts.proj_no,
                    self.cohorts.cohort_model == const.NB6_COHORTS]
        else:
            cond = [am1_data.dac_group == self.cohorts.dac_group_code,
                    am1_data.file_proj_no == self.cohorts.proj_no,
                    self.cohorts.cohort_model == const.INFORCE_COHORTS]

        #new_am1 = am1_data.join(self.cohorts, cond, 'left').select(*self.am1_select_col_list)
        new_am1 = am1_data.join(self.cohorts, cond, 'left').select(*const.am1_select_col_list)

        final_am1 = new_am1.toPandas()

        final_am1['row_id'] = final_am1.apply(lambda row: self.get_uuid(), axis=1)

        final_am1 = final_am1[['segment_number', 'segment_record', 'alfa_plan_code', 'issue_age',
                               'gender', 'tax_rider', 'biz_unit', 'legal_entity_stat_comp', 'is_qualified',
                               'gmdb_type', 'gmib_type', 'reinsurance_type', 'cohort_group', 'glwb_type',
                               'issue_year', 'issue_month', 'is_new_business', 'policy_number', 'dac_group',
                               'category', 'is_current_qtr', 'account_value', 'withdrawal_benefit_amt', 'accum_benefit_amt',
                               'policy_count', 'separate_accnt_value', 'cqocs_margin_amt', 'cqocs_wb_benefit_amt',
                               'cqocs_ab_benefit_amt', 'cqocs_ab_fee_amt', 'cqocs_wbdb_fee_amt', 'cqocs_wb_fee_amt',
                               'cqocs_wb_reserve_amt', 'cqocs_ab_reserve_amt', 'pqocs_wb_benefit_amt', 'pqocs_ab_benefit_amt',
                               'pqocs_ab_fee_amt', 'pqocs_wbdb_fee_amt', 'pqocs_wb_fee_amt', 'pqocs_wb_reserve_amt',
                               'pqocs_ab_reserve_amt', 'cqocs_up20bps_wb_reserve_amt', 'cqocs_up20bps_ab_reserve_amt',
                               'cqocs_dn20bps_wb_reserve_amt', 'cqocs_dn20bps_ab_reserve_amt', 'cqocs_nomrgn_ab_fee_amt',
                               'cqocs_nomrgn_wbdb_fee_amt', 'cqocs_nomrgn_wb_fee_amt', 'cqocs_nomrgn_wb_reserve_amt',
                               'cqocs_nomrgn_ab_reserve_amt', 'cqocs_nomrgn_dn20bps_ab_reserve_amt',
                               'cqocs_nomrgn_up20bps_ab_reserve_amt', 'cqocs_nomrgn_dn20bps_wb_reserve_amt',
                               'cqocs_nomrgn_up20bps_wb_reserve_amt', 'file_proj_no', 'file_run_no', 'cohort_name',
                               'row_id', 'model_name', 'valuation_date', 'as_of_date', 'load_date', 'load_time']]

        self.am1_all = self.am1_all.append(final_am1)
        #self.am1_all.append(final_am1)
        pass
    #endregion process_file


    #region persist_data
    def persist_data(self):
        #am1_data = ysera_base.ysera_base.spark.createDataFrame(final_am1, self.am1_final_schema)

        print('self.am1_all = ' + str(list(self.am1_all)))
        print('const.am1_final_schema = ' + str(const.am1_final_schema))

        am1_data = ysera_base.ysera_base.spark.createDataFrame(self.am1_all, const.am1_final_schema)

        am1_data.repartition(50)

        ltable  = str(self.db_name) + ".tl_" + str(self.am1_table_name)
        ptable  = str(self.db_name) + ".tp_" + str(self.am1_table_name)

        #am1_data.coalesce(1).write.insertInto(ltable)
        am1_data.write.insertInto(ltable)

        am1_data.createOrReplaceTempView("am1_data")

        sql_stmt = """
                   INSERT INTO TABLE {0}
                   PARTITION (model_name, valuation_date, as_of_date, load_date, load_time)
                   SELECT am1.segment_number,
                          am1.segment_record,
                          am1.alfa_plan_code,
                          am1.issue_age,
                          am1.gender,
                          am1.tax_rider,
                          am1.biz_unit,
                          am1.legal_entity_stat_comp,
                          am1.is_qualified,
                          am1.gmdb_type,
                          am1.gmib_type,
                          am1.reinsurance_type,
                          am1.cohort_group,
                          am1.glwb_type,
                          am1.issue_year,
                          am1.issue_month,
                          am1.is_new_business,
                          am1.policy_number,
                          am1.dac_group,
                          am1.category,
                          am1.is_current_qtr,
                          am1.account_value,
                          am1.withdrawal_benefit_amt,
                          am1.accum_benefit_amt,
                          am1.policy_count,
                          am1.separate_accnt_value,
                          am1.cqocs_margin_amt,
                          am1.cqocs_wb_benefit_amt,
                          am1.cqocs_ab_benefit_amt,
                          am1.cqocs_ab_fee_amt,
                          am1.cqocs_wbdb_fee_amt,
                          am1.cqocs_wb_fee_amt,
                          am1.cqocs_wb_reserve_amt,
                          am1.cqocs_ab_reserve_amt,
                          am1.pqocs_wb_benefit_amt,
                          am1.pqocs_ab_benefit_amt,
                          am1.pqocs_ab_fee_amt,
                          am1.pqocs_wbdb_fee_amt,
                          am1.pqocs_wb_fee_amt,
                          am1.pqocs_wb_reserve_amt,
                          am1.pqocs_ab_reserve_amt,
                          am1.cqocs_up20bps_wb_reserve_amt,
                          am1.cqocs_up20bps_ab_reserve_amt,
                          am1.cqocs_dn20bps_wb_reserve_amt,
                          am1.cqocs_dn20bps_ab_reserve_amt,
                          am1.cqocs_nomrgn_ab_fee_amt,
                          am1.cqocs_nomrgn_wbdb_fee_amt,
                          am1.cqocs_nomrgn_wb_fee_amt,
                          am1.cqocs_nomrgn_wb_reserve_amt,
                          am1.cqocs_nomrgn_ab_reserve_amt,
                          am1.cqocs_nomrgn_dn20bps_ab_reserve_amt,
                          am1.cqocs_nomrgn_up20bps_ab_reserve_amt,
                          am1.cqocs_nomrgn_dn20bps_wb_reserve_amt,
                          am1.cqocs_nomrgn_up20bps_wb_reserve_amt,
                          am1.file_proj_no,
                          am1.file_run_no,
                          am1.cohort_name,
                          am1.row_id,
                          am1.model_name as model_name,
                          am1.valuation_date as valuation_date,
                          am1.as_of_date as as_of_date,
                          am1.load_date as load_date,
                          am1.load_time as load_time
                   FROM   am1_data am1
                   """.format(ptable)

        ysera_base.ysera_base.spark.sql(sql_stmt)
        pass
    #endregion persist_data