#!/opt/cloudera/parcels/Anaconda-4.0.0/bin/python
import io
import datetime
import uuid
from UnixCmdExecutor import UnixCmdExecutor
from PandasUtilities import PandasUtilities
import os
import sys
import time
import Constants as const
import traceback
import logging
import subprocess
import pandas as pd
from pandas import pivot_table
import numpy as np
from pyspark import SparkContext, SparkConf, HiveContext
from pyspark.sql import SQLContext, SparkSession, Row
from pyspark.sql.functions import *
from pyspark.sql.types import *


spark = SparkSession.builder \
                    .appName("SparkSessionZipsExample") \
                    .config("hive.exec.dynamic.partition", "true") \
                    .config("hive.exec.dynamic.partition", "nonstrict") \
                    .enableHiveSupport() \
                    .getOrCreate()

hc = HiveContext(spark)
hc.sql("SET hive.exec.dynamic.partition = true;")
hc.sql("SET hive.exec.dynamic.partition.mode = nonstrict;")





def tester(record):
    print(record)

def tester2(path):
    am1Data = sc.textFile('hdfs://nameservice1' + path)
    print(path + ' - ' + str(am1Data.count()))

def addGuid():
    return str(uuid.uuid4())

def addProcessedTs(processed_ts):
    return str(processed_ts)

class SparkTest():

    def __init__(self):
        pass

    def run(self):
        am1Data = hc.read.csv(path='hdfs://nameservice1/projects/actuarialresultsdev/landing/Base.Proj.011.Run.011.Rreq.032.Inv015.Scn.Mean._.Am1',
                              sep='\t', header=True, inferSchema=True)
        #am1Data = sc.textFile()
        print('am1 #11 count = ' + str(am1Data.count()))

    def run2(self):
        #am1Data = sc.textFile('hdfs://nameservice1/projects/actuarialresultsdev/landing/Base.Proj.012.Run.012.Rreq.032.Inv015.Scn.Mean._.Am1')
        am1Data = hc.read.csv(path='hdfs://nameservice1/projects/actuarialresultsdev/landing/Base.Proj.012.Run.012.Rreq.032.Inv015.Scn.Mean._.Am1',
                              sep='\t', header=True, inferSchema=True)
        print('am1 #12 count = ' + str(am1Data.count()))

    def run3(self):
        path_list = []
        source_dir = '/projects/actuarialresultsdev/landing'
        cmd = 'hdfs dfs -find {} -name *.Am1'.format(source_dir).split()
        files = subprocess.check_output(cmd).strip().split('\n')
        for path in files:
            path_list.append(path)
            filename = path.split(os.path.sep)[-1].split('.txt')[0]
            #self.clean_am1(path)
            print(path, filename)
        return path_list

    def run4(self):
        #data = sc.textFile('hdfs://nameservice1/projects/actuarialresultsdev/landing/Base.Proj.012.Run.012.Rreq.032.Inv015.Scn.Mean._.Am1')
        data = hc.read.csv(path='hdfs://nameservice1/projects/actuarialresultsdev/landing/Base.Proj.012.Run.012.Rreq.032.Inv015.Scn.Mean._.Am1',
                           sep='\t', header=True, inferSchema=True)
        firstRow = data.first()
        data = data.filter(lambda row: row != firstRow)
        #data.saveAsTextFile('./clean_am1_test')
        #data.saveAsTextFile('clean_am1_test')
        data.foreach(tester)

    def clean_am1(self, path):
        #data = sc.textFile('hdfs://nameservice1' + path)
        data = hc.read.csv(path='hdfs://nameservice1' + path, sep='\t', header=True, inferSchema=True)
        firstRow = data.first()
        data = data.filter(lambda row: row != firstRow)
        data.saveAsTextFile('clean_am1_test')

    def run_wc(self):
        processed_ts = time.time()
        #am1Data = sc.textFile('hdfs://nameservice1/projects/actuarialresultsdev/landing/*.Am1')
        #am1Data = sc.textFile('hdfs://nameservice1/projects/actuarialresultsdev/landing/variable_annuity/fair_value/*.Am1')
        am1Data = hc.read.csv(path='hdfs://nameservice1/projects/actuarialresultsdev/landing/variable_annuity/fair_value/*.Am1',
                              sep='\t', header=True, inferSchema=True)
        print('am1 all rows count = ' + str(am1Data.count()))
        firstRow = am1Data.first()
        print(type(firstRow))
        print('firstRow = ' + firstRow)
        am1Data = am1Data.filter(lambda row: row != firstRow)
        print('am1 rows count after filter = ' + str(am1Data.count()))
        am1Data.saveAsTextFile('clean_am1_test')

    def run_sc(self):
        #data = sqlContext.read.csv(path='hdfs://nameservice1/projects/actuarialresultsdev/landing/Base.Proj.012.Run.012.Rreq.032.Inv015.Scn.Mean._.Am1',
        #                           sep='\t', header=True, inferSchema=True)

        data = hc.read.csv(path='hdfs://nameservice1/projects/actuarialresultsdev/landing/variable_annuity/fair_value/*.Am1',
                           sep='\t', header=True, inferSchema=True)


        #data.printSchema()
        some_data = data.limit(3).collect()
        #print(some_data)
        print(type(data))

        actValDs = data.select('AVIF_ts_E0', 'F133PVRptInitWBFace_th_ELastValue')
        print(type(actValDs))
        actValRdd = actValDs.rdd.take(5)
        print(type(actValRdd))

        for avr in actValRdd:
            print('AVIF = ' + str(avr.AVIF_ts_E0))
            print('F133PVRptInitWBFace_th_ELastValue = ' + str(avr.F133PVRptInitWBFace_th_ELastValue))
            print(type(avr))

        actValDf = sc.createDataFrame(actValRdd)
        print(type(actValDf))

        udfAddGuid = udf(addGuid, pst.StringType())
        data_with_guid = data.withColumn("row_id", udfAddGuid())
        print(data_with_guid.show(5))

        #processed_ts = str(datetime.datetime.utcnow())
        #udfAddProcessedTs = udf(addProcessedTs, pst.StringType())
        #data_w_guid_and_ts = data_with_guid.withColumn("processedTs", udfAddProcessedTs(processed_ts))
        #print(data_w_guid_and_ts.show(5))

        #print(data.select('AVIF_ts_E0').show(4))

        #data.registerTempTable('data_tbl')
        #df1 = sqlContext.sql('select DACGroup as dac_group, SUM(AVIF_ts_E0) as acnt_val from data_tbl group by DACGroup order by 1')
        #print('df1 = ' + str(df1))

        #df = pd.read_csv('./Base.Proj.011.Run.011.Rreq.032.Inv015.Scn.Mean._.Am1', sep='\t', skiprows=1, header=None, names=const.AM1_COLUMN_NAMES_ORIG)
        #print(df.head(2))
        #print(type(df))

        #df_col = pd.read_csv('./Base.Proj.025.Run.025.Rreq.032.Inv015.Scn.Mean._.Am1', sep='\t', skiprows=1, header=None, names=const.AM1_ORIGINAL_NAMES)
        #print(df_col.head(2))
        #print(type(df_col))

        #df_col['F133PVMarginOCS_th_ELastValue'] = df_col['F133PVMarginOCS_th_ELastValue'].astype('float64').fillna(0.0)
        #table = pivot_table(df_col, values='F133PVMarginOCS_th_ELastValue', index=['ck.Gender', 'ck.IssAge'], aggfunc=np.sum)
        #print('table.data = ' + str(table.values))

        #dates = pd.date_range(start='1/1/2018', freq='QS', periods=5)
        #print(dates)

        #filepaths = ['./Base.Proj.011.Run.011.Rreq.032.Inv015.Scn.Mean._.Am1',
        #             './Base.Proj.025.Run.025.Rreq.032.Inv015.Scn.Mean._.Am1',
        #             './Base.Proj.022.Run.022.Rreq.032.Inv015.Scn.Mean._.Am1',
        #             './Base.Proj.018.Run.018.Rreq.032.Inv015.Scn.Mean._.Am1',
        #             './Base.Proj.014.Run.014.Rreq.032.Inv015.Scn.Mean._.Am1',
        #             './Base.Proj.003.Run.003.Rreq.032.Inv015.Scn.Mean._.Am1']

        #df_test = pd.concat(map(lambda file: pd.read_csv(file, sep='\t', skiprows=1, header=None, names=const.AM1_COLUMN_NAMES_ORIG), filepaths))
        #print('df_test.count = ' + str(df_test.count()))
        #grouped = df_test.groupby(['dac_group'])
        #print('grouped.count = ' + str(grouped.count()))
        #print('grouped.sum = ' + str(grouped.sum()))
        pass

    def list_hdfs_path(self, path):
        uce = UnixCmdExecutor()
        return uce.execute("{0} {1}".format(const.HdfsCommands.List.value, path))

    def list_hdfs_path_name_only(self, path):
        uce = UnixCmdExecutor()
        return uce.execute("{0} {1}".format(const.HdfsCommands.ListNameOnly.value, path))

    def put_into_hdfs(self, src, dest):
        uce = UnixCmdExecutor()
        uce.execute("{0} {1} {2}".format(const.HdfsCommands.Put.value, src, dest))


    def parquet_insert(self):

        old_schema = StructType([
                     #StructField("row_id", StringType(), True),
                     #StructField("processed_ts", TimestampType(), True),
                     #StructField("model_name", StringType(), True),
                     #StructField("file_proj_no", StringType(), True),
                     #StructField("file_run_no", StringType(), True),
                     #StructField("alfa_run_id", StringType(), True),
                     StructField("//segmentnumber", StringType(), True),
                     StructField("segmentrecord", StringType(), True),
                     StructField("ck.Plan", StringType(), True),
                     StructField("ck.IssAge", IntegerType(), True),
                     StructField("ck.Gender", StringType(), True),
                     StructField("ck.Class", StringType(), True),
                     StructField("ck.Char1", StringType(), True),
                     StructField("ck.Char2", StringType(), True),
                     StructField("ck.Char3", StringType(), True),
                     StructField("ck.Char4", StringType(), True),
                     StructField("ck.Char5", StringType(), True),
                     StructField("ck.Char6", StringType(), True),
                     StructField("ck.Char7", StringType(), True),
                     StructField("ck.Char8", StringType(), True),
                     StructField("ck.IssYear", IntegerType(), True),
                     StructField("ck.IssMon", IntegerType(), True),
                     StructField("ck.NewBus", StringType(), True),
                     StructField("PolicyNumber", StringType(), True),
                     StructField("DACGroup", StringType(), True),
                     StructField("sys.category", StringType(), True),
                     StructField("ValDate", StringType(), True),
                     StructField("AsOfDate", StringType(), True),
                     StructField("CurrQtr", StringType(), True),
                     StructField("AVIF_ts_E0", DecimalType(30, 8), True),
                     StructField("F133PVRptInitWBFace_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVRptInitABFace_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVRptInitLives_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVRptInitSAV_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVMarginOCS_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMWBBenOCS_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMABBenOCS_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMABFeeOCS_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMWBDBFeeOCS_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMWBFeeOCS_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMWBResOCS_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMABResOCS_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMWBBenOCSPQ_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMABBenOCSPQ_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMABFeeOCSPQ_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMWBDBFeeOCSPQ_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMWBFeeOCSPQ_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMWBResOCSPQ_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMABResOCSPQ_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMWBResOCSUp_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMABResOCSUp_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMWBResOCSDn_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVGMABResOCSDn_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVNMGMABFeeOCS_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVNMGMWBDBFeeOCS_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVNMGMWBFeeOCS_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVNMGMWBResNoOCS_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVNMGMABResNoOCS_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVNMGMABResOCSDn_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVNMGMABResOCSUp_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVNMGMWBResOCSDn_th_ELastValue", DecimalType(30, 8), True),
                     StructField("F133PVNMGMWBResOCSUp_th_ELastValue", DecimalType(30, 8), True)])


        am1Data = spark.sparkContext.wholeTextFiles('hdfs://nameservice1/projects/actuarialresultsdev/db/am1_landing/*.Am1').collect()

        for file in am1Data:
            fnp = file[0].split("/")
            #print('file_name = ' + str(fnp))
            fnp2 = fnp[7].split(".")
            proj_no = fnp2[2]
            run_no = fnp2[4]
            print('proj_no = ' + str(proj_no))
            print('run_no = ' + str(run_no))

        am1_data = hc.read.csv(path='hdfs://nameservice1/projects/actuarialresultsdev/db/am1_landing/*.Am1',
                                  schema=old_schema, sep='\t', header=True)


        final_df = am1_data.withColumn("model_name", lit("Base"))
        final_df = final_df.withColumn("processed_ts", lit(datetime.datetime.utcnow().strftime("%Y-%m-%d-%H-%M-%S")))
        final_df = final_df.withColumnRenamed("//segmentnumber", "segmentnumber")
        final_df = final_df.withColumnRenamed("ck.Plan", "Plan")
        final_df = final_df.withColumnRenamed("ck.IssAge", "IssAge")
        final_df = final_df.withColumnRenamed("ck.Gender", "Gender")
        final_df = final_df.withColumnRenamed("ck.Class", "Class")
        final_df = final_df.withColumnRenamed("ck.Char1", "Char1")
        final_df = final_df.withColumnRenamed("ck.Char2", "Char2")
        final_df = final_df.withColumnRenamed("ck.Char3", "Char3")
        final_df = final_df.withColumnRenamed("ck.Char4", "Char4")
        final_df = final_df.withColumnRenamed("ck.Char5", "Char5")
        final_df = final_df.withColumnRenamed("ck.Char6", "Char6")
        final_df = final_df.withColumnRenamed("ck.Char7", "Char7")
        final_df = final_df.withColumnRenamed("ck.Char8", "Char8")
        final_df = final_df.withColumnRenamed("ck.issyear", "IssYear")
        final_df = final_df.withColumnRenamed("ck.issmon", "IssMon")
        final_df = final_df.withColumnRenamed("ck.NewBus", "NewBus")
        final_df = final_df.withColumnRenamed("sys.Category", "Category")

        print(final_df.printSchema())

        #some_data = am1_data.limit(3).collect()
        #print(some_data)

        #am1_data.registerTempTable('am1_data')
        #first_row = spark.sql('select model_name, valuation_date, as_of_date from am1_data limit 1').first()

        #am1_data.write.parquet(path="hdfs://nameservice1/projects/actuarialresultsdev/db/am1_landing/am1.parquet",
        #                       mode="append", compression="snappy")


        #drop the table if exists to get around existing table error
        hc.sql("use actuarialresultsdev")
        hc.sql("DROP TABLE IF EXISTS actuarialresultsdev.am1_test")
        #save as a hive table

        #**TBLPROPERTIES('serialization.null.format'='');**

        #final_df.coalesce(5).write.option("serialization.null.format", "''") \
        #                    .partitionBy(["model_name", "processed_ts", "valdate", "asofdate"]) \
        #                    .format('parquet').mode("overwrite").saveAsTable("actuarialresultsdev.am1_test")

        final_df.coalesce(1).write.option("serialization.null.format", "''") \
                            .format('text').mode("append").saveAsTable("actuarialresultsdev.am1_test")

        hc.sql("REFRESH actuarialresultsdev.am1_test")

        df3 = hc.read.parquet("hdfs://nameservice1/projects/actuarialresultsdev/db/am1_test/")
        df3.printSchema()
        print(df3.head(3))

        #make a similar query against the hive table
        val = hc.sql("SELECT policynumber FROM am1_test WHERE policynumber == '0643809'").collect()
        print(type(val))

        for v in val:
            print('v = ' + str(v))

        #print('policy number = ' + str(val.policynumber))

        #tbl_count = spark.sql('select count(*) from alfa_am1_input').first()[0]
        #print('tbl_count = ' + str(tbl_count))

        #data_row = spark.sql('select * from alfa_am1_input limit 10').collect()
        #print('data_row = ' + str(data_row))

        #sqlStmt = """LOAD DATA INPATH '/projects/actuarialresultsdev/db/am1_landing/am1.parquet'
        #             OVERWRITE INTO TABLE actuarialresultsdev.alfa_am1_input
        #             PARTITION (model_name='{0}',
        #                        processed_ts='{1}',
        #                        valuation_date='{2}',
        #                        as_of_date='{3}') """.format('Base',
        #                                                     datetime.datetime.utcnow().strftime("%Y-%m-%d-%H-%M-%S"),
        #                                                     '201803',
        #                                                     '201803')

        #sqlContext.sql(sqlStmt)

        #colList = ["model_name", "processed_ts", "valuation_date", "as_of_date"]

        #am1_data.coalesce(10).write.format('parquet').mode("overwrite").insertInto("actuarialresultsdev.alfa_am1_input")

        #am1_data.write.saveAsTable(path='hdfs://nameservice1/projects/actuarialresultsdev/db/alfa_am1_input/',
        #                           name='alfa_am1_input', format='parquet', mode="append", compression="snappy")

        #am1_data.write().mode(SaveMode.Append).format("parquet").partitionBy()

        #.parquet(path="hdfs://nameservice1/projects/actuarialresultsdev/db/alfa_am1_input/",
        #                       mode="append",
        #                       partitionBy="model_name, valuation_date, as_of_date, processed_ts",
        #                       compression="snappy")

        #am1_data = sqlContext.read.csv(path='hdfs://nameservice1/projects/actuarialresultsdev/db/am1_landing/*.txt',
        #                               schema=old_schema, sep='\t', header=False)

        am1_schema =  StructType([
                      StructField("row_id", StringType(), True),
                      StructField("processed_ts", TimestampType(), True),
                      StructField("alfa_run_id", IntegerType(), True),
                      StructField("model_name", StringType(), True),
                      StructField("file_proj_no", StringType(), True),
                      StructField("file_run_no", StringType(), True),
                      StructField("segment_number", StringType(), True),
                      StructField("segment_record", StringType(), True),
                      StructField("alfa_plan_code", StringType(), True),
                      StructField("issue_age", IntegerType(), True),
                      StructField("gender", StringType(), True),
                      StructField("tax_rider", StringType(), True),
                      StructField("biz_unit", StringType(), True),
                      StructField("legal_entity_stat_comp", StringType(), True),
                      StructField("is_qualified", StringType(), True),
                      StructField("gmdb_type", StringType(), True),
                      StructField("gmib_type", StringType(), True),
                      StructField("reinsurance_type", StringType(), True),
                      StructField("cohort_group", StringType(), True),
                      StructField("glwb_type", StringType(), True),
                      StructField("issue_year", IntegerType(), True),
                      StructField("issue_month", IntegerType(), True),
                      StructField("is_new_business", StringType(), True),
                      StructField("policy_number", StringType(), True),
                      StructField("cohort_name", StringType(), True),
                      StructField("dac_group", StringType(), True),
                      StructField("category", StringType(), True),
                      StructField("valuation_date", StringType(), True),
                      StructField("as_of_date", StringType(), True),
                      StructField("is_current_qtr", StringType(), True),
                      StructField("account_value", DecimalType(30, 8), True),
                      StructField("withdrawal_benefit_amt", DecimalType(30, 8), True),
                      StructField("accum_benefit_amt", DecimalType(30, 8), True),
                      StructField("policy_count", DecimalType(30, 8), True),
                      StructField("separate_accnt_value", DecimalType(30, 8), True),
                      StructField("cqocs_margin_amt", DecimalType(30, 8), True),
                      StructField("cqocs_wb_benefit_amt", DecimalType(30, 8), True),
                      StructField("cqocs_ab_benefit_amt", DecimalType(30, 8), True),
                      StructField("cqocs_ab_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_wbdb_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_wb_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("pqocs_wb_benefit_amt", DecimalType(30, 8), True),
                      StructField("pqocs_ab_benefit_amt", DecimalType(30, 8), True),
                      StructField("pqocs_ab_fee_amt", DecimalType(30, 8), True),
                      StructField("pqocs_wbdb_fee_amt", DecimalType(30, 8), True),
                      StructField("pqocs_wb_fee_amt", DecimalType(30, 8), True),
                      StructField("pqocs_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("pqocs_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_up20bps_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_dn20bps_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_dn20bps_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_up20bps_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_ab_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_wbdb_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_wb_fee_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_up20bps_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_up20bps_ab_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_dn20bps_wb_reserve_amt", DecimalType(30, 8), True),
                      StructField("cqocs_nomrgn_dn20bps_ab_reserve_amt", DecimalType(30, 8), True)])

def main():
    try:

        t = SparkTest()

        t.parquet_insert()

        #t.run_sc()
        #t.run_wc()

        #print(t.list_hdfs_path(const.LANDING_PATH))

        #fv_path = const.LANDING_PATH + const.ProductLines.VARIABLE_ANNUITY.value + '/' + const.Models.FAIR_VALUE.value

        #t.put_into_hdfs('/data1/mounts/ValTest/Frank.Policastro/alfa_input_orig/*/AM1/*.Am1 ', fv_path)

        #am1_list = t.list_hdfs_path_name_only(fv_path)

        #for am1 in am1_list.split(const.NEW_LINE):
        #    print(am1)

        #print(t.list_hdfs_path_name_only(const.DB_HDFS_PATH + 'new_arx2'))

    except Exception as ex:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback)

if __name__ == '__main__':
    main()
