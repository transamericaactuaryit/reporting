#!/opt/cloudera/parcels/Anaconda-4.0.0/bin/python
import sys
import subprocess
import time
import commands
import re
import traceback
import subprocess

class fire_starter():

    SJ_EXECUTOR_COUNT = 4
    SJ_EXECUTOR_CORES = 4
    SJ_DRIVER_MEMORY = "10G"
    SJ_EXECUTOR_MEMORY = "10G"
    SJ_PORT_MAX_RETRIES = 50
    YARN_MEMORY_OVERHEAD = 8192
    PYTHON_FILE_NAME = "/home/fpolicastro/main.py"
    OUTPUT_FILE_NAME = "/home/fpolicastro/output.log"
    WORK_DIR = "."

    def __init__(self):
        pass

    def submit_spark_job(self, jobs=None):

        queue = [subprocess.Popen([job], shell=True) for job in jobs]

        while queue:
            for job in queue:
                retcode = job.poll()
                if retcode is not None:
                    queue.remove(job)
                    break
                else:
                    time.sleep(.1)
                    continue

def main():
    try:
        spark_submit_cmd = """spark2-submit --num-executors {0} --executor-cores {1} --driver-memory {2} --executor-memory {3} --conf spark.port.maxRetries={4} --conf spark.yarn.executor.memoryOverhead={5} main.py > output.log"""
        spark_submit_cmd = spark_submit_cmd.format(fire_starter.SJ_EXECUTOR_COUNT, fire_starter.SJ_EXECUTOR_CORES, fire_starter.SJ_DRIVER_MEMORY, fire_starter.SJ_EXECUTOR_MEMORY, fire_starter.SJ_PORT_MAX_RETRIES, fire_starter.YARN_MEMORY_OVERHEAD)
        jobs = [spark_submit_cmd]

        fs = fire_starter()
        fs.submit_spark_job(jobs)
    except Exception as ex:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback)


if __name__ == '__main__':
    main()
