#!/opt/cloudera/parcels/Anaconda-4.0.0/bin/python
import io
from PandasUtilities import PandasUtilities
import os
import re
import sys

import traceback
import inspect
import uuid
import time
#import requests
import datetime
import Constants as const
from HdfsUtilities import HdfsUtilities
#from UnixCmdExecutor import UnixCmdExecutor
from collections import OrderedDict
import pandas as pd

def process_file(processed_ts, full_file_path):
    #################################################################
    #pu = PandasUtilities()

    #file_name = os.path.basename(full_file_path)
    #file_name_pieces = file_name.split(const.PERIOD)

    #inforce_cohorts_df = pu.convert_file_to_data_frame(const.INFORCE_COHORTS_PATH, const.ColumnNameSet.COHORT, 0)
    #nb6_cohorts_df = pu.convert_file_to_data_frame(const.NB6_COHORTS_PATH, const.ColumnNameSet.COHORT, 0)
    #am1_df = pu.convert_file_to_data_frame(full_file_path, const.ColumnNameSet.AM1_ORIG, 1)

    #am1_df['row_id'] = str(uuid.uuid4())
    #am1_df['model_name'] = file_name_pieces[0]
    #am1_df['file_proj_no'] = file_name_pieces[2]
    #am1_df['file_run_no'] = file_name_pieces[4]
    #am1_df['processed_ts'] = str(processed_ts)
    #am1_df['alfa_run_id'] = 2

    ##if str(am1_df.model_name[0]).lower() == const.NB6_MODEL_NAME.lower():
    #if str(am1_df.head(1).model_name).lower() == const.NB6_MODEL_NAME.lower():
    #    am1_df = pd.merge(am1_df, nb6_cohorts_df, how='left', left_on='DACGroup', right_on='dac_group', suffixes=["_L", "_R"])
    #else:
    #    am1_df = pd.merge(am1_df, inforce_cohorts_df, how='left', left_on=['DACGroup', 'file_run_no'], right_on=['dac_group', 'file_run_no'])

    #col_list = am1_df.columns.tolist()
    #cg_idx = col_list.index(const.COL_DAC_GROUP)
    #col_list.insert(cg_idx, col_list.pop())
    #am1_df = am1_df[col_list]
    #################################################################

    #am1_df.rename(columns={ 'file_run_no_L' : 'file_run_no' }, inplace=True)
    #am1_df.drop('file_run_no_R', axis=1, inplace=True)
    #am1_df.drop('dac_group', axis=1, inplace=True)

    #print(list(am1_df))
    #print(am1_df.head(1))

    # how to reorder columns in one call
    #cols = df.columns.tolist()
    #cols = [cols[-1]]+cols[:-1] # or whatever change you need
    #df.reindex(columns=cols)

    #tmp.to_parquet(full_file_path.lower().replace(const.AM1_FILE_EXT, '.parquet'))

    #with io.open(full_file_path.lower().replace(const.AM1_FILE_EXT, const.TXT_FILE_EXT), "w", const.FILE_BUFFER_SIZE, encoding="utf-8") as output:
    #    output.write(tmp.to_csv(sep=const.TAB_CHAR, header=True, index=False, encoding='utf-8'))

    file_name = os.path.basename(full_file_path)
    file_name_pieces = file_name.split(const.PERIOD)
    with io.open(full_file_path, "r", const.FILE_BUFFER_SIZE) as input:
        headerLine = input.readline()
        with io.open(full_file_path.lower().replace(const.AM1_FILE_EXT, const.TXT_FILE_EXT), "w", const.FILE_BUFFER_SIZE) as output:
            for line in input:
                new_line = str(uuid.uuid4()) + const.TAB_CHAR
                new_line += str(processed_ts) + const.TAB_CHAR
                new_line += file_name_pieces[0] + const.TAB_CHAR      # model name from file name
                new_line += file_name_pieces[2] + const.TAB_CHAR      # proj num from file name
                new_line += file_name_pieces[4] + const.TAB_CHAR      # run num from file name
                new_line += '2' + const.TAB_CHAR                      # placeholder for alfa_run_id
                new_line += line
                output.write(new_line)


####################################################################################################
# ARX2 Section
####################################################################################################

def get_type(var):
    try:
        if int(var) == float(var):
            return const.DataTypes.INT.value
    except:
        try:
            float(var)
            return const.DataTypes.FLOAT.value
        except:
            return const.DataTypes.STRING.value



def extract_cycles(full_file_path):
    try:
        rawLines = []
        with open(full_file_path) as f:
            for lines in f:
                rawLines.append(lines)

        x = rawLines
        variableList = []
        cycles = 0

        for index, i in enumerate(x):
            if re.search(r'Cycles: (\d+)', i):
                g = re.search(r'(\d+)', i[::]).span()
                cycles = int(i[g[0]:g[1]])
                break

        return cycles
    except Exception as ex:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback)



def process_arx_file(processed_ts, full_file_path, black_list, section_titles):

    arx_file = os.path.basename(full_file_path)

    # "********************" means number is too big to be displayed which indicates
    # something is possibly wrong or a scenario is so extreme

    col_list = []
    rows_list = []
    new_list = []
    tmp_list = []
    title = ''
    section = ''

    cycles = extract_cycles(full_file_path)

    file_name_pieces = arx_file.split(const.PERIOD)

    p = re.compile(r'(?P<Month>(\d{1,2}))/(?P<Year>(?:\d{4}))')
    q = re.compile(r'"[^\s{1}\d{1,4}/\d{1,4}]\w+[\s*|&|(|)]*.*')

    with io.open(full_file_path, "r", const.FILE_BUFFER_SIZE) as input:
        all_lines = input.readlines()

    for line in all_lines:
        if not line.startswith(tuple(black_list)):
            line = re.sub(r'""', '', line, count=0)
            line = re.sub(r'"""[-]+"', '', line, count=0)

            i = p.finditer(line)
            for m in i:
                col_list.append(m.group())

            tmp_list = []

            if line.startswith(tuple(section_titles)):
                section = line.replace('"', '')
                section = section.replace('\n', '')
                continue

            # match lines that ends with a lot of numbers
            matched_lines = re.findall(q, line)

            bq_counter = 0
            for ml in matched_lines:
                title = ''
                line_pieces = ml.split()

                for item in line_pieces:
                    if get_type(item) == const.DataTypes.STRING.value:
                        if str(item) == '"':
                            bq_counter += 1
                            if bq_counter == 2:
                                tmp_list.insert(0, const.NA)   # change this to be N/A instead of 0
                                bq_counter = 0
                        elif str(item).startswith(const.ARX_BAD_VALUE):
                            tmp_list.append(const.NA)
                        elif len(title) == 0:
                            title += item.strip('"')
                        else:
                            title += (' ' + item.strip('"'))
                        pass
                    elif get_type(item) == const.DataTypes.FLOAT.value:
                        tmp_list.append(float(item))
                    elif get_type(item) == const.DataTypes.INT.value:
                        tmp_list.append(int(item))


                tmp_list.insert(0, title)
                tmp_list.insert(0, section)
                tmp_list.insert(0, file_name_pieces[2])
                tmp_list.insert(0, file_name_pieces[0])
                rows_list.append(tmp_list)
    pass

    #dates = pd.date_range(start='1/1/2018', freq='QS', periods=420)

    if len(col_list) > 0:
        arx2_col_list = list(OrderedDict((x, True) for x in col_list).keys())
        new_list = list(OrderedDict((x, True) for x in col_list).keys())
        new_list.insert(0, const.M_TITLE)
        new_list.insert(0, const.M_SECTION)
        new_list.insert(0, const.M_RUN_NO)
        new_list.insert(0, const.M_MODEL_NAME)


    pu = PandasUtilities()
    df = pu.create_data_frame(new_list, rows_list)


    #df_dup = df[df.duplicated(keep='first')]
    #df_dup2 = df.loc[df.duplicated(keep=False), :]
    #print(df_dup)
    #print(df_dup2)

    print('arx2 before = ' + str(df.shape))
    df.drop_duplicates(inplace=True)
    print('arx2 after = ' + str(df.shape))

    table = pa.Table.from_pandas(df)
    pq.write_table(table, 'example.parquet')

    dft = df.transpose()

    #output_file = arx_file + const.EXCEL_FILE_EXT
    output_file = full_file_path.lower().replace(const.ARX2_FILE_EXT, const.EXCEL_FILE_EXT)
    writer = pd.ExcelWriter(output_file)
    df.to_excel(writer, index=False, sheet_name='ARX2')
    dft.to_excel(writer, header=False, sheet_name='ARX2 Transposed')
    writer.save()

    arx2_rows_list = []

    #arx_output_file = const.ARX2_FILE_PATH + arx_file.stem + const.TXT_FILE_EXT
    #arx_output_file = arx_file + const.TXT_FILE_EXT
    arx_output_file = full_file_path.lower().replace(const.ARX2_FILE_EXT, const.TXT_FILE_EXT)
    print('Working on ' + arx_output_file)

    t = 0;

    for row in rows_list:
        for idx, item in enumerate(arx2_col_list, start=4):
            tmp2_list = []
            tmp2_list.append(str(row[0]))
            tmp2_list.append(str(row[1]))
            tmp2_list.append(str(row[2]))
            tmp2_list.append(str(row[3]))

            tmp2_list.append(str(t))

            date_pieces = item.split(const.FWD_SLASH)

            tmp2_list.append(str(date_pieces[0]))
            tmp2_list.append(str(date_pieces[1]))
            #tmp2_list.append(str(item))

            if str(row[idx]) != const.NA:
                tmp2_list.append(str(row[idx]))
            else:
                tmp2_list.append(str(0))

            arx2_rows_list.append(tmp2_list)

            if t < cycles:
                t += 1
            else:
                t = 0

    df2 = pu.create_data_frame(const.ColumnNameSet.ARX2.value, arx2_rows_list)
    print('arx2 flattened before = ' + str(df2.shape))
    df2.drop_duplicates(inplace=True)
    print('arx2 flattened after = ' + str(df2.shape))

    try:
        # drop rows I don't care about

        # To select rows whose column value equals a scalar, some_value, use ==:
        # df.loc[df['column_name'] == some_value]

        # To select rows whose column value is in an iterable, some_values, use isin:
        # df.loc[df['column_name'].isin(some_values)]

        # Combine multiple conditions with &:
        # df.loc[(df['column_name'] == some_value) & df['other_column'].isin(some_values)]

        title_list = ['Discount Factor (before Tax)',
                      'Discount Factor (with OCS)',
                      'Lives Inforce (GMIB Reins only)',
                      'Fund Value Inforce (GMIB Reins only)',
                      'GMIB Reins Premium',
                      'GMIB Reins Benefit after Aggregate Limit']

        new_df = df2.loc[(df2['section'] == 'HEDGE COST RECOGNITION FOR DAC') & df2['title'].isin(title_list)]

        new_df.to_csv(arx_output_file, sep='\t', header=False, index=False)
    except Exception as ex:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback)

    #df2.to_csv(arx_output_file, sep='\t', header=False, index=False)


#def call_model_manager_db():
#    r = requests.get('http://crasmand01:8020/api/model/2146')
#    print('status_code = ' + str(r.status_code))
#    print('good status? = ' + str(r.status_code == requests.codes.ok))
#    print('encoding = ' + str(r.encoding))
#    print('text = ' + str(r.text))
#    print('content = ' + str(r.content))
#    print('json = ' + str(r.json))
#    r = requests.get('http://crasmand01:8020/api/modelrequest/2122')
#    print('json = ' + str(r.json))
#    print(r.json)


def main():
    try:
        # from edge node home dir /data1/mounts/ValTest/Frank.Policastro

        make_landing_dir_cmd = 'hdfs dfs -mkdir /projects/actuarialresultsdev/landing'
        clean_txt_file_cmd = 'rm *.txt'
        hdfs_cp_am1_cmd = 'hdfs dfs -put /data1/mounts/ValTest/Frank.Policastro/Cohort_Base/*.Am1 /projects/actuarialresultsdev/landing/'
        list_top_cmd = 'hdfs dfs -ls /projects/actuarialresultsdev/db/'
        list_contents_cmd = 'hdfs dfs -ls /projects/actuarialresultsdev/landing/'
        delete_am1_cmd = 'hdfs dfs -rm /projects/actuarialresultsdev/landing/*.Am1'

        hdfs_cp_am1_cmd2 = 'hdfs dfs -put '
        hdfs_cp_am1_cmd2 += '/data1/mounts/ValTest/Frank.Policastro/alfa_input_orig/*/AM1/*.Am1 '
        hdfs_cp_am1_cmd2 += '/projects/actuarialresultsdev/landing/variable_annuity/fair_value/'

        #uce = UnixCmdExecutor()

        #print(uce.execute(hdfs_cp_am1_cmd2))
        #print(uce.execute(clean_txt_file_cmd))
        #print(uce.execute(make_landing_dir_cmd))
        #print(uce.execute(delete_am1_cmd))
        #print(uce.execute(hdfs_cp_am1_cmd))
        #print(uce.execute(list_top_cmd))
        #print(uce.execute(list_contents_cmd))

        #print(const.ProductLines.VARIABLE_ANNUITY.value)
        #print(const.Models.FAIR_VALUE.value)

        #uce.execute('cp /data1/mounts/ValTest/Frank.Policastro/Cohort_Base/*.Am1 /homedir/fpolicastro/')

        #call_model_manager_db()

        #try:
        #    #print(os.path.dirname(os.path.abspath(inspect.getframeinfo(inspect.currentframe()).filename)))

        #    arx2_file_list = ['./Bopparallel.Run.003.Arx2', './Base.Run.002.Arx2']

        #    for arx in arx2_file_list:
        #        process_arx_file(datetime.datetime.utcnow(), arx, const.BLACK_LIST_OF_ROWS, const.SECTION_TITLES)

        #except Exception as ex:
        #    exc_type, exc_value, exc_traceback = sys.exc_info()
        #    traceback.print_exception(exc_type, exc_value, exc_traceback)


        #try:
        #    am1_file_broken = ['./Base.Proj.003.Run.003.Rreq.032.Inv015.Scn.Mean._.Am1',
        #                       './Base.Proj.014.Run.014.Rreq.032.Inv015.Scn.Mean._.Am1']


        #    for af in am1_file_broken:
        #        process_file(datetime.datetime.utcnow(), af)
        #except Exception as ex:
        #    exc_type, exc_value, exc_traceback = sys.exc_info()
        #    traceback.print_exception(exc_type, exc_value, exc_traceback)

        #try:
        #    am1_file_broken = ['./base/Base.Proj.003.Run.003.Rreq.032.Inv015.Scn.Mean._.Am1',
        #                       './base/Base.Proj.014.Run.014.Rreq.032.Inv015.Scn.Mean._.Am1',
        #                       './base/Base.Proj.018.Run.018.Rreq.032.Inv015.Scn.Mean._.Am1',
        #                       './base/Base.Proj.022.Run.022.Rreq.032.Inv015.Scn.Mean._.Am1',
        #                       './base/Base.Proj.025.Run.025.Rreq.032.Inv015.Scn.Mean._.Am1']


        #    for af in am1_file_broken:
        #        process_file(datetime.datetime.utcnow(), af)
        #except Exception as ex:
        #    exc_type, exc_value, exc_traceback = sys.exc_info()
        #    traceback.print_exception(exc_type, exc_value, exc_traceback)


        #try:
        #    #print(os.path.dirname(os.path.abspath(inspect.getframeinfo(inspect.currentframe()).filename)))

        #    arx2_file_list = ['./base/Base.Run.002.Arx2',
        #                      './base/Base.Run.003.Arx2']

        #    for arx in arx2_file_list:
        #        process_arx_file(datetime.datetime.utcnow(), arx, const.BLACK_LIST_OF_ROWS, const.SECTION_TITLES)

        #except Exception as ex:
        #    exc_type, exc_value, exc_traceback = sys.exc_info()
        #    traceback.print_exception(exc_type, exc_value, exc_traceback)


        try:
            q118_base_list = ['./q118_base/Base.Proj.002.Run.002.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.003.Run.003.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.004.Run.004.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.005.Run.005.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.006.Run.006.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.007.Run.007.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.008.Run.008.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.009.Run.009.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.010.Run.010.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.011.Run.011.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.012.Run.012.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.013.Run.013.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.014.Run.014.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.015.Run.015.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.016.Run.016.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.017.Run.017.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.018.Run.018.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.019.Run.019.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.020.Run.020.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.021.Run.021.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.022.Run.022.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.023.Run.023.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.024.Run.024.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.025.Run.025.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.026.Run.026.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.027.Run.027.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.028.Run.028.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.029.Run.029.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.030.Run.030.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.031.Run.031.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.032.Run.032.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.033.Run.033.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.034.Run.034.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.035.Run.035.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.036.Run.036.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.037.Run.037.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q118_base/Base.Proj.038.Run.038.Rreq.032.Inv015.Scn.Mean._.Am1']


            q218_base_list = ['./q218_base/Base.Proj.002.Run.002.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.003.Run.003.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.004.Run.004.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.005.Run.005.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.006.Run.006.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.007.Run.007.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.008.Run.008.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.009.Run.009.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.010.Run.010.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.011.Run.011.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.012.Run.012.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.013.Run.013.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.014.Run.014.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.015.Run.015.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.016.Run.016.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.017.Run.017.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.018.Run.018.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.019.Run.019.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.020.Run.020.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.021.Run.021.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.022.Run.022.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.023.Run.023.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.024.Run.024.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.025.Run.025.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.026.Run.026.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.027.Run.027.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.028.Run.028.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.029.Run.029.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.030.Run.030.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.031.Run.031.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.032.Run.032.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.033.Run.033.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.034.Run.034.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.035.Run.035.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.036.Run.036.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.037.Run.037.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.038.Run.038.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_base/Base.Proj.039.Run.039.Rreq.032.Inv015.Scn.Mean._.Am1']


            q218_bopp_list = ['./q218_bop_parallel/Bopparallel.Proj.002.Run.002.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.003.Run.003.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.004.Run.004.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.005.Run.005.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.006.Run.006.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.007.Run.007.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.008.Run.008.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.009.Run.009.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.010.Run.010.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.011.Run.011.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.012.Run.012.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.013.Run.013.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.014.Run.014.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.015.Run.015.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.016.Run.016.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.017.Run.017.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.018.Run.018.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.019.Run.019.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.020.Run.020.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.021.Run.021.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.022.Run.022.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.023.Run.023.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.024.Run.024.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.025.Run.025.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.026.Run.026.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.027.Run.027.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.028.Run.028.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.029.Run.029.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.030.Run.030.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.031.Run.031.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.032.Run.032.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.033.Run.033.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.034.Run.034.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.035.Run.035.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.036.Run.036.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.037.Run.037.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.038.Run.038.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_bop_parallel/Bopparallel.Proj.039.Run.039.Rreq.032.Inv015.Scn.Mean._.Am1']


            q218_rate_list = ['./q218_rate_attrib/Rateattribution.Proj.002.Run.002.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.003.Run.003.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.004.Run.004.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.005.Run.005.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.006.Run.006.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.007.Run.007.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.008.Run.008.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.009.Run.009.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.010.Run.010.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.011.Run.011.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.012.Run.012.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.013.Run.013.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.014.Run.014.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.015.Run.015.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.016.Run.016.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.017.Run.017.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.018.Run.018.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.019.Run.019.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.020.Run.020.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.021.Run.021.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.022.Run.022.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.023.Run.023.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.024.Run.024.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.025.Run.025.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.026.Run.026.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.027.Run.027.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.028.Run.028.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.029.Run.029.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.030.Run.030.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.031.Run.031.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.032.Run.032.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.033.Run.033.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.034.Run.034.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.035.Run.035.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.036.Run.036.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.037.Run.037.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.038.Run.038.Rreq.032.Inv015.Scn.Mean._.Am1',
                              './q218_rate_attrib/Rateattribution.Proj.039.Run.039.Rreq.032.Inv015.Scn.Mean._.Am1']

            time_format_partition = "%Y-%m-%d-%H-%M-%S"
            time_format = "%Y-%m-%d %H:%M:%S"

            for af in q118_base_list:
                process_file(datetime.datetime.utcnow().strftime(time_format), af)

            for af in q218_base_list:
                process_file(datetime.datetime.utcnow().strftime(time_format), af)

            for af in q218_bopp_list:
                process_file(datetime.datetime.utcnow().strftime(time_format), af)

            for af in q218_rate_list:
                process_file(datetime.datetime.utcnow().strftime(time_format), af)

        except Exception as ex:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback)

    except Exception as ex:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback)

if __name__ == '__main__':
    main()
