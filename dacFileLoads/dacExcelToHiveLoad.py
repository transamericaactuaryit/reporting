#################################################################################################
# dacExcelToHiveLoad - based on passed parms, check if input .xlsx is found and read through rows.  
#                      Write the parquet table as the final output.
#
#  CHANGE DATE    CHANGE DESCRIPTION
#  =============  ==============================================================================
#  08/09/2018     Created
#
#
#################################################################################################
#import Constants as const
import archiveFileToHDFS  		# class to create flat files from .XLSX files
import excelToDelimitedPandas  	# class to create flat files from .XLSX files
import delimitedToHive     		# class to create hive tables from delimited files
import datetime
import io
import json
import os
import pandas as pd
import sys
import subprocess
import time
import traceback
#################################################################################################
# FUNCTIONS
#################################################################################################

#################################################################################################
# getDelimiter()
# Set the Delimiter based on passed parameter required, default is Pipe delimited
#################################################################################################
def getDelimiter(delimiter):
    if delimiter == "t":          # TAB
        return "\t";
    elif delimiter == "c":        # COMMA
        return ",";
    else:                         # DEFAULT PIPE
        return "|";

def printExitStatement():
	print 
	print "################## dacExcelToHiveLoad Program End  : " +   str(datetime.datetime.now())
	print 

#################################################################################################
# MAIN()
#################################################################################################
print 
print "################## dacExcelToHiveLoad Program Start: " +   str(datetime.datetime.now())
print 

### Get Table Name from line command parameter
baseTableName    = sys.argv[1]

### Get Configuration data for run using the table name passed in through commandline
try:
	with open('/home/UF9A82/dacExcelToHiveLoad.json') as json_data_file:
		configData   = json.load(json_data_file)

	baseHDFSSystemPath = configData['general_app']['baseHDFSSystemPath']
	delimiter          = configData[baseTableName]['delimiter']
	expectedColumns    = configData[baseTableName]['expectedColumns']
	skipHeader         = configData[baseTableName]['skipHeader']
	excelFile          = configData[baseTableName]['inFile']
	outputFileName     = configData[baseTableName]['outFile']
	hdfsDirectory      = configData[baseTableName]['hdfsDir']
	baseActuarialApp   = configData[baseTableName]['actuarialApp']
	sheetName          = configData[baseTableName]['sheetName']
	useDelimiter       = getDelimiter(delimiter)
except IOError as ioerror:
	print "ERROR: Cannot find Config file or Configuration Parameter for Program - " + str(ioerror)
	printExitStatement()
	sys.exit(8)
except KeyError as keyerror:
	print "ERROR: One or more JSON keys could not be found in the Config file: " + str(keyerror)
	printExitStatement()
	sys.exit(8)

print "   Program Setup "
print "      Python version   : #" + sys.version + "#"
print "      Pandas version   : #" + pd.__version__ + "#"
print "      hdfs system path : #" +  baseHDFSSystemPath + "#"
print "      input file       : #" + excelFile + "#"
print "      output file      : #" + outputFileName + "#"
print "      output delimiter : #" + useDelimiter + "#"   
print "      sheet name       : #" + sheetName + "#"   
print "      exepected columns: #" + expectedColumns + "#"
print "      hdfs directory   : #" + hdfsDirectory + "#"   
print "      base table name  : #" + baseTableName + "#"
print

# main process steps to 
#   1. create the delimited version of the Excel file
#   2. copy to HDFS landing, propogate data to Parquet and Flat version of the data tables
#   3. archive the original source file into the HDFS archive directory

excelToDelimitedPandas.excelToDelimited(expectedColumns, skipHeader, excelFile, outputFileName, useDelimiter, sheetName)  	# Create the delimited file from the Excel Source
delimitedToHive.delimitedToHive(baseActuarialApp, baseTableName, hdfsDirectory, outputFileName, useDelimiter)				# Read the delimited file and load to Landing, Parquet and Flat Hive tables
archiveFileToHDFS.archiveXLSXfile(baseHDFSSystemPath, baseActuarialApp, excelFile)											# Archive the source file to HDFS

printExitStatement()
