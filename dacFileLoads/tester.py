import sys
from zipfile import ZipFile
import os
from win32com.client import Dispatch
        

class Extract_VBA_Files:
    
    def __init__(self, key_value_config):
        self.config = key_value_config

    # Function to extract vbaProject.bin binary file from an xlsm
    def extract_vba_project_bin(self, macro_file, landing_path):
        try:
            vba_filename = 'vbaProject.bin'
            f_1 = os.path.basename(macro_file)
            f_2 = f_1.replace('.xlsb', '')
            f_3 = f_2.replace('.xlsm', '')
            final_file_name = f_3 +' Binary File'
            xlsm_zip = ZipFile(macro_file, 'r')
            vba_data = xlsm_zip.read('xl/' + vba_filename)
            vba_file = open(landing_path + final_file_name + '.bin', "wb")
            vba_file.write(vba_data)
            vba_file.close()  
        except:
            e = sys.exc_info()[1]
            print("Error %s" % str(e))
        
    def extract_vba_text_files(self, macro_file, landing_path):
        try:
            xl = Dispatch("Excel.Application")
            xl.Visible = False
            wb = xl.Workbooks.Open(macro_file)
            f_1 = os.path.basename(macro_file)
            f_2 = f_1.replace('.xlsb', '')
            f_3 = f_2.replace('.xlsm', '')
            final_file_name = f_3
            for vb_node_component in wb.VBProject.VBComponents:
                xlmodule = wb.VBProject.VBComponents(vb_node_component.Name)
                if xlmodule.Type in [1,2,3]:
                    vba_code_lines = xlmodule.CodeModule.Lines(1,1000000)
                    vba__text_file = open(landing_path + final_file_name + ' ' + str(vb_node_component.Name) + '.txt', "w")
                    vba__text_file.write(vba_code_lines)
                    vba__text_file.close()
            wb.Close()
            xl.Quit
        except:
            e = sys.exc_info()[1]
            print("Error %s" % str(e))


#******************************************************      
# main
#******************************************************    
def main():  
    config = {'Master Template':'C:\\Users\\jwilliamson\\Desktop\\MacroTestEnvironment\\Master Template 2018M06 Base_1.xlsb'
             }
    x = Extract_VBA_Files(config)
    for key, value in x.config.items():
        x.extract_vba_project_bin(value, 'C:\\Users\\jwilliamson\\Desktop\\MacroTestEnvironment\\Binary_VBA_Files\\')
        x.extract_vba_text_files(value, 'C:\\Users\\jwilliamson\\Desktop\\MacroTestEnvironment\\VBA_Source_Code_Text_Files\\')
    
if __name__ == '__main__':
    main()
  



