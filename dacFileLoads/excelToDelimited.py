#################################################################################################
# excelToDelimited      - based on passed parms, check if input .xlsx is found and read through rows.  
#                         Write the parquet table as the final output.
#
#  CHANGE DATE    CHANGE DESCRIPTION
#  =============  ==============================================================================
#  08/09/2018     Created
#
#
#################################################################################################
import datetime
import io
#import json
import os
#import pandas as pd
#import sys
#import subprocess
#import time
import traceback
import uuid
from cStringIO import StringIO
from xlrd import *

def excelToDelimited(expectedColumns, skipHeader, excelFile, outputFileName, useDelimiter):

    outputFile      = open(outputFileName,"w"); # open passed outputfile for writing 

    ### Verify excel file exists
    if os.path.isfile(excelFile):
        print "File Exists";
    else:
        print "SEVERE:: No Valid Input File Present!";
        sys.exit("SEVERE:: No Valid Input File Present!")
    try:   
        wb = open_workbook(excelFile);
    except ValueError, Argument:
        print "ERROR::opening Excel Workbook!", Argument;     
    
    print "Worksheet name(s):" , wb.sheet_names();
    print "  LOOP SHEETS";
     
    for sheet in wb.sheets():
        numberOfRows = sheet.nrows
        numberOfColumns = sheet.ncols
    
        if skipHeader == "y":
            recordStart = 1;
        else:
            recordStart = 0;   
                
        rows  = []
    
        if numberOfColumns == 0:  # Bypass if sheet has no data on it
            break   
    
        if numberOfColumns != int(expectedColumns) and numberOfColumns != 0:  #If the worksheet has data, check to see columns match control
            print "ERROR::Invalid number of columns for this sheet, expected " + str(expectedColumns) + " got " + str(numberOfColumns)
            sys.exit(8);
        else:
            print ("rows:" + str(numberOfRows) + ", columns:" + str(numberOfColumns));
            for row in range(recordStart, numberOfRows):               # read each row
                recordOut = str(uuid.uuid4()) + useDelimiter           # set the UUID as the first field, then loop through columns
                for col in range(numberOfColumns):                     # get column data 
                    try:
                        value     = str(sheet.cell(row,col).value)     # read the cell data value
                        recordOut = recordOut + str(value.replace(useDelimiter, "")).strip(" ") + useDelimiter; #trim and remove passed delimiter
                    except ValueError:
                        pass
                recordOut = recordOut + str(datetime.datetime.now())   # add on a time_stamp
                outputFile.write(recordOut + "\n");                    # Write record to file
	outputFile.close() 
