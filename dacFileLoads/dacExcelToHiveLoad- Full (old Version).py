#################################################################################################
# excelToDelimited - based on passed parms, check if input .xlsx is found and read through rows.  
#                    Write the parquet table as the final output.
#
#  CHANGE DATE    CHANGE DESCRIPTION
#  =============  ==============================================================================
#  08/09/2018     Created
#
#
#################################################################################################
#import Constants as const
import datetime
import io
import json
#import numpy as np
import os
import pandas as pd
import sys
import subprocess
import time
import traceback
import uuid
from cStringIO import StringIO
from py4j.java_gateway import java_import
from pyspark import *
from pyspark.sql import SQLContext, SparkSession, Row
from pyspark.sql.functions import udf
from pyspark.sql.types import *
from xlrd import *

#################################################################################################
# FUNCTIONS
#################################################################################################

#################################################################################################
# getDelimiter()
# Set the Delimiter based on passed parameter required, default is Pipe delimited
#################################################################################################
def getDelimiter(delimiter):
    if delimiter == "t":          # TAB
        return "\t";
    elif delimiter == "c":        # COMMA
        return ",";
    else:                         # DEFAULT PIPE
        return "|";

def excelToPandas():

    ### Verify excel file exists
    #if os.path.isfile(excelFile):
    #    print "File Exists"
    #else:
    #    sys.exit("SEVERE:: No Valid Input File Present!")

	print "process excel file to Pandas"
	xl = pd.ExcelFile(excelFile)
	xlSheetDF = pd.read_excel(xl, "Sheet1")
   
	numberOfColumns = len(xlSheetDF.columns)
	numberOfRows    = len(xlSheetDF.index)

	print ("rows: " + str(numberOfRows) + " cols: " + str(numberOfColumns))

	if numberOfColumns == 0:  # Bypass if sheet has no data on it
		print "WARN: 0 columns read from the spreadsheet"
		
	if numberOfColumns != int(expectedColumns) and numberOfColumns != 0:  #If the worksheet has data, check to see columns match control
		print "ERROR::Invalid number of columns for this sheet, expected " + str(expectedColumns) + " got " + str(numberOfColumns)	
		sys.exit(8);
	else:
		print "good"
	
	print xlSheetDF.head()
	xlSheetDF['TestColumn']
	print xlSheetDF.head()
	
		
#################################################################################################
# writeCSVFile()
# loop through Excel File writing data to output file in delimited format.  Delimiter set based
# of passed in delimiter type from commandline.
#################################################################################################
def writeCSVFile():
    ### Verify excel file exists
    if os.path.isfile(excelFile):
        print "File Exists";
    else:
        print "SEVERE:: No Valid Input File Present!";
        sys.exit("SEVERE:: No Valid Input File Present!")
    try:   
        wb = open_workbook(excelFile);
    except ValueError, Argument:
        print "ERROR::opening Excel Workbook!", Argument;     
    
    print "Worksheet name(s):" , wb.sheet_names();
    print "  LOOP SHEETS";
     
    for sheet in wb.sheets():
        numberOfRows = sheet.nrows
        numberOfColumns = sheet.ncols
    
        if skipHeader == "y":
            recordStart = 1;
        else:
            recordStart = 0;   
                
        rows  = []
    
        if numberOfColumns == 0:  # Bypass if sheet has no data on it
            break   
    
        if numberOfColumns != int(expectedColumns) and numberOfColumns != 0:  #If the worksheet has data, check to see columns match control
            print "ERROR::Invalid number of columns for this sheet, expected " + str(expectedColumns) + " got " + str(numberOfColumns)
            sys.exit(8);
        else:
            print ("rows:" + str(numberOfRows) + ", columns:" + str(numberOfColumns));
            for row in range(recordStart, numberOfRows):               # read each row
                recordOut = str(uuid.uuid4()) + useDelimiter           # set the UUID as the first field, then loop through columns
                sqlOut    = "'" + str(uuid.uuid4()) + "', "           
                for col in range(numberOfColumns):                     # get column data 
                    try:
                        value     = str(sheet.cell(row,col).value)     # read the cell data value
                        recordOut = recordOut + str(value.replace(useDelimiter, "")).strip(" ") + useDelimiter; #trim and remove passed delimiter
                        sqlOut    = sqlOut + "'" + str(value.replace(",", "")).strip(" ") + "', "; 
                    except ValueError:
                        pass
                recordOut = recordOut + str(datetime.datetime.now())   # add on a time_stamp
                outputFile.write(recordOut + "\n");                    # Write record to file
                sqlOut = sqlOut +  "'" + str(datetime.datetime.now()) + "', " + "'" + loadDate + "'"
                #print sqlOut
                insertProductionTable = "insert into " + ptable  + " partition (load_date) values (" + sqlOut + ")"
                #print "|" + insertProductionTable + "|"
                #spark.sql(insertProductionTable)
	outputFile.close() 
	#hdfsCommand = "hdfs dfs -rm {0}/*".format(hdfsDirectory)
	#print hdfsCommand
	#subprocess.check_output(hdfsCommand)

	hdfsCommand = "hdfs dfs -put -f {0} {1}".format(outputFileName,  hdfsDirectory).split()
	#hdfsCommand = "hdfs dfs -ls {0}".format('hdfs://nameservice1/user/UF9A82').split()
	print hdfsCommand
	subprocess.check_output(hdfsCommand)


	#sys.exit(0)

#################################################################################################
# MAIN()
#################################################################################################
print 
print "################## excelToDelimited Program Start: " +   str(datetime.datetime.now())
print 


### Get Table Name from line command parameter
baseTableName   = sys.argv[1]
loadDate        = time.strftime("%m-%d-%Y")
loadTime        = time.strftime("%H.%M.%S")

### Get Configuration data for run using the table name passed in through commandline
with open('excelToDelimited.json') as json_data_file:
    configData  = json.load(json_data_file)

delimiter       = configData[baseTableName]['delimiter']
expectedColumns = configData[baseTableName]['expectedColumns']
skipHeader      = configData[baseTableName]['skipHeader']
excelFile       = configData[baseTableName]['inFile']
outputFileName  = configData[baseTableName]['outFile']
hdfsDirectory   = configData[baseTableName]['hdfsDir']
outputFile      = open(outputFileName,"w"); 
useDelimiter    = getDelimiter(delimiter)

print "   Program Setup "
print "      Python version   : #" + sys.version + "#"
print "      Pandas version   : #" + pd.__version__ + "#"
print "      input file       : #" + excelFile + "#"
print "      output file      : #" + outputFileName + "#"
print "      output delimiter : #" + useDelimiter + "#"   
print "      exepected columns: #" + expectedColumns + "#"
print "      hdfs directory   : #" + hdfsDirectory + "#"   
print "      base table name  : #" + baseTableName + "#"
print "      load date        : #" + loadDate + "#"
print "      load time        : #" + loadTime + "#"
print

#excelToPandas()
#sys.exit(0)

### Setup SPARK session for Parquet file Load
spark = SparkSession.builder.getOrCreate()
sc    = spark.sparkContext
spark.sql("set hive.exec.dynamic.partition.mode=nonstrict")
spark.sql("set hive.exec.dynamic.partition=true")

fs = sc._jvm.org.apache.hadoop.fs.FileSystem.get(sc._jsc.hadoopConfiguration())
java_import(spark._jvm, 'org.apache.hadoop.fs.Path')

ltable  = "actuarialresultsdev.l" + baseTableName
lpath   ="/projects/actuarialresultsdev/db/variable_annuity/dac/{0}/".format(baseTableName)
lschema = spark.sql("""select * from {} where 1=0""".format(ltable))  # get landing schema
#print "      landing schema       : " + str(lschema)

ptable  = "actuarialresultsdev.p" + baseTableName
ppath   ="/projects/actuarialresultsdev/db/enterpriseready/variable_annuity/dac/" + baseTableName
pschema = spark.sql("""select * from {} where 1=0""".format(ptable))  # get production schema
#print "      production schema    : " + str(pschema)

ftable  = "actuarialresultsdev.f" + baseTableName
fpath   ="/projects/actuarialresultsdev/db/flat/variable_annuity/dac/{0}/".format(baseTableName)
fschema = spark.sql("""select * from {} where 1=0""".format(ftable))  # get landing schema


print "      landing path         : " + lpath
print "      landing table name   : " + ltable
print "      landing schema       : "
lschema.printSchema()

print "      production path      : " + ppath
print "      production table name: " + ptable
print "      production schema    : "
pschema.printSchema()

print "      flat path            : " + fpath
print "      flat table name      : " + ftable
print "      flat schema          : "
fschema.printSchema()


### Create instream dataframe for excel data
writeCSVFile()

ldataframe = spark.read.option("header", "false").option("delimiter", useDelimiter).csv(lpath)
ldataframe.createOrReplaceTempView("ltable")
print ldataframe

### Write the Finalized records to the Parquet Table version based on "landed" version of the data
insertProductionTable = "insert overwrite table " + ptable + " partition (load_date, load_time) select lan.*, " + "'" + loadDate + "', '" + loadTime + "' " + " from ltable lan"
print "SQL: " + insertProductionTable
spark.sql(insertProductionTable)

### Write the Finalized records to the Parquet Table version based on "landed" version of the data
insertFlatTable = "insert overwrite table " + ftable + "  select lan.*  from ltable lan"
print "SQL: " + insertFlatTable
spark.sql(insertFlatTable)


print 
print "################## excelToDelimited Program End  : " +   str(datetime.datetime.now())
print 
