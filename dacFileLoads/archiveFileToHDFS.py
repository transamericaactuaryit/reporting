#################################################################################################
# excelToDelimitedPandas - based on passed parms, check if input .xlsx is found and read through rows.  
#                          Write the parquet table as the final output.
#
#  CHANGE DATE    CHANGE DESCRIPTION
#  =============  ==============================================================================
#  08/09/2018     Created
#
#
#################################################################################################
import datetime
import subprocess
import time
import traceback

def archiveXLSXfile(baseHDFSSystemPath, baseActuarialApp, excelFile):

	loadDate    = time.strftime("%Y%m%d")
	loadTime    = time.strftime("%H%M%S")
	archiveFile = baseHDFSSystemPath + 'archive/' + baseActuarialApp + '/' + loadDate + '_' + loadTime + '_' + excelFile[excelFile.rfind('/')+1:len(excelFile)] # find the source file name to append date/time for archive name

	print
	print 'ARCHIVE FILE LOCATION: |' + archiveFile + '|'
	print
	hdfsCommand = ['hdfs', 'dfs', '-put', '-f', str(excelFile.replace(" ", "%20")), str(archiveFile)]  # build the list for the HDFS put command.  %20 has to replace all spaces on the source file or will throw an error
	print hdfsCommand
	print

	subprocess.check_output(hdfsCommand)
