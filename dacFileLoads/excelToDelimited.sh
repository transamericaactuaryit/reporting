#!/bin/bash
## Including Global functions in the code.
sh spark2-submit --conf spark.port.maxRetries=50 excelToDelimited.py p 7 y /home/UF9A82/DACAccountingFiles/FormattedFiles/Q218VAExpectedUnderlyingEarningsDACandSOP_modified_v2.xlsx /home/UF9A82/DACAccountingFiles/FormattedFiles/Q218VAExpectedUnderlyingEarningsDACandSOP_modified_v2.txt
#spark2-submit --conf spark.port.maxRetries=50 excelToDelimited.py p 4 y /home/UF9A82/DACAccountingFiles/FormattedFiles/FReCRegulatoryVAReinsVOBA2018M06_modified_v2.xlsx /home/UF9A82/DACAccountingFiles/FormattedFiles/FReCRegulatoryVAReinsVOBA2018M06_modified_v2.txt
#spark2-submit --conf spark.port.maxRetries=50 excelToDelimited.py p 5 y /home/UF9A82/DACAccountingFiles/FormattedFiles/SOPResultSummary2018M06Base_1_Modified_v2.xlsx /home/UF9A82/DACAccountingFiles/FormattedFiles/SOPResultSummary2018M06Base_1_Modified_v2.txt
RC_Ingestion=$?

	if [ $RC_Ingestion != "0" ]; then
		echo "ERROR: Ingestion Script failed" >> $Ingestion_LogFile
		exit ${RC_Ingestion}
	fi

echo "FINAL-EXIT"
echo "FINAL-EXIT" >> $Ingestion_LogFile
exit
