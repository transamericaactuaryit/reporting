#################################################################################################
# excelToDelimitedPandas - based on passed parms, check if input .xlsx is found and read through rows.  
#                          Write the parquet table as the final output.
#
#  CHANGE DATE    CHANGE DESCRIPTION
#  =============  ==============================================================================
#  08/09/2018     Created
#
#
#################################################################################################
import datetime
import io
import os
import pandas as pd
import traceback
import uuid

def excelToDelimited(expectedColumns, skipHeader, excelFile, outputFileName, useDelimiter, sheetName):

    ### Verify excel file exists
	if os.path.isfile(excelFile):
		print "File Exists";
	else:
		print "SEVERE:: No Valid Input File Present!";
		sys.exit(8)

	df = pd.read_excel(excelFile, sheetname=sheetName) 				       		# create dataframe from excel file and worksheet
	print "rows: " + str(len(df.index)) + " columns: " + str(len(df.columns))
	print(df.columns)

	df.insert(0, 'uuid', str(uuid.uuid4()))                                		# add uuid column to the beginning of dataframe.  0 is the first column

	for i in range (len(df.index)):                                        		# fill the uuid column with dynamically create UUID's
		df.set_value(i, 'uuid', str(uuid.uuid4()))                         		# set the uuid to the row value
	df.insert(len(df.columns), 'time_stamp', str(datetime.datetime.now())) 		# add time_stamp as the last column of dataframe
	
	jsonFile = outputFileName.replace('.csv', '.json') 					   		# create a new json file with same base path/name as .csv file
	htmlFile = outputFileName.replace('.csv', '.html') 					   		# create a new html file with same base path/name as .csv file

	df.to_csv(outputFileName, sep=useDelimiter, header=False, index=False) 		# write as a csv file
	df.to_json(jsonFile, orient='records')					   			   		# write the json data version
	df.to_html(htmlFile)					   			   				   		# write the html data version
