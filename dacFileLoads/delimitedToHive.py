#################################################################################################
# delimitedToHive      - based on passed parms, check if input .xlsx is found and read through rows.  
#                        Write the parquet table as the final output.
#
#  CHANGE DATE    CHANGE DESCRIPTION
#  =============  ==============================================================================
#  08/09/2018     Created
#
#
#################################################################################################
#import Constants as const
import datetime
#import io
#import json
#import numpy as np
#import os
#import pandas as pd
#import sys
import subprocess
import time
import traceback
#import uuid
#from cStringIO import StringIO
from py4j.java_gateway import java_import
from pyspark import *
from pyspark.sql import SQLContext, SparkSession, Row
from pyspark.sql.functions import udf
from pyspark.sql.types import *
#from xlrd import *

#################################################################################################
# FUNCTIONS
#################################################################################################


def delimitedToHive(baseActuarialApp, baseTableName, hdfsDirectory, outputFileName, useDelimiter):
	
	loadDate        = time.strftime("%m-%d-%Y")
	loadTime        = time.strftime("%H.%M.%S")

	### Create instream dataframe for excel data
	hdfsCommand = "hdfs dfs -put -f {0} {1}".format(outputFileName,  hdfsDirectory).split()
	print hdfsCommand
	subprocess.check_output(hdfsCommand)

	### Setup SPARK session for data loads
	spark = SparkSession.builder.getOrCreate()
	sc    = spark.sparkContext
	spark.sql("set hive.exec.dynamic.partition.mode=nonstrict")
	spark.sql("set hive.exec.dynamic.partition=true")
	
	fs = sc._jvm.org.apache.hadoop.fs.FileSystem.get(sc._jsc.hadoopConfiguration())
	java_import(spark._jvm, 'org.apache.hadoop.fs.Path')
	
	ltable  = "actuarialresultsdev.l" + baseTableName
	lpath   ="/projects/actuarialresultsdev/db/landing/{0}/{1}/".format(baseActuarialApp, baseTableName)
	lschema = spark.sql("""select * from {} where 1=0""".format(ltable))  # get landing schema
	#print "      landing schema       : " + str(lschema)
	
	ptable  = "actuarialresultsdev.p" + baseTableName
	ppath   ="/projects/actuarialresultsdev/db/enterpriseready/{0}/".format(baseActuarialApp) + baseTableName
	pschema = spark.sql("""select * from {} where 1=0""".format(ptable))  # get production schema
	#print "      production schema    : " + str(pschema)
	
	ftable  = "actuarialresultsdev.f" + baseTableName
	fpath   ="/projects/actuarialresultsdev/db/flat/{0}/{1}/".format(baseActuarialApp, baseTableName)
	fschema = spark.sql("""select * from {} where 1=0""".format(ftable))  # get landing schema
	
	
	print "      landing path         : " + lpath
	print "      landing table name   : " + ltable
	print "      landing schema       : "
	lschema.printSchema()
	
	print "      production path      : " + ppath
	print "      production table name: " + ptable
	print "      production schema    : "
	pschema.printSchema()
	
	print "      flat path            : " + fpath
	print "      flat table name      : " + ftable
	print "      flat schema          : "
	fschema.printSchema()

	ldataframe = spark.read.option("header", "false").option("delimiter", useDelimiter).csv(lpath)
	ldataframe.createOrReplaceTempView("ltable")
	print ldataframe
	
	### Write the Finalized records to the Parquet Table version based on "landed" version of the data
	insertProductionTable = "insert overwrite table " + ptable + " partition (load_date, load_time) select lan.*, " + "'" + loadDate + "', '" + loadTime + "' " + " from ltable lan"
	print "SQL: " + insertProductionTable
	spark.sql(insertProductionTable)
	
	### Write the Finalized records to the Parquet Table version based on "landed" version of the data
	insertFlatTable = "insert overwrite table " + ftable + "  select lan.*  from ltable lan"
	print "SQL: " + insertFlatTable
	spark.sql(insertFlatTable)
