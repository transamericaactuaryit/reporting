#################################################################################################
# hiveToExcel -   
#               
#
#  CHANGE DATE    CHANGE DESCRIPTION
#  =============  ==============================================================================
#  08/22/2018     Created
#
#
#################################################################################################
import excelDeleteSheet
import datetime
import io
import json
import os
import pandas as pd
import sys
import subprocess
import time
import traceback
import uuid
import xlsxwriter


from cStringIO import StringIO
from openpyxl import load_workbook
from py4j.java_gateway import java_import
from pyspark import *
from pyspark.sql import SQLContext, SparkSession, Row
from pyspark.sql.functions import udf
from pyspark.sql.types import *
#from xlrd import *
#################################################################################################
# FUNCTIONS
#################################################################################################

#################################################################################################
# MAIN()
#################################################################################################
print 
print "################## hiveToExcel Program Start: " +   str(datetime.datetime.now())
print 


### Get Table Name from line command parameter
excelProcess    = sys.argv[1]
loadDate        = time.strftime("%m-%d-%Y")
loadTime        = time.strftime("%H.%M.%S")

### Get Configuration data for run using the table name passed in through commandline
with open('/home/UF9A82/hiveToExcel.json') as json_data_file:
    configData  = json.load(json_data_file)

templatePath	= configData[excelProcess]['templatePath']
templateFile    = configData[excelProcess]['templateFile']
excelPath       = configData[excelProcess]['excelPath']
excelFile       = configData[excelProcess]['excelFile']
sheetName       = configData[excelProcess]['sheetName']
SQLSelect       = configData[excelProcess]['SQLSelect']

print "   Program Setup "
print "      Python version   : #" + sys.version + "#"
print "      Pandas version   : #" + pd.__version__ + "#"
print "      Excel Process    : #" + excelProcess + "#"
print "      Template file    : #" + templatePath + templateFile + "#"
print "      Output Excel     : #" + excelPath + excelFile + "#"
print "      Sheet Name       : #" + sheetName + "#"
print "      SQL Select       : #" + SQLSelect + "#"
print


### Setup SPARK session for Parquet file Load
spark = SparkSession.builder.getOrCreate()
sc    = spark.sparkContext
spark.sql("set hive.exec.dynamic.partition.mode=nonstrict")
spark.sql("set hive.exec.dynamic.partition=true")

fs = sc._jvm.org.apache.hadoop.fs.FileSystem.get(sc._jsc.hadoopConfiguration())
java_import(spark._jvm, 'org.apache.hadoop.fs.Path')


sqlContext = SQLContext(sc)
df = sqlContext.sql(SQLSelect)

print
print
df.printSchema()
df.show()
pandasDF = df.toPandas()
print pandasDF.head()


### Excel Writing Portion of the Show
fileExtension = templateFile[templateFile.rfind('.'):len(templateFile)]
#excelFile   = templateFile.replace(".xlsm", (" " + loadDate + "-" + loadTime + ".xlsm")) 
excelFile     = templateFile.replace(fileExtension, (" stage" + fileExtension)) 
workBook      = load_workbook(templatePath + templateFile, keep_vba=True)							# Create a Pandas Excel writer using XlsxWriter as the engine.
writer        = pd.ExcelWriter(excelPath + excelFile, engine='openpyxl')		
writer.book   = workBook

workBook.get_sheet_names()
print "=====> Worksheets in Workbook "
print workBook.get_sheet_names()
 
writer.sheets = dict((ws.title, ws) for ws in workBook.worksheets)
pandasDF.to_excel(writer, sheet_name=sheetName, index=False, startrow=1, header=False)  # Convert the dataframe to an XlsxWriter Excel object.
writer.save()                                                 # Close the Pandas Excel writer and output the Excel file.

deleteSheet = 'PivotARX'
excelDeleteSheet.delete(excelPath + excelFile, deleteSheet)

deleteSheet = 'PivotAlteryx'
excelDeleteSheet.delete(excelPath + excelFile, deleteSheet)

print
print




print 
print "################## hiveToExcel Program End  : " +   str(datetime.datetime.now())
print 
