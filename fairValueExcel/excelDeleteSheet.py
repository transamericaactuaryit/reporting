#################################################################################################
# deleteExcelSheet -   
#               
#
#  CHANGE DATE    CHANGE DESCRIPTION
#  =============  ==============================================================================
#  09/24/2018     Created
#
#
#################################################################################################
import datetime
#import io
#import json
#import os
import pandas as pd
#import sys
#import subprocess
#import time
#import traceback
#import uuid
import xlsxwriter

from openpyxl import load_workbook

#################################################################################################
# FUNCTIONS
#################################################################################################

#################################################################################################
# MAIN()
#################################################################################################

def delete(workBookName, sheetName):
	print
	print
	print
	print 'workbook: ' + workBookName + ' sheetName: ' + sheetName
	print
	print
	print
	#fileExtension = templateFile[templateFile.rfind('.'):len(templateFile)]
	#excelFile     = templateFile.replace(fileExtension, (" stage" + fileExtension)) 

	workBook      = load_workbook(workBookName, keep_vba=True)							# Create a Pandas Excel writer using XlsxWriter as the engine.
#	writer        = pd.ExcelWriter(workBook, engine='openpyxl')		
#	writer.book   = workBook

	workBook.get_sheet_names()
	print
	print "=====> Worksheets in Workbook "
	print workBook.get_sheet_names()

	if sheetName in workBook.get_sheet_names():
		print
		print "=====> " + sheetName + " exists and will be removed"
		#workBook.remove_sheet(workBook.get_sheet_by_name(sheetName))
		print
		print workBook.get_sheet_names()
		print

	print 'saving...'
	workBook.save(workBookName)
	print 'done saving...'